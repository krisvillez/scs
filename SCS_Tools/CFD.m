function Colour  =   CFD(Qualifier,Map)

% -------------------------------------------------------------------------
% SCS toolbox - CFD.m
% -------------------------------------------------------------------------
% Description 
%
% This CFD (Colours For Drawing) function obtains RGB colour codes
% corresponding to qualitative markers. 
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Colour  =   CFD(Qualifier,Map)
%
%
% INPUT
%	Qualifier:	Vector of qualitative markers for episodes, as integers
%               ranging from 1 to 14. If left unspecified, complete colour
%               map is returned.
%               (optional, default=[1:14])
%	Map:        Colour map type used. Use 1 for colour map (blue/red
%               tones), use 2 for grey scale map.
%               (optional, default=1)
%
% OUTPUT:
%	Colour:		Matrix of RGB colours for each element in Qualifier. Each
%               of three columns represents one channel (RGB). 
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-06-09
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2014-2019 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------



if nargin<1 || isempty(Qualifier),  Qualifier   =   (1:14)  ;   end % get complete map
if nargin<2 || isempty(Map),        Map         =   1       ;   end % unspecified map, use colour one

% verify whether qualifier are proper
%assert(all(Qualifier(:)>=-3),'At least one qualifier too low (mimimally: -3')
%assert(all(Qualifier(:)<=3),'At least one qualifier too high (maximally: +3')

switch Map
    case 1
        % colour map
        Colour   =   [  0   0   1   ;
                        0   0   .8  ;
                        0   0   .3  ;
                        0   0   0   ;
                        1   0   0   ;
                        .8  0   0   ;
                        .3  0   0   ;
                        0   0   .8  ;
                        0   0   0   ;
                        .8  0   0   ;
                        0   .5  .5  ;
                        0   0   0   ;
                        .5  .5  0   ;
                        1   1   1   ]   ;
        Colour(1:7,:) = (Colour(1:7,:));
    case 2
        % grey scale map
		Colour			=		ones(14,3)	;
        Colour(1:7,:)   =   [   5/7 2/7 1/7 4/7 7/7 6/7 3/7 ]'/2   ;
        Colour   		=       mean(Colour,2)*ones(1,3)           ;
    otherwise
        error('unknown map')
end

Colour      =   Colour(Qualifier,:) ;   % get corresponding RGB colours
