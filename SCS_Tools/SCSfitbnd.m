function [LB,UB,BestX,modelLB,modelUB]  =  SCSfitbnd(model,KnotInt)

% -------------------------------------------------------------------------
% SCS toolbox - SCSfitbnd.m
% -------------------------------------------------------------------------
% Description
%   This function computes the lower and upper bound values for the
%   objective function of the shape constrained spline fitting problem by
%   considering the provided feasible interval for the transition
%   positions. It also produces a feasible solution corresponding the upper
%   bound solution and the modified SCS models corresponding to the lower
%   and upper bound specification.
%
% -------------------------------------------------------------------------
% Syntax:   [LB,UB,BestX,modelLB,modelUB]  =  SCSfitbnd(model,KnotInt)
%
% Inputs:
%	model       Shape Constrained Spline model
%	KnotInt     Intervals for the transitions expressed relatively to the
%               spline knot positions
%
% Outputs:
%   LB          Objective function lower bound
%   UB          Objective function upper bound
%   BestX       Best-known upper bound solution
%   modelLB     Shape Constrained Spline model (lower bound solution)
%   modelUB     Shape Constrained Spline model (upper bound solution)
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-06-09
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% ========================================================================
%	INPUTS
% ========================================================================

nEpisode    =	model.dims.episodes     ;
nKnot     	=   model.dims.knots        ;

nTrans      =	model.dims.transitions	;
verbose     =	false                   ;


% ========================================================================
%	INPUT CHECKS
% ========================================================================

% format of KnotInt:
%   rows = transitions
%   cols = left/right bound

[I,J]       =   size(KnotInt)           ;
nTrans      =	model.dims.transitions	;

message     =	[ 'Error in SCSfitbnd: The input dimensions are incorrect. The row dimensions of input "KnotInt" should match the value of "model.dimensions.transitions". The latter is equal to ' num2str(nTrans) '.'] ;
assert(I==nTrans,message)

KnotInt            =	min(nKnot,max(1,KnotInt) )              ;
D                   =	KnotInt(:,2)-KnotInt(:,1)               ;
assert(all(D>0),'Error in SCSfitbnd: At least one interval has a left-bound which is higher than its right-bound.')


% ========================================================================
%	PRE-ALLOCATION
% ========================================================================
LB      =   inf             ;
UB      =   inf             ;
BestX	=	nan(nTrans,1)	;
modelLB =	model           ;
modelUB =	model           ;


% ========================================================================
%	GRAPHICAL OUTPUT
% ========================================================================
if isfield(model,'graphbound') && model.graphbound
    doplot = true ;
    figure(73)
    close 73
else
    doplot = false;
end


% ========================================================================
%	SEARCH A FEASIBLE SOLUTION
% ========================================================================

[FeasibleSolution,KnotSolution] =	TransSearch(KnotInt,nKnot) ;
BestX                           =	KnotSolution            ;

% ========================================================================
%	COMPUTE BOUNDS
% ========================================================================

if FeasibleSolution % Aa feasible solution was found
    
    modelZERO	=	model   ;
    
    for J=1:2
        % J=1 -> compute upper bound
        % J=2 -> compute lower bound
        
        % ----------------------------------------------------------------
        %   Get input
        % ----------------------------------------------------------------
        switch J
            case 1, INPUT   =   KnotSolution(:)             ;
            case 2, INPUT   =   KnotInt                     ;
        end
        
        % ----------------------------------------------------------------
        %   Compute fit
        % ----------------------------------------------------------------
        switch model.type
            case 'SCS'
                
                % Compute for every spline knot which episode it belong to:
                Episode             =   GetEpisodes(model,INPUT)	;
                
                % Create new model structure and add info:
                modelONE            =	modelZERO   ;
                modelONE.episodes	=	Episode     ;
                
                % Model fitting:
                if J==1 && model.dims.nVarOut>1
                    modelONE        =	SCSfit(modelONE,KnotSolution)	;
                else
                    modelONE        =	SCSfit(modelONE,[],modelONE.episodes)	;
                end
                
            case 'SCSD'
                
                % Modify the model structure to account for
                % discontinuities:
                modelONE            =	ApplyDiscon(modelZERO,INPUT)	;
                
                % Indicate that basis was re-evaluated already:
                modelONE.basisrecomputed	=	true	;
                
                % Model fitting:
                modelONE	=	SCSfit(modelONE,[],modelONE.episodes)	;
        end
        
        
        % ----------------------------------------------------------------
        %   Rename model and store info
        % ----------------------------------------------------------------
        switch J
            case 1
                modelUB =	modelONE                        ;
                UB      =	modelUB.performance.objective   ;
            case 2 
                modelLB =	modelONE                        ;
                LB      =	modelLB.performance.objective   ;
        end
    end
    
    % ----------------------------------------------------------------
    %   Graphical output
    % ----------------------------------------------------------------
    if doplot
        figure(73), hold on
        plot(modelUB.t,modelUB.y,'o')
        plot(modelUB.knots,modelUB.Xk{1}*modelUB.beta,'.-')
        plot(modelLB.knots,modelLB.Xk{1}*modelLB.beta,'.-')
        stairs(modelUB.knots,modelUB.episodes(:)','m+-')
        stairs(modelLB.knots,modelLB.episodes(:)','mx-')
    end
    
    
end


% ----------------------------------------------------------------
%   Bound correction if needed
% ----------------------------------------------------------------
if isnan(LB), LB= 0; end
if isnan(UB), UB= 0; end
if UB<LB
%     disp(['-> ModelFitTransBound: bound deficit ' num2str(LB-UB) ] )
    LB = 0; %
    %LB = min([UB LB]) ;
end


end

% ***********************************************************************
% ***********************************************************************


% ========================================================================
%	TransSearch: Dynamic program to compute a feasible solution
% ========================================================================

function [FeasibleSolution,Trans] = TransSearch(TransInt,nKnot)

% ----------------------------------------------------------------
% Number of transitions and states in Markov chain
% ----------------------------------------------------------------
nTrans	=	size(TransInt,1)    ;
nState	=	nTrans+1            ;

% ----------------------------------------------------------------
% Pre-allocation and initialization of forward pass
% ----------------------------------------------------------------
Trans   =	nan(nTrans,1)       ;

% Cost to reach state
C1      =	inf(nState,nKnot)   ; 
C1(1,1) =	0                   ;

% Predecessor
P1      =	nan(nState,nKnot)   ;
P1(1,1) =	0                   ;

% ----------------------------------------------------------------
% Forward pass
% ----------------------------------------------------------------
for ik=2:nKnot
    
    for c=1:nState % for every state  ...
        
        Cost	=	nan(1,nState)	;	% initialize costs to reach it
        
        % for every candidate predecessor state ...
        for p=max(1,c-1):c
            
            % get cost to reach that state
            Cost(p)	=	C1(p,ik-1)  ;
            
            % compute transition cost
            if p<c
                if ik>floor(TransInt(p,1)) && ik<=ceil(TransInt(p,2)) 
                    % if: current time point is within the permissible range
                    % for the considered state transition,
                    % then: compute a
                    % quadratic cost to make the transition
                    tm      =	sum(TransInt(p,:))/2 ;
                    Cost(p) =	Cost(p) + (ik-tm).^2 ;
                else
                    % else: infeasible state transition, therefore infinite
                    % cost
                    Cost(p) = inf;
                end
            end
        end
        
        % identify the minimum cost among those computed:
        [CostMin,predec]    =   min(Cost)	;
        
        % select the minimum-cost predecessor as the best predecessor and
        % store associated cost to reach the considered state via this
        % predecessor 
        C1(c,ik)            =	CostMin ;
        P1(c,ik)            =   predec	; 
    end
    
end
    
% ----------------------------------------------------------------
%   Final solution / Backward pass
% ----------------------------------------------------------------
if C1(end,nKnot)==inf
    % Cost in the final state is infinite -> there is no feasible path from
    % the first to the last state
    FeasibleSolution = false;
else
    
    FeasibleSolution = true;

    % ----------------------------------------------------------------
    % Pre-allocation and initialization of forward pass
    % ----------------------------------------------------------------
    S = nan(nKnot,1) ;
    S(nKnot)=nState ;
    
    % ----------------------------------------------------------------
    %   Backward pass
    % ----------------------------------------------------------------
    for ik=nKnot:-1:2
        % from the current state, identify its predecessor and select it
        S(ik-1) = P1(S(ik),ik);
        
        % if this means a transition is made from ik-1 to ik ...
        if S(ik-1)<S(ik)
            % compute the following two points:
            
            % the time of the predecessor state or the left bound for the
            % transition time, whichever is higher:
            iLeft	=	max( ik-1 , TransInt(S(ik-1),1)	)	;
            
            % the time of the current state or the right bound for the
            % transition time, whichever is lower:
            iRight	=	min( ik , TransInt(S(ik-1),2)	)	;
            
            % identify the transition as the model between the two
            % identified points:
            Trans(S(ik-1)) = (iLeft+iRight)/2;
        end
    end
    
end

end

% ========================================================================
% ========================================================================