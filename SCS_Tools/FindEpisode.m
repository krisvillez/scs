function	iEpisode  =  FindEpisode(knotindex,TransInt,nEpisode)

% -------------------------------------------------------------------------
% SCS toolbox - FindEpisode.m
% -------------------------------------------------------------------------
% Description
%   The function identifies the applicable episode to the knot indicated by
%   knotindex.
%
% -------------------------------------------------------------------------
% Syntax:   iEpisode  =  FindEpisode(knotindex,TransInt,nEpisode)
%
% Inputs:
%   knotindex	Considered knot 
%   TransInt    Bounding intervals for the transitions expressed relative
%               to the knot positions
%   nEpisode    Number of episodes
%
% Outputs:
%	iEpisode    Episode number of the considered knot index
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2018.01.25
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

switch size(TransInt,2)
    case 1
        col1    =   1	;
        col2    =   1	;
    case 2
        col1    =   1	;
        col2    =   2	;
    otherwise
        error('Error in FindEpisode: Column dimension of TransInt is not 1 or 2.')
end

if knotindex<=TransInt(1,col1)
    iEpisode    =	1                               ;
elseif knotindex>=TransInt(end,col2)
    iEpisode    =	nEpisode                        ;
else
    rule1       =	knotindex>=TransInt(1:end-1,col2)   ;
    rule2       =	knotindex<=TransInt(2:end,col1)     ;
    iEpisode	=	1+find( and( rule1 , rule2 ) )  ;
end
            
end
