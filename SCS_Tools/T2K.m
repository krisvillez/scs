function K  =  T2K(model,T)

% -------------------------------------------------------------------------
% SCS toolbox - T2K.m
% -------------------------------------------------------------------------
% Description
%   This function translates independent values in the original scale of
%   the variable to independent values expressed relative to the spline
%   knots.
%
% -------------------------------------------------------------------------
% Syntax:   K  =  T2K(model,T)
%
% Inputs:
%   model   Shape Constrained Spline model
%   T       Independent variable in original scale
%
% Outputs:
%   K       Independent variable expressed relative to knot positions
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2018.01.25
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% =======================================================================
%   INPUT AND CHECKS
% =======================================================================

assert(isfield(model,'knots'),      'Error in T2K: Field model.knots not found in model')
assert(isfield(model,'dims'),       'Error in T2K: Field model.dims not found in model')
assert(isfield(model.dims,'knots'), 'Error in T2K: Field model.dims.knots not found in model.dims')

knots       =	model.knots         ;
nk          =	model.dims.knots	; 

if ~isempty(T)
    assert(min(T(:))>=knots(1),     'Error in T2K: Not all elements in T are above or equal to the first knot (model.knots(1))')
    assert(max(T(:))<=knots(end),   'Error in T2K: Not all elements in T are below or equal to the last knot (model.knots(end))')
end

% ========================================================================
%   TRANSFORMATION
% ========================================================================


% Pre-allocation
[I,J]	=   size(T)     ;
K       =	nan(I,J)    ;

for i=1:I
    for j=1:J
        % For every element of T, t, ...
        
        
        % Find first knot which is equal to or after the considered t
        t       =	T(i,j)                      ;
        index	=	find(t>=knots,1,'first')	;
        if index==nk
            % IF: the identified knot is the last knot, 
            % THEN: the K-value is equal to this knot's index
            k	=	index                       ;
        else
            % ELSE: The K-value is between two knot indices:
            f	=	( t-knots(index) )/( knots(index+1)-knots(index) )	;
            k	=	index + f                                           ;
        end
        
        % Store value:
        K(i,j)	=	k           ;
    end
end
