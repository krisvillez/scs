function model  =  SCSsetup(y,t,degree,knots,lambda,twindow) 

% -------------------------------------------------------------------------
% SCS toolbox - SCSsetup.m
% -------------------------------------------------------------------------
% Description
%   This function produces the basic information about the shape
%   constrained spline model in the form of a structure. 
%
% -------------------------------------------------------------------------
% Syntax:   model  =  SCSsetup(y,t,degree,knots,lambda,twindow) 
%
% Inputs:
% 	y           Dependent variable measurement matrix [MxN]
%   t           Independent variable column vector [Mx1]
%   degree  (x)	Degree of polynomial (currently overwritten to be 3) [1x1]
%               (+ degree=3)
%   knots   (x)	Knot positions [Kx1] (+ knot=t )
%   lambda  (x)	Smoothing penalty meta-parameter [1x1] (+ lambda=10^-7)
%   twindow	(x)	Windows of the independent variable over which the smoothing
%               penalty is computed. Absent or empty entry means
%               penalty is computed over whole function domain. [Wx2] 
%               (+ twindow=[]) 
%
%   (x)     Optional input
%   (+)     Default value
%
% Outputs:
%   model   Shape Constrained Spline model
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-06-09
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% ========================================================================
%   DEFAULT PARAMETER VALUES
% ========================================================================
def_lambda	=	10^-7	;   % default smoothing penalty meta-parameter
def_degree	=	3       ;   % default degree of the spline basis


% ========================================================================
%   DATA INPUTS
% ========================================================================

% checks dimension of t/y:
assert(length(size(y))==2,	'Error in SCSsetup: Output data matrix has more than 2 dimensions.')
assert(size(t,2)==1,        'Error in SCSsetup: Input data matrix is not a column vector.')

% get length of the time series:
dimt	=   find(size(t)~=1)            ;

% check that dimensions of t and y match:
assert(any(size(y)==size(t,dimt)),  'Error in SCSsetup: Input and output data matrix must share at least a length in one dimension.')

% ensure that t is a column vector and that the columns of y have the same
% dimensions as t:
dimy    =   find(size(y)==size(t,dimt))	;
if dimt==2,	t = t'	;   end
if dimy==2, y = y'	;   end

nVarOut  =  size(y,2) ;


% ========================================================================
%   MODEL STRUCTURE INPUTS
% ========================================================================

% apply default degree if not provided:
if nargin<3 || isempty(degree),     degree = 3 ;    end

% check that the degree is equal to 3, since it is the only supported
% option right now. Override if necessary:
if degree~=3
    disp('-> ModelSetup : Current version of the toolbox only supports spline functions of order 4, i.e. of polynomial degree 3. The degree is set to 3.')
 	degree	=	def_degree      ;
end

% use default knot placements if not provided (smoothing spline):
if nargin<4 || isempty(knots)
    disp('-> ModelSetup : Knots not provided - Assumed smoothing spline model (knots placed in every data point).')
    knots	=	t(1:1:end)      ;
end
knots       =	sort(knots(:))	;

% check that there are no knots with multiplicity at the function domain
% boundaries:
assert(sum(knots(1)==knots)==1,...
    'Error in SCSsetup: There are too many knots at the left side of the domain')
assert(sum(knots(end)==knots)==1,...
    'Error in SCSsetup: There are too many knots at the right side of the domain')


% apply default smoothing penalty parameter if necessary:
if nargin<5 || isempty(lambda)
    lambda = def_lambda;
end

% define domain and order as distinct variables:
domain	=	[	knots(1)	knots(end)	]   ;
order	=       degree + 1                  ;


% ========================================================================
%   SPLINE BASIS SETUP AND EVALUATIONS
% ========================================================================

% --------------------------------------------------------------------
%   functional basis
% --------------------------------------------------------------------

% meta-information
nKnot       =	length(knots)	;   % number of knots
nSegment	=	nKnot-1         ;   % number of inter-knot segments
nSample     =	size(y,1)       ;   % number of data samples

% create functional basis object
basisobj	=	create_bspline_basis(domain,[],order,knots) ;

% number of regression coefficients
nAlpha      =	0                  	;   % not in spline
nBeta       =	getnbasis(basisobj) ;   % in spline
nGamma      =	nAlpha+nBeta        ;   % total

% --------------------------------------------------------------------
%   compute smoothing penalty matrix
% --------------------------------------------------------------------
if nargin<6 || isempty(twindow)
    R       =   eval_penalty(basisobj,degree-1)                     ;
else
    R       =   zeros(nBeta)                                        ;
    for iwin=1:size(twindow,1)
        R	=	R+eval_penalty(basisobj,degree-1,twindow(iwin,:))	;
    end
end
% normalization of smoothing penalty matrix - this means that changes in
% the scale of t does not affect the value of the smoothness penalty:
R           =	R*( (t(end)-t(1))/(length(knots)-1) ).^6            ;

% --------------------------------------------------------------------
%   evaluate the basis functions in the knots 
% --------------------------------------------------------------------
Xk          =	cell(degree+1,1)                ;
Xt          =	cell(degree+1,1)                ;
for d=0:(degree)
    Xk{d+1} =   eval_basis(knots,basisobj,d)	;
    Xt{d+1} =   eval_basis(t,basisobj,d)        ;
end


% ========================================================================
%   UNCONSTRAINED SOLUTION
% ========================================================================

% --------------------------------------------------------------------
%   Design matrix
% --------------------------------------------------------------------
Z               =   Xt{1}   ;
[m,n]           =	size(Z) ;

% --------------------------------------------------------------------
%   Update lambda if necessary
% --------------------------------------------------------------------
if m<n && lambda==0
    lambda          =   def_lambda	;
    model.lambda    =   lambda      ;
    disp('-> ModelSetup : ')
    disp('  Autocorrect: The number of basis functions is higher than the number of data points.')
    disp('               Switching to a smoothing spline setup.')
    disp(['               lambda penalty parameter set to ' num2str(lambda) '.'])
end

% --------------------------------------------------------------------
%   Compute solution as well as the centering and rotation needed to search
%   for the regression coefficients in whitened space
% --------------------------------------------------------------------
gamma_unc	=	nan(nGamma,nVarOut) ;

% get to the projection/reconstruction matrices:
W           =	Z'*Z+lambda*R   ;
W           =   (W+W')/2        ;
[L,p]       =   chol(W)         ;
if p~=0
    W       =	W+10^(-9)*trace(W)*eye(size(W)) ;
end
L           =   chol(W)         ;   % projection matrix (whitening)
M           =	(L'*L)^(-1)*L'	;   % reconstruction matrix (whitening)

% compute reference solution
for iVarOut=1:nVarOut
    method	=   1;
    switch method
        case 1 % naive solution
            gamma_unc(:,iVarOut)	=   RidgeRegression(Z,y(:,iVarOut),R,lambda,method)	;
        case 2 % via Cholesky factorization
            [L,p]                   =   chol(R)                                         ;
            gamma_unc(:,iVarOut)	=   RidgeRegression(Z,y(:,iVarOut),L,lambda,method)	;
    end
    
    % Store f-vector (see quadprog.m for meaning)
    if nVarOut>1,   f{iVarOut}      =   -y(:,iVarOut)'*Z    ;
    else,           f               =   -y(:)'*Z            ;
    end
        
end


% ========================================================================
%   STORE INFORMATION
% ========================================================================

% model description
model.type      =   'SCS'       ;
model.string = 'Q';

% spline basis
model.basisobj  =   basisobj	;   % spline basis object
model.Xk        =   Xk          ;
model.Xt        =   Xt          ;

% modelled data
model.t         =	t           ;
model.y         =	y           ;

% parameters defining the fit and penalty objectives
model.R         =   R           ;
model.R0        =	R           ;
model.L         =   L           ;
model.L0        =	L           ;
model.M         =	M           ;
model.Z         =	Z           ;
model.W         =	W           ;
model.f         =	f           ;

% meta-parameters 
model.domain    =   domain      ;
model.knots     =   knots       ; 
model.lambda    =   lambda      ;

% current solution
model.alpha     =	[]          ;
model.alpha_unc	=	[]          ;
model.beta      =   gamma_unc   ;
model.beta_unc	=	gamma_unc	;
model.gamma     =   gamma_unc   ;
model.gamma_unc	=	gamma_unc	;

% model dimension parameters
model.dims.alpha        =	nAlpha      ;
model.dims.beta         =	nBeta       ;
model.dims.gamma        =	nGamma      ;
model.dims.degree       =   degree      ;
model.dims.knots        =	nKnot       ;
model.dims.order        =	order       ;
model.dims.sample       =	nSample     ;
model.dims.segment      =	nSegment	;
model.dims.contin_eq	=   0           ;
model.dims.nVarOut      =   nVarOut     ;

% ========================================================================
%   NEEDE TO SOLVE SCS-FITTING AS A CONIC OPTIMIZATION PROBLEM
% ========================================================================
model	=	PrecomputeConstraints(model)    ;

end

% ***********************************************************************
% ***********************************************************************
