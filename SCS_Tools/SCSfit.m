function model  =  SCSfit(model,TransK,Episode)

% -------------------------------------------------------------------------
% SCS toolbox - SCSfit.m
% -------------------------------------------------------------------------
% Description
%   This function solves the shape constrained spline fitting problem.
%
% -------------------------------------------------------------------------
% Syntax:   model  =  SCSfit(model,TransK,Episode)
%
% Inputs:
%   model       Shape Constrained Spline model
%   TransK	(x)	Transition positions expressed relative to the knot
%                positions (+ TransK=[])
%   Episode	(x)	Assignment of knots to episodes in the qualitative sequence
%                (+ Episode=[]) 
%
%   (x)     Optional input
%   (+)     Default value
%
% Outputs:
% 	model       Shape Constrained Spline model (updated)
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-06-09
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% ========================================================================
%   Basic input checks
% ========================================================================

if nargin<2 || isempty(TransK),     TransK	=   [   ]	;   end
if nargin<3 || isempty(Episode),	Episode	=   [   ]	;   end

TransK      =   TransK(:)	;
Episode     =   Episode(:)	;


% ------------------------------------------------------------------------
assert(isfield(model,'dims'),       'Error in SCSfit: Field model.dims not found in model')
assert(isfield(model.dims,'beta'),  'Error in SCSfit: Field model.dims.beta not found in model.dims')
assert(isfield(model.dims,'knots'),	'Error in SCSfit: Field model.dims.knots not found in model.dims')
% ------------------------------------------------------------------------

nk              =	model.dims.knots            ;

% ------------------------------------------------------------------------
if ~isempty(TransK)
    assert(min(TransK(:))>=1,	'Error in SCSfit: Not all elements in Knots are above or equal to the maximum number of knots (model.dims.knots)')
    assert(max(TransK(:))<=nk,  'Error in SCSfit: Not all elements in Knots are below or equal to the maximum number of knots (model.dims.knots)')
end
% ------------------------------------------------------------------------

global softwareoptim

if exist('softwareoptim','var') && ~isempty(softwareoptim)
    soft = softwareoptim;
else
    soft = 'CVX' ;
end


% ========================================================================
%   Recompute model setup if necessary:
% ========================================================================
if strcmp(model.type,'SCSD')
    
    basisrecomputed = ( isfield(model,'basisrecomputed') && model.basisrecomputed ) ;
    if ~basisrecomputed
        assert(~isempty(TransK),'Error in SCSfit: For SCSD models, the transition positions must be given')
        model 	=	ApplyDiscon(model,TransK)       ;
        Episode	=	model.episodes                  ;
    end
    
end

% ========================================================================
% !!! DO NOT MOVE THESE LINES OF CODE !!! 
% This must be computed with the latest model structure as (possibly)
% computed in the section above
% ========================================================================
nAlpha          =	model.dims.alpha	;
nBeta           =	model.dims.beta     ;
nGamma          =	model.dims.gamma    ;
nVarOut         =   model.dims.nVarOut  ;

model.transknot	=	TransK              ;
model.transtime =	K2T(model,TransK)   ;

assert(nAlpha+nBeta==nGamma,'Error in SCSfit: Shape constrained spline model wrongly defined. It must hold that model.dims.alpha+model.dims.beta=model.dims.gamma')

% ========================================================================
%   Pre-allocation
% ========================================================================
gamma           =	nan( nGamma	,   nVarOut	)   ;
beta            =	nan( nBeta	,   nVarOut	)   ;
alpha           =	nan( nAlpha	,   nVarOut	)   ;

% "Strict" means that shape constraints are applied in the exact locations
% of the transitions. This is necessary for multivariate time series
% without discontinuities.
Strict          =	(nVarOut>1 && ~isempty(TransK)) && strcmp(model.type,'SCS');

for iVarOut=1:nVarOut
    
    % ========================================================================
    %   Setup MOSEK problem formulation
    % ========================================================================
    if isfield(model,'QS')
        
        if model.dims.transitions~=0
            assert( any(~[isempty(Episode) isempty(TransK) ]),'Error in SCSfit: Neither Episode or TransK have been specified. One of them is necessary when the field QS is provided in model.QS' )
        end
        
        if nargin<3 && isempty(Episode) 
            % "AND" condition strictly necessary, if Episode is computed
            % above in the section "Recompute model setup if necessary:"
            % then it ought not be recomputed!   
            Episode		=	GetEpisodes(model,TransK)	;
            model.episodes  =	Episode                    ;
        end
        
        if Strict
            [Aeq,beq,Aineq,bineq,nSeg0,nSeg1] = SetupQCS(model,Episode,iVarOut,TransK);
        else
            [Aeq,beq,Aineq,bineq,nSeg0,nSeg1] = SetupQCS(model,Episode,iVarOut);
        end
    else
        Aeq     =	sparse( 0   ,   model.dims.gamma	)   ;
        beq     =   sparse( 0   ,   1                   )   ;
        Aineq   =	sparse( 0   ,   model.dims.gamma	)   ;
        bineq   =	sparse( 0   ,   1                   )   ;
        nSeg0   =	0       ;
        nSeg1   =	0       ;
        
    end
    % ========================================================================
    
    switch soft
        case {'CVX','cvx'}
            % ************************************************
            %   Solve problem with CVX
            % ************************************************
            
            cvx_begin
%                 cvx_quiet true
                variables delta1(nGamma,1) C_0(6,nSeg0)  C_1(4,nSeg1)
                minimize norm( delta1 )
                subject to
                	Aineq*[delta1 ; C_0(:) ; C_1(:) ] >= bineq
                    Aeq*[delta1 ; C_0(:) ; C_1(:) ] == beq
                    for iSeg=1:nSeg0
                        { [ C_0(2:3,iSeg) ],  C_0(1,iSeg)  } == lorentz(2)
                        { [ C_0(5:6,iSeg) ],  C_0(4,iSeg)  } == lorentz(2)
                    end
                    for iSeg=1:nSeg1
                        { [ C_1(2:3,iSeg) ], C_1(1,iSeg)  } == lorentz(2)
                        C_1(4,iSeg)>=0
                    end
            cvx_end
            
            delta	=	delta1  ;
            % ************************************************
            
        case {'mosek71'}
            
            % ************************************************
            %   Solve problem with MOSEK
            % ************************************************
            
            try
                clear prob ;
                [r, res] = mosekopt('symbcon echo(0)');
            catch
                clear prob ;
                [r, res] = mosekopt('symbcon echo(3)');
            end
            
            A	=	[	Aeq	;	Aineq	]   ;
            b	=	[	beq	;	bineq	]   ;
            
            [ nCon	, nVar ] =  size(A)     ;
            [ nConIn, nVar ] =  size(Aineq) ;
            
            types	=	repmat(res.symbcon.MSK_CT_QUAD,[1 1+nSeg0*2+nSeg1])                         ;
            cc      =   setdiff(1:nSeg1*4,(1:nSeg1)*4)                                              ;
            sub     = [	1:(nGamma+nSeg0*6+1)	nGamma+nSeg0*6+1+cc                   ]	;
            subptr	= [	1	nGamma+1+(1:3:nSeg0*6) nGamma+nSeg0*6+1+3*(1:nSeg1)-2	]   ;
            
            c       =   [   1	zeros(1,nVar)       ]   ;
            a       =   [	sparse(nCon,1)      A	]	;
            blc     =       b                           ;
            buc     =	[	beq	;	inf(nConIn,1)	]   ;
            
            prob.a              =	a       ;
            prob.blc            =   blc     ;
            prob.buc            =	buc     ;
            prob.c              =	c       ;
            
            prob.cones.type     =	types   ;
            prob.cones.sub      =   sub     ;
            prob.cones.subptr   =   subptr  ;
            
            try, 	[r,res]=mosekopt('minimize echo(0)',prob);
            catch,  [r,res]=mosekopt('minimize echo(3)',prob);
            end
            
            delta	=	res.sol.itr.xx      ;
            delta	=	delta(2:end)        ;
            
    end
    
    % ========================================================================
    %   Process and store solution
    
    % M and beta_unc are used to transform the solution in the whitened
    % coefficient space into the original coefficient space:
    M                   =   model.M                         ;
    gamma_unc           =	model.gamma_unc(:,iVarOut)      ;
    delta               =	delta(1:nGamma)                 ;
    gamma(:,iVarOut)	=	gamma_unc+M*delta               ;
    alpha(:,iVarOut)	=   gamma(nBeta+(1:nAlpha),iVarOut)	;
    beta(:,iVarOut)     =   gamma(1:nBeta,iVarOut)          ;
    
    % ========================================================================
    
end


% ========================================================================
% 	Process and store model performance
% ========================================================================

model.alpha	=	alpha               ;
model.beta	=	beta                ;
model.gamma	=	gamma               ;

% fit
resid       =   model.y-model.Z*gamma   ;
fit         =   sum(resid(:).^2)        ;

% smoothness penalty
penalty     =   0                       ;
for iVarOut=1:nVarOut
    penalty = penalty + model.lambda*gamma(:,iVarOut)'*model.R*gamma(:,iVarOut)	;
end

% store:
N                           =	model.dims.sample*nVarOut	;

model.performance.fit       =   fit/N                       ;
model.performance.penalty   =   penalty/N                   ;
model.performance.objective	=   (fit+penalty)/N             ;

% ***********************************************************************
% ***********************************************************************

end