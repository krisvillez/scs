
% -------------------------------------------------------------------------
% SCS toolbox - TryMOSEK.m
% -------------------------------------------------------------------------
% Description 
%
% This script tests the MOSEK optimization software by trying a small test
% example adapted from the qco1.m example on the MOSEK website.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	TryMOSEK()
%
%
% INPUT
%   N/A
%
% OUTPUT
%   N/A
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-06-09
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2019 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
clear all
close all

MosekInstallDir = 'C:\Program Files\Mosek71\7\toolbox\r2013a\';

if ~exist(MosekInstallDir,'dir')
    fprintf('\nERROR: Directory ''%s'' was not found. The MOSEK folder appears not to be in the Matlab path.',MosekInstallDir)
else
    addpath(MosekInstallDir)
    if ~exist('mosekopt.m','file')
        fprintf('\nERROR: Function ''mosekopt'' was not found.')
        fprintf('\n')
    else
        
        try
            % qco1.m
            
            clear prob;
            
            % Specify the linear objective terms.
            prob.c = ones(3,1);
            
            % Specify the quadratic objective terms.
            prob.qcsubk = [1 1 1 2 ]';
            prob.qcsubi = [1 2 3 2 ]';
            prob.qcsubj = [1 2 3 2 ]';
            prob.qcval = [2.0 2.0 2.0 0.2]';
            
            % Specify the linear constraint matrix
            prob.a = [sparse(1,3);sparse([1 0 1])];
            
            prob.buc = [1 0.5]';
            
            [r,res] = mosekopt('minimize echo(3)',prob);
            
            % Display the solution.
            fprintf('\nx:');
            fprintf(' %-.4e',res.sol.itr.xx');
            fprintf('\n||x||: %-.4e',norm(res.sol.itr.xx));
            fprintf('\n')
            fprintf('\nSUCCESS: MOSEK appears installed correctly')
            fprintf('\n')
        catch
            
            fprintf('\nERROR: MOSEK appears installed incompletely or incorrectly. A valid license may be missing.')
            fprintf('\n')
            
        end
        
        
    end
end
return
