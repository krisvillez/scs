function    model  =  ApplyDiscon(model,TransK)

% -------------------------------------------------------------------------
% SCS toolbox - ApplyDiscon.m
% -------------------------------------------------------------------------
% Description
%   This function modifies the shape constrained spline model structure to
%   account for the imposed appearance of discontinuities in the transition
%   positions. To this end, the set of knots is augmented and all elements
%   of the model are updated to account for this.
%
% -------------------------------------------------------------------------
% Syntax:   model  =  ApplyDiscon(model,TransK)
%
% Inputs:
%   model	Shape Constrained Spline model
%   TransK 	Transition positions expressed relative to the spline knot
%           positions 
%
% Outputs:
% 	model	Shape Constrained Spline model (updated)
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2018.01.25
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

modelZERO   =   model                       ;

% ========================================================================
%   DIMENSIONS
% ========================================================================
nEpisode    =	modelZERO.dims.episodes     ;
nTrans      =	modelZERO.dims.transitions	;

% ========================================================================
%   INPUT CHECKS
% ========================================================================

BOOL        =   size(TransK,1)==nTrans      ;
assert(BOOL,'Error in ApplyDiscon: Row dimension of TransK does not match model.dims.transitions')

J           =	size(TransK,2)              ;
BOOL        =   or(J==1,J==2)               ;
assert(BOOL,'Error in ApplyDiscon: Column dimension of TransK is not 1 or 2')

% ========================================================================
%   INPUT VALUES
% ========================================================================
y           =   modelZERO.y                         ;
t           =   modelZERO.t                         ;
degree      =   modelZERO.dims.degree               ;
nKnot       =	modelZERO.dims.knots                ;
nSample     =	modelZERO.dims.sample               ;
knots       =   modelZERO.knots                     ;
lambda      =   modelZERO.lambda                    ;
String      =   modelZERO.string                    ;


% ========================================================================
%   COMPUTE NEW KNOTS
% ========================================================================

[QS,Disc]       =   String2QS(String)	;
multipli        =	degree+1            ;

% --------------------------------------------------------------------
%   Pre-allocation
% --------------------------------------------------------------------
TimeKnotAdd     =	nan(sum(any(Disc(1:end-1,:),2))*multipli,J) ;
TimeSolution	=	nan(nTrans,J)                   ;

% --------------------------------------------------------------------
%   Add knots
% --------------------------------------------------------------------
for j=1:J
    iAdd = 0 ;
    for iTrans=1:nTrans
        
        % Get knot index value of transition
        if J==1,    TimeK	=	TransK(iTrans)	;
        else,       TimeK	=	TransK(iTrans,j) ;
        end
        
        % identify closest existing knot before transition:
        iKnot       =	floor(TimeK)	;
        
        if iKnot==nKnot
            % if the transition is placed at the end, store the
            % knot's position as solution
            TimeSolution(iTrans,j) = knots(iKnot);
        else
            % if the transition is placed elsewhere, compute it:
            TimeSolution(iTrans,j) = knots(iKnot)+( TimeK-iKnot )/( 1) * ( knots(iKnot+1)-knots(iKnot) ) ;
        end
        
        % if the considered transition corresponding to a discontinuity,
        % add additional knots to the set of knots:
        if any(Disc(iTrans,:))>0
            TimeKnotAdd(iAdd+(1:multipli),j)	=	TimeSolution(iTrans,j)	;
            iAdd                                =	iAdd + multipli         ;
        end
    end
end

% gather and sort all knots:
knots           =   sort([   knots(:)    ;   TimeKnotAdd(:) ]);


% ========================================================================
%   WARPED INPUT SCALE
% ========================================================================

% --------------------------------------------------------------------
%   Compute new values for warped input variables
% --------------------------------------------------------------------

% start with previous situation
u_knots         =	knots           ;
u_t             =	t               ;
nk              =	length(knots)	;
u_TimeSolution	=	TimeSolution	;

shift           =	1               ;

for ik=nk:-1:2 % run backward over all knots
    if knots(ik-1)==knots(ik) 
        % if the predecessor knot has the same value as the considered
        % knot, shift the new time scale with 1 unit:
        u_knots(ik:end)     =	u_knots(ik:end)+shift       ;
        u_t(t>=knots(ik))	=	u_t(t>=knots(ik))+shift     ;
        % apply the same warping for the transitions:
        for j=1:J
            bool                    =   TimeSolution(:,j)>=knots(ik)    ;
            u_TimeSolution(bool,j)	=	u_TimeSolution(bool,j)+shift	;
        end
    end
end


% u_window  =   [	u_knots(1)	u_knots(end)	]   ;
% u_window
% for iTrans=1:nTrans ; %:-1:1
%     index1      =       min(find(knots==TimeSolution(iTrans,1))) ;
%     index2      =       max(find(knots==TimeSolution(iTrans,J))) ;
%  	u_window	=   [   u_window(1:iTrans,:)	;
%                         u_window(iTrans:end,:)	];
%     u_window(iTrans,2)	=   u_knots(index1)	;
%     u_window(iTrans+1,1)	=   u_knots(index2)	;
%     
% end
    
if J==2
    % For lower bound compuation; identify which data samples should be
    % omitted for proper lower bound computation
    
    % Pre-allocation
    include     =       true(nSample,1)                 ;
    
    % Run over the transitions and exclude data samples inside the
    % corresponding intervals
    for iTrans=1:nTrans
        rule1           =   u_t>u_TimeSolution(iTrans,1)    ;
        rule2           =   u_t<u_TimeSolution(iTrans,2)    ;
        bool            =	and(rule1,rule2)                ;
        include(bool)	=	false                           ;
    end
    u_t         =	u_t(include)    ;
    y           =	y(include)      ;
    
    % Update model structure for new set of knots and reduced data samples:
    modelONE	=	SCSsetup(y,u_t,degree,u_knots,lambda) ;
    
    %modelONE	=	ModelSetup(y,u_t,degree,u_knots,lambda,u_window) ;
else
    
    % Update model structure for new set of knots
    modelONE	=	SCSsetup(y,u_t,degree,u_knots,lambda) ;
    
    %modelONE	=	ModelSetup(y,u_t,degree,u_knots,lambda,u_window) ;
    
end

% Add 'warp' field to the model so that warping and unwarping is always
% possible
Warp            =	[	knots	u_knots	]	;
modelONE.warp	=       Warp                ;

% --------------------------------------------------------------------
%   Compute new values for transitions according to new input scale
% --------------------------------------------------------------------
KnotSolution	=	nan(nTrans,J)	;

for j=1:J
    for iTrans=1:nTrans
        if any(Disc(iTrans,:))
            iKnot                       =	find(  knots==TimeSolution(iTrans,j),1,'last')-1	;
            KnotSolution(iTrans,j)      =   iKnot                                               ;
        else
            iKnot                       =	find( knots<=TimeSolution(iTrans,j) , 1, 'last'  )	;
            if iKnot==length(knots)
                KnotSolution(iTrans,j)	=	iKnot ;
            else
                frac                    =   (TimeSolution(iTrans,j)-knots(iKnot) ) /( knots(iKnot+1)-knots(iKnot) ) ;
                KnotSolution(iTrans,j)	=	iKnot+frac   ;
            end
        end
    end
end

% ========================================================================
%   Update model
% ========================================================================

% Apply qualitative sequence
modelONE	=	ApplyQS(modelONE,String)             ;

% Compute Episode, vector indicating to which episode each knot belongs
nk          =	modelONE.dims.knots	;
Episode     =   nan(nk,J)                   ;

for j=1:J
    for ik = 1:nk
        iEpisode            =	FindEpisode(ik,KnotSolution(:,j),nEpisode) ;
        if or(ik==1,ik==nk) || or(knots(ik)~=knots(ik-1),knots(ik)~=knots(ik+1))
            % Assign found episode number only if (1) the knot is on the
            % boundary of the function domain or (2) the knot is not in the
            % same location as both its predecessor and its successor. The
            % knots placed "inside" a transition with discontinuity do not
            % belong to any episode.
            Episode(ik,j)	=	max(iEpisode) ;
        end
    end
end

switch J
    case 1
        % Upper bound solution. Continuity constraints are added to the
        % model setup.
        
        basisobj	=	modelONE.basisobj       ;
        ncoeff      =	getnbasis(basisobj)     ;
        Acon        =	zeros(0,ncoeff)         ;
        bcon        =	zeros(0)                ;
        for iTrans=1:nTrans
            if any(Disc(iTrans,:))
                for d=0:(degree-1)
                    if ~Disc(iTrans,d+1)
                        % Add a continuity constraint for the d-th
                        % derivative in a transition with discontinuities
                        % if the d-th derivative is not the one that has a
                        % discontinuity.
                        
                        % Find the first and last knot corresponding to the
                        % transition
                        index	=   find(u_knots(KnotSolution(iTrans))==u_knots(:)');
                        index	=   find(knots(index)==knots)       ;
                        index	=	[	min(index)	max(index)	]   ;
                        u_equal =       u_knots(index)              ;
                        
                        % The derivative value must be the same in these
                        % two knots
                        X       =	eval_basis(basisobj,u_equal,d)  ;
                        D       =	X(1,:)-X(2,:)                   ;
                        Acon	=	[	Acon ; D	]               ;
                        bcon	=	[	bcon ; 0	]               ;
                    end
                end
            end
            
        end
        modelONE.con.a_contin_beta	=	Acon            ;
        modelONE.con.b_contin       =	bcon            ;
        modelONE.dims.contin_eq     =   length(bcon)	;
    case 2
        % if lower bound solution is sought
        % Find knot locations which are assigned to different episodes pending
        % which bounding value for the transition intervals is used
        bool                =   Episode(:,1)~=Episode(:,2)	;
        % Indicate that their episode cannot be identified
        Episode( bool ,1 )	=	nan                         ;
        % Reduce Episode to a single vector
        Episode             =	Episode(:,1)                ;
end

modelONE.episodes       =	Episode                     ;

model   = modelONE	;

end


% ***********************************************************************
% ***********************************************************************