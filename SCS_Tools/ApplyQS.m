function model  =  ApplyQS(model,String)

% -------------------------------------------------------------------------
% SCS toolbox - ApplyQS.m
% -------------------------------------------------------------------------
% Description
%   This function modifies the shape constrained spline model by providing
%   additional information related to the imposed qualitative sequence.
%
% -------------------------------------------------------------------------
% Syntax:   model  =  ApplyQS(model,String)
%
% Inputs:
%   model       Shape Constrained Spline model
%   String      Qualitative sequence expressed as string
%
% Outputs:
%	model       Shape Constrained Spline model (updated)
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2018.01.25
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% ========================================================================
%	PROVIDE QUALITATIVE SEQUENCE IN MATRIX FORM
% ========================================================================
if ischar(String)
    [QS,Disc]       =	String2QS(String)	;   % Convert string format of the qualitative sequence to matrix form
    model.string	=	String              ;   
else
    QS              =   String              ;
    
    assert(size(QS,2)==model.dims.degree,...
        'Error in ApplyQS: Column dimension of the matrix QS do not match the degree of the spline basis.')
end


% ========================================================================
%	MODEL CHECKS
% ========================================================================
if model.dims.nVarOut>1 && any(Disc(:))
    warning('ApplyQS: The fitting of shape constrained splines with with discontiuities to multivariate data series is currently not tested. Continue at your own peril.')
end

% ========================================================================
%   COMPUTE META-INFORMATION
% ========================================================================
lambda              =   model.lambda	;
if any(Disc(:)),    
    type            =	'SCSD'          ;
    if lambda==0,   
        lambda      =   10^(-7)         ;  
    end
else
    type            =	model.type      ;
end
nEpisode            =	size(QS,1)      ;

assert(nEpisode<model.dims.knots,'Error in ApplyQS: More episodes in QS than knot intervals in spline basis')


% ========================================================================
%   STORE INFORMATION
% ========================================================================
model.Disc              =   Disc            ;
model.QS                =	QS              ;
model.dims.episodes     =   nEpisode        ;
model.dims.transitions	=   nEpisode-1      ;
model.lambda           	=   lambda          ;
model.type           	=   type            ;

end

% ***********************************************************************
% ***********************************************************************

