function	tgrid  =  GenerateGrid(model,n)

% -------------------------------------------------------------------------
% SCS toolbox - GenerateGrid.m
% -------------------------------------------------------------------------
% Description
%   The function creates a grid of independent variable values which are
%   spaced evenly with each inter-knot segment.
%
% -------------------------------------------------------------------------
% Syntax:   tgrid  =  GenerateGrid(model,n)
%
% Inputs:
%   model       Shape Constrained Spline model
%   n           Number of values per inter-knot segment
%
% Outputs:
%	tgrid       Vector of independent values
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-06-09
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

assert(n>=1,'The number of samples per inter-knot interval must be equal to or higher than 1.')
n   =	floor(n) ; % making sure its an integer

if n==1 % only one sample per interval -> simply take the knots
    tgrid	=   model.knots                                     ;
else
    t       =   model.knots                                     ;
    tgrid	=	repmat(t(1:end-1),[1 n-1])+diff(t)*(1:n-1)/n	;
    tgrid	=	[ t(1);	tgrid(:) ;	t(end)	]                       ;
    tgrid   =   unique(sort(tgrid))                             ;
end