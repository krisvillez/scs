function Episode  =  GetEpisodes(model,Knots)	

% -------------------------------------------------------------------------
% SCS toolbox - GetEpisodes.m
% -------------------------------------------------------------------------
% Description
%   The function identifies the applicable episode number for a set of
%   values for the independent variable expressed relative to the spline
%   knots.
%
% -------------------------------------------------------------------------
% Syntax:   Episode  =  GetEpisodes(model,Knots)	
%
% Inputs:
%   model       Shape Constrained Spline model
%   Knots       Independent variable values expressed relative to the
%                spline knot positions
%
% Outputs:
%	Episode     Assigments of independent variable values to episodes
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2018.01.25
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% =======================================================================
%   INPUTS
% =======================================================================

% --------------------------------------------------------------------
%   Input checks
% --------------------------------------------------------------------

assert(isfield(model,'dims'),'Error in GetEpisodes: Field model.dims not found in model')
assert(isfield(model.dims,'knots'),'Error in GetEpisodes: Field model.dims.knots not found in model.dims')
assert(isfield(model.dims,'episodes'),'Error in GetEpisodes: Field model.dims.episodes not found in model.dims')

% --------------------------------------------------------------------
%   Dimensions
% --------------------------------------------------------------------

nKnot       =   model.dims.knots    ;
nEpisode	=   model.dims.episodes ;

% =======================================================================
%   COMPUTE EPISODES
% =======================================================================

%   Episode is a vector with length equal to the number of spline knots.
%   Each element in Episode is set equal to the value of the episode to
%   which the corresponding spline knot belongs. NaN values are assigned if
%   the knot cannot be located inside any of the episodes.

if isempty(Knots) 
    % No transitions => Only one episode possible
    Episode     =	ones(nKnot,1)           ;
else
    
    % list the knot indices
    knotsindex	=	1:nKnot                ;
    
    % J=2 means the knots are specified as intervals (for lower bound
    % computation) 
    J           =   size(Knots,2)          ; 
    
    % Initialize all as NaN
    Episode     =	nan(nKnot,1)           ;
    
    % For every knot ...
    for ik = 1:nKnot
        switch J
            case 1
                % Find episode the ik-th knot belongs to
                iEpisode        =	FindEpisode(ik,Knots,nEpisode) ;
            case 2
                if any(and( knotsindex(ik)>Knots(:,1),knotsindex(ik)<Knots(:,2) ))
                    % If the knot belongs to an interval for any
                    % transition, then the knot cannot be assigned to a
                    % particular episode 
                    iEpisode	=	nan     ;
                else
                    % Find episode the ik-th knot belongs to
                    iEpisode	=	FindEpisode(knotsindex(ik),Knots,nEpisode) ;
                end
        end
        % Store reported episode:
        Episode(ik,1)	=	iEpisode	;
    end
end

% ***********************************************************************
% ***********************************************************************
