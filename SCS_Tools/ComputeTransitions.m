function [TransKnot,TransTime,TransInt]  =  ComputeTransitions(model)

% -------------------------------------------------------------------------
% SCS toolbox - ComputeTransitions.m
% -------------------------------------------------------------------------
% Description
%   Given a fitted model, this function computes the positions of the
%   transitions as precisely as possible.
%
% -------------------------------------------------------------------------
% Syntax:   [TransKnot,TransTime,TransInt]  =  ComputeTransitions(model)
%
% Inputs:
%   model       Shape Constrained Spline model
%
% Outputs:
% 	TransKnot	Transition positions expressed relative to the spline knot
%                positions 
%   TransTime	Transition positions expressed in original scale of the
%                independent variable 
%   TransInt	Transition position intervals expressed in original scale
%                of the independent variable 
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2018.01.25
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% Hard-coded option - used for debugging
display     =	false                   ;

% ========================================================================
%   DIMENSIONS
% ========================================================================
degree      =	model.dims.degree       ;
nTrans      =   model.dims.transitions	;

% ========================================================================
%   INPUT VALUES
% ========================================================================

basisobj	=	model.basisobj          ;
beta        =	model.beta(:,end)       ;
discon      =	model.Disc              ;
knots       =	model.knots             ;
Xk          =	model.Xk                ;
QS          =	model.QS                ;
type        =	model.type              ;

% ========================================================================
%   PRE-ALLOCATION
% ========================================================================
TransKnot	=	nan(nTrans,1)           ;
TransTime	=	nan(nTrans,1)           ;
TransInt	=	nan(nTrans,2)           ;

if isfield(model,'episodes')
    
    Episodes	=	model.episodes      ;
    
    if strcmp(model,'SCS') && any( isnan(Episodes) )
        disp('Some NaN present. Skipping computation.')
    else
        
        for iTrans=1:nTrans
            if strcmp(type,'SCSD') && any(discon(iTrans,:))
                % if the considered transition implies a discontinuity then
                % the knot corresponding to last occurrence of the episode
                % preceeding this transition equals the transition that is
                % sought. This is by virtue of construction of the SCSD
                % model type, i.e. the transitions with discontinuities
                % lead to the addition of knots at their exact location.
                loc                 =	find(Episodes==iTrans,1,'last') ;
                tk                  =	knots(loc)                      ;
                TransKnot(iTrans)	= 	loc                             ;
                TransTime(iTrans)	=	tk                              ;
            else
                
                % find locations in Episodes where an episode change occurs
                % corresponding to the considered transition
                BOOL1	=   Episodes(1:end-1)==iTrans	;
                BOOL2	=   Episodes(2:end)==iTrans+1	;
                loc     =	find(and(BOOL1,BOOL2))      ;
                tk      =	knots(loc+(0:1))            ;
                
                % knot interval within which the transition occurs
                TransInt(iTrans,:)	=	tk              ;

                % check if some of the derivative signs before or after the
                % transition are zero
                AnyZero	=	or(QS(iTrans,:)==0,QS(iTrans+1,:)==0)       ;
                
                % find the derivatives that undergo a sign change, apart
                % from NaN values or zero-valued derivatives
                BOOL1	=   QS(iTrans,~AnyZero)~=QS(iTrans+1,~AnyZero)	;
                BOOL2	=   ~any(isnan(QS(iTrans+(0:1),~AnyZero)),1)    ; 
                drv     =	-1+find( and( BOOL1, BOOL2 ) )              ;
                
                if isempty(drv)
                    % no derivative could be found
                    BOOL1	=	any(QS(iTrans,:)==0)        ;
                    BOOL2	=	any(QS(iTrans+1,:)==0)      ;
                    if and(BOOL1,BOOL2) || ~and(BOOL1,BOOL2)
                        % both episodes have a zero-valued derivative or do
                        % not have any zero-valued derivative => take the
                        % center between the identified knots
                        rts                 =   (tk(2)-tk(1))/2	;
                        TransTime(iTrans)	=   tk(1)   +	rts ;
                        TransKnot(iTrans)   =   loc     +	1/2	;
                    elseif BOOL1
                        % zero-valued derivative before transition => set
                        % the transition equal to the left-most knot
                        TransTime(iTrans)	=   tk(1)	;
                        TransKnot(iTrans)	=   loc     ;
                    elseif BOOL2
                        % zero-valued derivative after transition => set
                        % the transition equal to the right-most knot
                        TransTime(iTrans)   =   tk(2)	;
                        TransKnot(iTrans)	=   loc+1   ;
                    else
                        warning('Error in ComputeTransitions: This statement is tought not to be reachable.')
                    end
                else
                    
                    % Normal case, a single derivative with a sign change
                    % has been found. 
                    % Identify the degree of the considered polynomial in
                    % the knot interval: 
                    polydeg     =	degree-drv	;
                    
                    switch polydeg
                        case 1 % linear trend
                            
                            % Evaluate the polynomial in the knot locations
                            % surrounding the transition
                            X	=	Xk{drv+1}                   ;
                            y2	=	X(loc+(0:1),:)*beta         ;
                            
                            % Compute location of zero-crossing within the
                            % unit-scaled interval
                            f	=	- y2(1) /( (y2(2)-y2(1)))	;
                            
                            % If the location within the interval?
                            tf	=	and(f>=0,f<=1)              ;
                            if tf 
                                % Yes - trivial computation of the
                                % zero-crossing in original scale:
                                TransKnot(iTrans)	=	loc + f                     ;
                                TransTime(iTrans)	=	tk(1) + f *( tk(2)-tk(1) )	;
                            else
                                % No => report the most likely knot of the
                                % two knots
                                [~,select]          =	min(abs(y2))                ;
                                TransKnot(iTrans)	=	loc + select-1              ;
                                TransTime(iTrans)	=	tk(select)                  ;
                            end
                            
                            if display % for debugging
                                figure, hold on,
                                plot(knots,X*beta,'k.-')
                                plot(TransTime(iTrans),0,'ro')
                                title(iTrans)
                            end
                            
                        case 2 % quadratic trend
                            
                            % get polynomial coefficients:
                            X	=	Xk{4}           ;
                            p2	=	X(loc,:)*beta/2	;
                            X	=	Xk{3}           ;
                            p1	=	X(loc,:)*beta   ;
                            X	=	Xk{2}           ;
                            p0	=	X(loc,:)*beta   ;
                            
                            if display % for debugging
                                tsim	=	knots(1):1/n:knots(end) ;
                                tsim	=	round(tsim*n)/n         ;
                                tsim	=	sort(tsim)              ;
                                twindow =	tk(1):1/n:tk(end)       ;
                                
                                X2      =	eval_basis(basisobj,twindow,drv)	;
                                
                                funp    =	p2*(twindow-tk(1)).^2	...
                                            + p1*(twindow-tk(1)).^1	...
                                            + p0*(twindow-tk(1)).^0     ;
                                
                                SCSplot(model,tsim,1)
                                plot(twindow,X2*beta,'r.-')
                                plot(twindow,funp,'ro-')
                                plot(twindow,twindow*0,'m--')
                                title(iTrans)
                            end
                            
                            % find roots of the parabola
                            polyco	=   [   p2	p1	p0  ]               ;
                            rts     =       roots(polyco)               ;
                            tf      =	and(rts>=0,rts<=tk(2)-tk(1))	;
                            
                            if sum(tf)==1 % one root within interval
                                TransTime(iTrans)    =   rts(tf)+tk(1)                  ;
                                TransKnot(iTrans)    =   loc + rts(tf)/(tk(2)-tk(1))	;
                            elseif sum(tf)>1 % more than one root within interval
                                warning('more than one root -- selecting the first one.')
                                rts                 =	rts(tf)                 ;
                                rts                 =	rts(1)                  ;
                                TransTime(iTrans)	=   rts + tk(1)             ;
                                TransKnot(iTrans)	=   loc + rts/(tk(2)-tk(1))	;
                            else % no root
                                warning('optimal location for maximum/minimum could not be found -- setting value to center of interval')
                                rts                 =   (tk(2)-tk(1))/2	;
                                TransTime(iTrans)	=   tk(1)   +	rts ;
                                TransKnot(iTrans)   =   loc     +	1/2	;
                            end
                            
                        case 3 % cubic trend
                            
                            % get polynomial coefficients:
                            X	=	Xk{4}         ;
                            p3	=	X(loc,:)*beta ;
                            X	=	Xk{3}         ;
                            p2	=	X(loc,:)*beta ;
                            X	=	Xk{2}         ;
                            p1	=	X(loc,:)*beta ;
                            X	=	Xk{1}         ;
                            p0	=	X(loc,:)*beta ;
                            
                            if display % for debugging
                                tsim	=	knots(1):1/n:knots(end)     ;
                                tsim	=	round(tsim*n)/n             ;
                                tsim	=	sort(tsim)                  ;
                                twindow =	tk(1):1/n:tk(end)           ;
                                
                                X2      =	eval_basis(basisobj,twindow,0)      ;
                                
                                funp    =	p3/6*(twindow-tk(1)).^3     ...
                                            + p2/2*(twindow-tk(1)).^2	...
                                            + p1*(twindow-tk(1)).^1     ...
                                            + p0*(twindow-tk(1)).^0     ;
                                
                                SCSplot(model,tsim,0)
                                plot(twindow,X2*beta,'r.-')
                                plot(twindow,funp,'ro-')
                                plot(twindow,twindow*0,'m--')
                            end
                            
                            % find roots of the cubic polynomial
                            polyco	=   [   p3/6  p2/2  p1  p0	]       ;
                            rts     =       roots(polyco)               ;
                            tf      =	and(rts>=0,rts<=tk(2)-tk(1))	;
                            
                            if sum(tf)==1 % one root
                                TransTime(iTrans)    =	rts(tf)	+   tk(1)                   ;
                                TransKnot(iTrans)    =	loc     +   rts(tf)/(tk(2)-tk(1))	;
                            elseif sum(tf)>1 % more than one root
                                warning('more than one root -- selecting the first one.')
                                rts = rts(tf) ;
                                rts  = rts(1);
                                TransTime(iTrans)    =	rts     +   tk(1)                   ;
                                TransKnot(iTrans)    =	loc     +   rts/(tk(2)-tk(1))       ;
                            else % no root
                                error('optimal location for zero-crossing could not be found -- setting value to center of interval')
                                rts                 =   (tk(2)-tk(1))/2	;
                                TransTime(iTrans)	=   tk(1)   +	rts ;
                                TransKnot(iTrans)   =   loc     +	1/2	;
                            end
                            
                    end
                end
            end
            
        end
    end
    
else
    disp('Field missing. Skipping computation.')
end


% ***********************************************************************
% ***********************************************************************