function [SolX,IntX,exitflag,info]  =  BBsearch(model,intX,options)

% -------------------------------------------------------------------------
% SCS toolbox - BBsearch.m
% -------------------------------------------------------------------------
% Description
%   Implementation of the branch-and-bound optimization method.
%
% -------------------------------------------------------------------------
% Syntax:   [SolX,IntX,exitflag,info]  =  BBsearch(model,intX,options)
%
% Inputs:
%   model       Model as a structure
%   intX        Search space for model parameters
%   options (*) Structure defining the meta-parameters specifying algorithm
%                execution. The following optional fields are used:
%                - branching: methods for node selection during branching
%                       0: breadth-first
%                   (+)	1: best lower bound 
%                       2: best upper bound
%                       3: worst lower bound
%                       4: worst upper bound
%                - display: interger which controls command line output 
%                   (+)	0:	no command line output
%                       >0: provided number gives the interval of iterations by
%                       which a report is printed
%                - graph: boolean which determines wheter graphical
%                   performance tracking is enabled (true) 
%                   or not (false (+))
%
%   (x)     Optional input
%   (+)     Default value
%
% Outputs:
%   SolX        Best-known solution
%   IntX        Best-known node in the solution tree
%   exitflag    Exit flag
%               [-2: no result, +1: several results, +2: single result]
%   info        Meta-information describing algorithm performance
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2018.01.26
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% =======================================================================
%   PREPROCESSING
% =======================================================================

% --------------------------------------------------------------------
%   INPUT CHECKS
% --------------------------------------------------------------------

[nVar,n2]	=	size(intX)              ;
assert(n2==2,'Error in BBsearch: Column dimension of intX is not 2')

% --------------------------------------------------------------------
%   ALGORITHM PARAMETERS
% --------------------------------------------------------------------

% default options:
branching   =   1   ; % best lower bound
display     =   0   ; % no command line output
graph       =   0   ; % no graph
% get provided options, if any:
if nargin>=3 && ~isempty(options)
    if isfield(options,'branching'),branching	=	options.branching	;   end
    if isfield(options,'display'),	display     =	options.display     ;   end
    if isfield(options,'graph'),	graph       =	options.graph       ;   end
end

boundfun	=	model.boundfun          ;
resol       =	model.resol             ;

colParent	=	1                       ;
colLB       =	2                       ;
colUB       =	3                       ;
colLive     =	4                       ;
colXlow     =   colLive+(1:nVar)        ;
colXupp     =   colLive+(1:nVar)+nVar	;
colXbest	=   colLive+(1:nVar)+2*nVar	;

% --------------------------------------------------------------------
%   ROOT SOLUTION
% --------------------------------------------------------------------
[LB,UB,BestX]	=	boundfun(model,intX)	;

D  =	intX(:,2)-intX(:,1)                 ;
if all(D<=resol),   LiveStatus	=	-1      ;
else,               LiveStatus	=	+1      ;
end

% --------------------------------------------------------------------
%   PREALLOCATION AND INITIALIZATION
% --------------------------------------------------------------------

% default tree size, found large enough for typical problems:
TreeSize	=	100000                  ;   

% pre-allocation
Tree        =	nan(nVar*3+4,TreeSize)	;	% initialize tree to a certain size

% store root node:
nbranch         =       1	;
Tree(:,nbranch) =	[	0 ;	LB ;	UB ;	LiveStatus ;	intX(:,1) ;	intX(:,2) ;	BestX(:) ]	;

% find live and brancheable nodes:
Alive           =   find(and(Tree(colLive,:)~=0,~isnan(Tree(colLive,:))) )  ;
Brancheable     =	find(Tree(colLive,:)==1)                                ;

% track performance:
iter            =   0                       ;
Perf(iter+1,1)	=	length(Alive)           ;
Perf(iter+1,2)  =	min(Tree(colLB,Alive))  ;
Perf(iter+1,3)  =	min(Tree(colUB,Alive))  ;
Perf(iter+1,4)  =	max(Tree(colUB,Alive))  ;
Perf(iter+1,5)  =	length(Brancheable)     ;

if display && mod(iter,display)==0
    DisplayFun(Perf,iter)
end

% --------------------------------------------------------------------
%   GRAPHICAL OUTPUT PARAMETERS AND FIGURE INITIALIZATION
% --------------------------------------------------------------------

if graph 
    
    title1	=	'Performance'	;
    title2	=	'Search space'	;
    
    han=findobj('type','figure','name',title1); close(han)
    han=findobj('type','figure','name',title2); close(han)
    drawnow()
    
    % get screen info:
    ScreenPos	=   get(0,'ScreenSize')         ;
    ScreenPos2  =   get(0, 'MonitorPositions')  ;
    if size(ScreenPos2,1)>=2
        bool=~ismember(ScreenPos2,ScreenPos,'rows') ;
        scr = find(bool,1,'first');
        ScreenPos = ScreenPos2(scr,:) ;
    end
    
    SX          =	ScreenPos(1)        ; 
    SY          =	ScreenPos(2)        ;
    SW          =	ScreenPos(3)        ; 
    SH          =	ScreenPos(4)        ;
    
    % color and scale parameters:
    blue_majorelle = [96 80 220]/256 ; 
    maxintX = max(intX(:));
    minintX = min(intX(:));
    
    % decided which graphs to create:
    graph0 = graph ;
    graph1 = graph>1 && nVar==1 ;
    graph2 = graph>1 && nVar==2 ;
    graph3 = graph>1 && nVar>2 ;
    
    % algorithm performance overview
    if graph0
        han0 = figure('Name',title1,'Numbertitle','off');
        set(gcf,'Position',[SX+2/15*SW+6*SW/15 SY+1/6*SH 6*SW/15 SH*2/3 ])
    end
    
    % search space for 1-dimensional search
    if graph1
        han1 = figure('Name',title2,'Numbertitle','off');
        set(gcf,'Position',[SX+1/20*SW SY+1/6*SH 4*SW/10 SH*2/3 ])
        hold on,
        plot([ intX(:,1) intX(:,2)],[LB LB],'b.-')
        plot([ BestX(:)],[UB LB],'r+')
        set(gca,'Xlim',[ intX(:,1) intX(:,2)])
        set(gca,'Yscale','log')
        xlabel('\theta_1')
        ylabel('Objective function')
    end
    
    % search space for 2-dimensional search
    if graph2
        han1 = figure('Name',title2,'Numbertitle','off');
        set(gcf,'Position',[SX+1/20*SW SY+1/6*SH 4*SW/10 SH*2/3 ])
        pos = [ intX(:,1)' (intX(:,2)-intX(:,1))'] ;
        hold on,
        rectangle('pos',pos,'FaceColor','w','EdgeColor','b')
        xlabel('\theta_1')
        ylabel('\theta_2')
        set(gca,'Xlim',[ intX(1,1) intX(1,2)])
        set(gca,'Ylim',[ intX(2,1) intX(2,2)])
    end
    
    % search space for higher-dimensional search
    if graph3
        han3 = figure('Name',title2,'Numbertitle','off');
        set(gcf,'Position',[SX+1/20*SW SY+1/6*SH 4*SW/10 SH*2/3 ])
        PlotGraph3(han3,Tree,Alive,Brancheable,colXlow,colXupp,nVar,minintX,maxintX)
    end
else
    graph0 = false;
    graph1 = false;
    graph2 = false;
    graph3 = false;
end


% =======================================================================
% =======================================================================


    
% =======================================================================
%   ACTUAL SEARCH
% =======================================================================

while any(Brancheable)
    iter=iter+1;
    
    % --------------------------------------------------------------------
    % 1. select node to branch from:
    % --------------------------------------------------------------------
    method	=	branching(mod(iter,length(branching))+1) ;
    switch method
        case 0, [~,index]	=	min(Tree(colParent,Brancheable));
        case 1, [minLB,index]	=	min(Tree(colLB,Brancheable))	;
            if minLB ==0,   index=1;    end % return to breadth-first if lower bound is not computed well
        case 2, [~,index]	=	min(Tree(colUB,Brancheable))	;
        case 3, [~,index]	=	max(Tree(colLB,Brancheable))	;
        case 4, [~,index]	=	max(Tree(colUB,Brancheable))	;
    end
    if isempty(index)
        branch = Brancheable(1) ;
    else
        branch = Brancheable(index) ;
    end
    
    % --------------------------------------------------------------------
    % 2. execute branching + bounding
    % --------------------------------------------------------------------
    Tree(colLive,branch)	=	0                                           ;   %	set current node as unbrancheable
    Lengths                 =	Tree(colXupp,branch)-Tree(colXlow,branch)	;   %   get dimensions of the node
    Divisable               =	find( (Lengths./resol)>1 )                  ;   %   find dimensions which can be divided along
    [~,index]               =   max(Lengths(Divisable))                     ;   %   find largest dimensions which can be divided
    index                   =	Divisable(index)                            ;   %   select node dimensions to split along
    
    % find value where the node is split:
    intX_0		=	[ Tree(colXlow,branch)  Tree(colXupp,branch) ]  ;       %	get node box
    d           =	intX_0(index,2)-intX_0(index,1)                 ;       %   get length of divided dimension
    split       =	intX_0(index,1)+2^(ceil(log2(d))-1)             ;       %   define location where the node is split
    
    % info about parent node
    LBparent    =   Tree(colLB,branch)                              ;       %   get lower bound of node parent
    UBparent    =   Tree(colLB,branch)                              ;       %   get upper bound of node parent
    BestXparent =   Tree(colXbest,branch)                           ;       %   get best known solution in the node
    
    
    % for each of the two new nodes ... 
    for leaf=1:2    
        
        % get parent box
        intX	=	intX_0	;
        
        % compute node box
        switch leaf
            case 1, intX(index,1) = split ; % for node 1: set the splitted location as lower bound for the variable
            case 2, intX(index,2) = split ; % for node 1: set the splitted location as upper bound for the variable
            otherwise,  disp('unknown option')
        end
        
        % compute bounds
        [LB,UB,BestX] = boundfun(model,intX) ;
        
        % inherit the lower bound
        if (LB<LBparent && LBparent<=UB)
            LB = LBparent ;
        end
        
        % upper bound inheritance has caused some problems with the SCSD
        % models, presumably because the computed objective function is too
        % optimistic.
%         % inherit the upper bound and solution of the parent if they are
%         % better than the newly found upper bound solution
%         if UB>UBparent && UBparent>=LB 
%             if all(and(intX_0(:,1)<=BestX,BestX<=intX_0(:,2)))
%                 UB      =	UBparent	;
%                 BestX	=	BestXparent	;
%             end
%         end
        assert(LB<=UB,'The computed lower bound is higher than the computed upper bound. Check the bounding function for proper execution.')
        
        % has the desired resolution been reached?
        D   =	intX(:,2)-intX(:,1) ;
        if all(D<=resol),   LiveStatus = -1 ;   % yes, set as alive but unbrancheable
        else,               LiveStatus = +1 ;   % no, set as alive and brancheable
        end
        
        % graphical output
        if graph1
            
            figure(han1), hold on,
                plot([ intX(:,1) intX(:,2)],[LB LB],'bo-')
                plot([ BestX(:)],[UB UB],'r+')
                
        elseif graph2
            
            figure(han1), 
                hold on,
                pos = [ intX(:,1)' (intX(:,2)-intX(:,1))'] ;
                switch LiveStatus
                    case -1,    rectangle('pos',pos,'FaceColor','m','EdgeColor','k')
                    case +1,    rectangle('pos',pos,'FaceColor','w','EdgeColor','k')
                end
        end
        
        % add solution to tree
        nbranch         =	nbranch+1	;
        entry           =	[ 0 ; LB ; UB ; LiveStatus ; intX(:,1) ; intX(:,2) ; BestX(:) ]  ;
        Tree(:,nbranch) =	entry       ;

    end
    
    % --------------------------------------------------------------------
    % 3. fathoming
    % --------------------------------------------------------------------
    
    % find nodes to keep:
    Alive   =   and(Tree(colLive,:)~=0,~isnan(Tree(colLive,:))) ;   % live nodes
    BestUB	=	min(Tree(colUB,Alive))                          ;   % find best upper bound
    Keep    =   and(Alive,Tree(colLB,:)<=BestUB)                ;   % find nodes with lower bound below or equal to best upper bound
    
    % graphical output:
    if graph2
        Fathom	=	find(and(Alive,~Keep))  ;
        if ~isempty(Fathom)
            for f=Fathom
                intX	=	[	Tree(colXlow,f)	Tree(colXupp,f)         ];
                pos     =	[	intX(:,1)'      (intX(:,2)-intX(:,1))'  ];
                figure(han1),
                hold on,
                rectangle('pos',pos,'FaceColor',blue_majorelle,'EdgeColor','k')
            end
        end
    end
    
    % update tree:
    Tree(:,1:sum(Keep))     =	Tree(:,Keep)                ;
    Tree(:,sum(Keep)+1:end) =	nan                         ;
    Brancheable             =	find(Tree(colLive,:)==1)    ;
    
    % --------------------------------------------------------------------
    % 4. performance tracking
    % --------------------------------------------------------------------
    
    Alive                   =   find(and(Tree(colLive,:)~=0,~isnan(Tree(colLive,:))) );
    Perf(iter+1,1)          =	length(Alive)           ;
    Perf(iter+1,2)          =	min(Tree(colLB,Alive))  ;
    Perf(iter+1,3)          =	min(Tree(colUB,Alive))  ;
    Perf(iter+1,4)          =	max(Tree(colUB,Alive))  ;
    Perf(iter+1,5)          =	length(Brancheable)     ;

    % --------------------------------------------------------------------
    % 5. command line and graphical output
    % --------------------------------------------------------------------
    if graph0,  PlotGraph0(han0,Perf,iter), end
    if graph3,  PlotGraph3(han3,Tree,Alive,Brancheable,colXlow,colXupp,nVar,minintX,maxintX),   end
    if display && mod(iter,display)==0, DisplayFun(Perf,iter),  end

end

% =======================================================================
%   POSTPROCESSING
% =======================================================================

% --------------------------------------------------------------------
%   PROCESS RESULTS FOR FUNCTION OUTPUT
% --------------------------------------------------------------------

% clean up tree
Alive	=   find(and(Tree(colLive,:)~=0,~isnan(Tree(colLive,:))))   ;
Tree	=	Tree(:,Alive)                                           ;

% algorithm exit flag and best solution (if any)
switch length(Alive)
    case 0
        exitflag    =	-2          ;
        SolX        =   nan(nvar,1) ;
        IntX        =   nan(nvar,2) ;
    case 1
        exitflag    =	+2                  ;
        SolX        =   Tree(colXbest,:)	;
        IntX        =   [ Tree(colXlow,:)  Tree(colXupp,:) ];
    otherwise
        exitflag    =	+1          ;
        % more than one solution set - report the one with best upper bound
        [~,index] = min(Tree(colUB,:));
        SolX        =   Tree(colXbest,index) ;
        IntX        =   [ Tree(colXlow,index)  Tree(colXupp,index) ];
end

% put all meta-information together into the structure 'info'
info.tree           =   Tree ;
info.iteration      =   iter ;
info.minlowerbound  =   min(Tree(colLB,Alive));
info.maxlowerbound  =   max(Tree(colLB,Alive));
info.minupperbound  =   min(Tree(colUB,Alive));
info.maxupperbound  =   max(Tree(colUB,Alive));
info.performance    =	Perf ;

% --------------------------------------------------------------------
%   COMMAND LINE AND GRAPHICAL OUTPUT
% --------------------------------------------------------------------

if display, DisplayFun(Perf,iter),      end
if graph0,  PlotGraph0(han0,Perf,iter), end

end

% ***********************************************************************
% ***********************************************************************

function DisplayFun(Perf,iter)
    disp(['Iteration: ' num2str(iter)])
    disp(['	Total nodes: ' num2str(Perf(iter+1,1))])
    disp(['	Brancheable nodes: ' num2str(Perf(iter+1,5))])
    disp(['	Best upper bound: ' num2str(Perf(iter+1,3))])
    disp(['	Best lower bound: ' num2str(Perf(iter+1,2))])
end

% ***********************************************************************
% ***********************************************************************

function PlotGraph0(han0,Perf,iter)
    figure(han0); 
    subplot(2,1,1)
        hold off,   plot(0:iter,Perf(:,1),'k.','MarkerSize',17)
        hold on,    plot(0:iter,Perf(:,5),'.','MarkerSize',11,'Color',[96 80 220]/256)
        hLeg1 = legend({'All','Available for branching'},'Location','NorthWest') ;
        set(hLeg1.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[1 1 1 0]'));
        ylabel('Live nodes')
        XT = get(gca,'Xtick') ;
        XT = XT(mod(XT,1)==0);
        set(gca,'Xtick',XT);
    subplot(2,1,2), 
        hold off,
        plot(0:iter,Perf(:,4),'k.','MarkerSize',17)
        hold on,
        plot(0:iter,Perf(:,3),'r.','MarkerSize',14)
        plot(0:iter,Perf(:,2),'.','MarkerSize',11,'Color',[96 80 220]/256)
        set(gca,'Yscale','log')
        hLeg2 = legend({'Worst upper bound';'Best upper bound';'Best lower bound'},'Location','West') ;
        xlabel('Iteration number')
        ylabel('Objective function')
        set(gca,'Xtick',XT);
        
end

% ***********************************************************************
% ***********************************************************************

function PlotGraph3(han3,Tree,Alive,Brancheable,colXlow,colXupp,nVar,minintX,maxintX)
        UBX = max(Tree(colXupp,Alive),[],2) ;
        LBX = min(Tree(colXlow,Alive),[],2) ;
        figure(han3)
        hold off,
            plot(repmat(1:nVar,[ 2 1]),[LBX  UBX]','ko-','MarkerFaceColor','k')
        if length(Brancheable)>=1
        UBX1 = max(Tree(colXupp,Brancheable),[],2) ;
        LBX1 = min(Tree(colXlow,Brancheable),[],2) ;
            hold on,
            plot(repmat(1:nVar,[ 2 1]),[LBX1  UBX1]','ko','MarkerFaceColor',[96 80 220]/256)
        end
            set(gca,'Xlim',[.5 nVar+.5],'Ylim',[minintX maxintX],'Xtick',[1:nVar])
            xlabel('Parameter index')
            ylabel('Parameter interval')
            
end

% ***********************************************************************
% ***********************************************************************
