function model  =  PrecomputeConstraints(model)

% -------------------------------------------------------------------------
% SCS toolbox - PrecomputeConstraints.m
% -------------------------------------------------------------------------
% Description
%   This function computes all constraints that may be applied during model
%   fitting for the given shape constrained spline model. The advantage is
%   that for the simplest models this only has to be executed once and the
%   model fitting procedure can be initiated by simply selecting the
%   applicable constraints and applying the sign values - instead of
%   evaluating the applicable constraints again and again for every
%   instance of the shape constrained spline function fitting problem.
%
% -------------------------------------------------------------------------
% Syntax:   model  =  PrecomputeConstraints(model)
%
% Inputs:
%   model   Shape Constrained Spline model
%
% Outputs:
%   model	Shape Constrained Spline model (updated)
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2018.01.25
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% ========================================================================
%   GLOBAL VARIABLES
% ========================================================================

global delta_global

if nargin<2 || isempty(delta)
    if exist('delta_global','var') && ~isempty(delta_global)
        delta = delta_global;
    else
        delta = 1e-4 ;
    end
end

% ========================================================================
%   INPUTS
% ========================================================================

% ------------------------------------------------------------------------
%   Dimensions:
% ------------------------------------------------------------------------
degree      =	model.dims.degree       ;
nAlpha      =	model.dims.alpha        ;
nBeta       =	model.dims.beta         ;
nKnot       =	model.dims.knots        ;
nSegment	=	model.dims.segment      ;

% ------------------------------------------------------------------------
%   Others:
% ------------------------------------------------------------------------
knots       =	model.knots             ;
Xc          =	model.Xk                ;


% ========================================================================
%   COMPUTE ROTATION MATRICES
% ========================================================================

% ------------------------------------------------------------------------
%   From spline coefficients to polynomial coefficients over normalize
%   inter-knot intervals: 
% ------------------------------------------------------------------------

% needed to noralize the inter-knot distance to unit distance:
dtk     =	diff(knots) ; 

% coefficients of the function (cubic polynomials)
%   dimension 1: polynomial term from 0th to 3rd degree
%   dimension 2: spline coefficients
%   dimension 3: inter-knot segment
P0      =	nan(4,nBeta,nSegment)	;
for iSeg=1:nSegment
    P0(:,:,iSeg)	=	diag(dtk(iSeg).^[0:3])*[    Xc{1}(iSeg,:)     ;   ...
                                                    Xc{2}(iSeg,:)     ;   ...
                                                    Xc{3}(iSeg,:)/2   ;   ...
                                                    Xc{4}(iSeg,:)/6	]	;
end

% coefficients of the 1st derivative (quadratic polynomials)
%   dimension 1: polynomial term from 0th to 2nd degree
%   dimension 2: spline coefficients
%   dimension 3: inter-knot segment
P1 = nan(3,nBeta,nSegment) ;
for iSeg=1:nSegment
    P1(:,:,iSeg)	=	diag(dtk(iSeg).^[0:2])*[	Xc{2}(iSeg,:)     ;	...
                                                    Xc{3}(iSeg,:)     ;	...
                                                    Xc{4}(iSeg,:)/2	]	;
end

% ------------------------------------------------------------------------
%   Rotations needed to formulate conic constraints in standard form: 
% ------------------------------------------------------------------------

% Rotations needed to formulate conic constraints in Mosek:
U   =	[   +1 0 +1     ;
            +1 0 -1     ;
            0  2  0  ]  ;
T	=       U^(-1)      ;

% 0th derivative of cubic polynomial
Q0	=	[   0   0   0   +1	0   0       ;
            +1	0   0   -1  +2	0       ;
            0   +2  0   0   -2  +1      ;
            0   0   +1  0   0   -1  ]   ;
R0	=       Q0*blkdiag(T,T)             ;

% 1st derivative of cubic polynomial
Q1	=	[	1   0	0	0               ;
            0   2	0   1               ;
            0	0	1   -1   ]          ;
R1	=       Q1*blkdiag(T,1)             ;


% ========================================================================
%   DEFINE SHAPE CONSTRAINT MATRICES AND VECTORS
% ========================================================================

% ------------------------------------------------------------------------
%   Dimensions for auxiliary variables
% ------------------------------------------------------------------------

n0          =	(degree-1)*degree/2             ;
n1          =	(degree-2)*(degree-1)/2         ;

% number of auxiliary variables and constraints per cubic polynomial:
nExtraVar0	=	n0+n0       ;
nConEq0     =	degree+1	;

% number of auxiliary variables and constraints per quadratic polynomial:
nExtraVar1	=	n0+n1       ;
nConEq1     =	degree      ;

% ------------------------------------------------------------------------
%   Constraints for non-spline regression coefficients
% ------------------------------------------------------------------------

% number of constraints
nAlphaCon           =	(nAlpha>0)*(nAlpha-1)                       ;

% default constraints formulated in all regression coefficients in original
% space:
b_alpha             =       sparse(nAlphaCon,1)                     ;
a_alpha_beta        =       sparse(nAlphaCon,nBeta+nAlpha)          ;
cc                  =       nBeta+(1:nAlpha)                        ;
a_alpha_beta(:,cc)	=	[	ones(nAlphaCon,1)	-eye(nAlphaCon)	]   ;

% ------------------------------------------------------------------------
%   Shape constraints associated with the spline knots
% ------------------------------------------------------------------------

b_knt           =       delta*ones(nKnot*degree,1)                  ;
a_knt_beta      =   [   Xc{1}	;	Xc{2}	;   Xc{3}	]           ;

% ------------------------------------------------------------------------
%   Shape constraints associated with the inter-knot intervals
% ------------------------------------------------------------------------

SegIndex        =       1:nSegment                                  ;

% indicate which auxiliary variables are involved in the constraints
segcol0         =	kron(   SegIndex	,   ones(1,nExtraVar0)	)   ;
segcol1         =	kron(   SegIndex	,   ones(1,nExtraVar1)	)   ;

% indicate which segment the constraints are associated with
segrow0         =	kron(	SegIndex	,   ones(1,nConEq0)     )   ;
segrow1         =	kron(   SegIndex	,   ones(1,nConEq1)     )   ;
segrow1_ineq	=           SegIndex                                ;

% pre-allocation of equality constraints
a0_beta         =   sparse( nSegment*nConEq0 ,	nBeta               )   ;
a0_conic        =   sparse( nSegment*nConEq0 ,	nSegment*nExtraVar0	)   ;
a1_beta         =	sparse( nSegment*nConEq1 ,	nBeta               )   ;
a1_conic        =	sparse( nSegment*nConEq1 ,	nSegment*nExtraVar1	)   ;

% pre-allocation of inequality constraints
a1_conic_ineq	=	sparse( nSegment	,   nSegment*nExtraVar1	)	;

% run over all inter-knot segments
for iSeg=1:nSegment
    rr              =   (iSeg-1)*nConEq0+(1:nConEq0)        ;   % constrained index
    cc              =   (1:nExtraVar0)+(iSeg-1)*nExtraVar0	;   % auxiliary variables involved
    a0_beta(rr,:)	=	squeeze(P0(:,:,iSeg))               ;   % left-hand side: from spline coefficients to the polynomial coefficients
    a0_conic(rr,cc) =	-R0                                 ;   % right-hand side: from the auxiliary variables to the polynomial coefficients
    
    rr              =	(iSeg-1)*nConEq1+(1:nConEq1)        ;   % constrained index
    cc              =	(1:nExtraVar1)+(iSeg-1)*nExtraVar1	;   % auxiliary variables involved
    a1_beta(rr,:)	=	squeeze(P1(:,:,iSeg))               ;   % left-hand side: from spline coefficients to the polynomial coefficients
    a1_conic(rr,cc) =	-R1                                 ;   % right-hand side: from the auxiliary variables to the polynomial coefficients
    
    a1_conic_ineq(iSeg,iSeg*nExtraVar1)	=   1               ;   % inequality constrain for the fourth auxiliary variable for quadratic polynomials
    
end

% ========================================================================
%   STORE COMPUTED MATRICES AND VECTORS
% ========================================================================

%   Projection and rotation matrices:
model.con.P0            =	P0              ;
model.con.P1            =	P1              ;

model.con.Q0            =	Q0              ;
model.con.Q1            =	Q1              ;

model.con.R0            =	R0              ;
model.con.R1            =	R1              ;

%   Constraint matrices - simple ones:
model.con.a_alpha_beta	=	a_alpha_beta    ;
model.con.b_alpha       =	b_alpha         ;

model.con.a_knt_beta	=	a_knt_beta      ;
model.con.b_knt         =	b_knt           ;

%   Constraint matrices + indicator vectors - for cubic polynomials:
model.con.a0_beta       =	a0_beta         ;
model.con.a0_conic      =	a0_conic        ;
model.con.segcol0       =	segcol0         ;
model.con.segrow0       =	segrow0         ;

%   Constraint matrices + indicator vectors - for quadratic polynomials:
model.con.a1_beta       =	a1_beta         ;
model.con.a1_conic      =	a1_conic        ;
model.con.a1_conic_ineq	=	a1_conic_ineq	;
model.con.segcol1       =	segcol1         ;
model.con.segrow1       =	segrow1         ;
model.con.segrow1_ineq	=	segrow1_ineq	;

%   Continuity constraints (default = empty)
model.con.a_contin_beta =	sparse(0,nBeta) ;
model.con.b_contin      =	sparse(0,1)     ;

% ***********************************************************************
% ***********************************************************************

end