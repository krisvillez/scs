function Handle =  QRTplot(QS,Endpoints,Type)

% -------------------------------------------------------------------------
% SCS toolbox - PlotQRT.m
% -------------------------------------------------------------------------
% Description
%
% This function vizualizes a qualitative representation (QR) by means of
% coloured patches.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Handle = PlotQRT(QR,Type,Scale)
%
% INPUT
%   QS:         String specifying the qualitative sequence (required)
%   Endpoints:  Location of the episode end points (required)
%   Type:       Define whether to print the primitives as characters. 
%               (optional, default = false)
%
% OUTPUT
%   Handle:     Handle of the produced plot axis.
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-06-09
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2019 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<3
    Type = 0 ;
end

% assert(any(Type==3),'Unknown Type of vizualization')

hold on
if ~isa(QS,'cell')
    QS    =   { QS }    ;
end
if ~isa(Endpoints,'cell')
    Endpoints    =   { Endpoints }    ;
end
n   =   length(Endpoints)    ;


set(gca,'Ytick',1:n)

map = 1;
for i=1:n
    qs          =   QS{i}                     ;
    endpts = Endpoints{i};
    endpts = endpts(:);
    if ~isempty(qs)
        
        key = Alphabet();
        [tf,index]=ismember(qs,key);
        X = sort(repmat([endpts(1:end-1) endpts(2:end) ],[1 2]),2);
        Y = i+[0 1]-0.5;
        Y= repmat([ Y fliplr(Y) ],[length(qs) 1]);
        C = CFD(index,map);
        patch(X',Y',reshape(C,[1 length(qs) 3]),'LineStyle','none')
        if Type % add characters
            TextX       =   mean(X')               ;
            TextY       =   mean(Y')               ;
            text(TextX,TextY,qs(:),'Color','w','VerticalAlignment','middle','HorizontalAlignment','center','fontsize',get(gca,'fontsize'),'fontname','Helvetica-Narrow','fontweight','normal')
        end

    end
end

drawnow
Handle = gca ;
return


N       =   size(XXX,2)                 ;
CCC     =   reshape(CFD(CC,map),[1 N 3 ])   ;  % Actual Colors

XXX     =   (XXX)*Scale          	;
%     XXX=XXX-1;
    
XMIN    =   min(XXX(:))         ;
XMAX    =   max(XXX(:))         ;
YMIN    =   min(YYY(:))         ;
YMAX    =   max(YYY(:))         ;
axis([XMIN XMAX  YMIN YMAX])    ;
% drawnow

patch(XXX,YYY,CCC,'LineStyle','none')
drawnow

LineWidth = 1.2 ;
%PlotHorizontal((YMIN+1:1:YMAX-1),'w-','LineWidth',LineWidth)
set(gca,'Ydir','reverse');
if Type % add characters
    select  =   find(XXX(3,:)-XXX(1,:)>=1)  ;
    CC      =   CC(select)                  ;
    XXX     =   XXX(:,select)               ;
    YYY     =   YYY(:,select)               ;
    
    QRstring    =   cellstr(Alphabet(CC,Type)')  ;
    TextX       =   mean(XXX)               ;
    TextY       =   mean(YYY)               ;
%     nChar       =   length(QRstring)        ;
    text(TextX,TextY,QRstring,'Color','w','VerticalAlignment','middle','HorizontalAlignment','center','fontsize',10,'fontname','Helvetica-Narrow','fontweight','normal')
end

set(gca,'Ytick',1:n)
drawnow
Handle = gca ;
