function [QS,Disc]  =  String2QS(QSstring)

% -------------------------------------------------------------------------
% SCS toolbox - String2QS.m
% -------------------------------------------------------------------------
% Description
%   This function converts qualitative sequence expressed as string into a
%   qualitative description expressed with a matrix. This matrix is
%   organized as an ExD matrix corresponding to E primitives and D=degree
%   derivatives (d=0:(D-1)). In addition, a matrix Disc is produced of
%   dimensions (E-1)x(D+1). If Disc(e,d) is true, this means a
%   disctonituity exists in the (d-1)th derivative at the end of episode e.
%   Otherwise, this derivative is continuous in the transition.
%
% -------------------------------------------------------------------------
% Syntax:   [QS,Disc]  =  String2QS(QSstring)
%
% Inputs:
%	QSstring	Qualitative sequence expressed as string [1xE]
%
% Outputs:
%   QS          Qualitative sequence expressed as a matrix [ExD]
%   Disc        Discontinuities expressed as a matrix [(E-1)x(D+1)]
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-06-09
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% ========================================================================
%   LIST ALL RECOGNIZED PRIMITIVES (CHARACTERS) AND ASSOCIATED SIGNS OF THE
%   1st and 2nd DERIVATIVES
% ========================================================================

Characters      =   'ABCDEFGNPLUQ'              ;
OtherLegalChar	=   '+o-012'                    ;
QS              =   nan(length(Characters),3)	;

% Triangular primitives - non-negative signs
QS(1,2:3)	=   [	-1  +1  ]   ; %	A 
QS(2,2:3)	=   [	+1  +1  ]   ; %	B 
QS(3,2:3)	=   [	+1  -1  ]   ; %	C 
QS(4,2:3)	=   [	-1  -1  ]   ; %	D 

% Triangular primitives - some zero signs
QS(5,2:3)	=   [	-1  0   ]   ; %	E 
QS(6,2:3)	=   [	0   0   ]   ; %	F 
QS(7,2:3)	=   [	+1  0	]   ; %	G 

% Monocurvature primitives
QS(8,2:3)	=   [	nan -1	]   ; %	N
QS(9,2:3)	=   [	nan	+1	]   ; %	P

% Monotonic primitives
QS(10,2:3)	=   [	-1  nan	]   ; %	L
QS(11,2:3)	=   [	+1  nan	]   ; %	U 

% Unknown primitives
QS(12,2:3)	=   [	nan	nan	]   ; %	Q


% ========================================================================
%	INPUT CHECKS
% ========================================================================
assert(ischar(QSstring),'Error in String2QS: QSstring is not a string')
assert(all(ismember(QSstring,[Characters OtherLegalChar])),'Error in String2QS: Illegal characters in input QSstring.')


% ========================================================================
%   FIND PRIMITIVES IN PROVIDED QUALITATIVE SEQUENCE AND REPRESENT IT IN
%   MATRIX FORM
% ========================================================================

% locate the characters in QSstring and set up the matrix QS accordingly:
[~,loc]     =   ismember(QSstring,Characters)	;   
locT        =	loc(loc~=0)                     ;
QS          =	QS(locT,:)                      ;

% number of episodes
nEpisode        =   sum(loc~=0)         ;
% Compute the episode to which each character in QSstring is asoociated
indexEpisode	=   cumsum(loc~=0)      ;

% Initialize matrix describing discontinuities:
Disc            =   false(nEpisode,4)	;

% run over all characters that are not a primitive
if any(loc==0)
    indexNoPrim     =	find(loc==0)    ;
    for i=indexNoPrim(:)'
        iEpisode	=	indexEpisode(i)	;
        if QSstring(i)=='+',        QS(iEpisode,1)	=	+1	; % positive function value
        elseif QSstring(i)=='-',    QS(iEpisode,1)	=	-1	; % negative function value
        elseif QSstring(i)=='o',    QS(iEpisode,1)	=	0	; % zero function value
        else
            % discontinuity in derivative identified by QSstring(i)
            col                 =	str2num(QSstring(i))+1	; 
            Disc(iEpisode,col)	=	true                    ;
        end
    end
end

% discard last row, cannot have a discontinuity at end of the last episode:
Disc	=   Disc(1:(nEpisode-1),:) ; 

end

