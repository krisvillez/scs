function [String,Signs] = Alphabet(Qualifier,Type)

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - Alphabet.m
% -------------------------------------------------------------------------
% Description 
%
% Obtain characters and signs of derivatives according to primitives
% (numerically indexed 1 to 14). The vector of integers is converted into a
% string.  
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[String,Signs] = Alphabet(Qualifier,Type)
%
% INPUT
%	Qualifier:  Vector of qualitative markers for episodes, as integers
%               ranging from 1 to 14. 
%               (optional, default=[1:14])
%	Type:       Specify whether monotonic or triangular primitives are
%               used. Use 1 for monotonic, 2 for triangular.
%               (optional, default=2)
%
% OUTPUT
%	String:     String consisting of characters corresponding to primitives
%	Signs:		Matrix containing signs of 1st and 2nd derivatives for each
%               considered primitive (indexed 1 to 14). Row 1 represents
%               the first derivative, row 2 represents the second
%               derivative.   
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-06-23
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2014-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<1 || isempty(Qualifier),  Qualifier   =   1:14    ;   end % get complete map
if nargin<2 || isempty(Type),       Type        =   3       ;   end % unspecified type, then use triangular primitives

% verify whether qualifiers are proper
%assert(all(Qualifier(:)>=-3),'At least one qualifier too low (mimimally: -3')
%assert(all(Qualifier(:)<=3),'At least one qualifier too high (maximally: +3')

% verify whether specified type is proper

if Type == 0,       Characters  =   'LLLFUUULFUQQQQ'     ;   % Signs first derivative: L:- / F:0 / U:+
elseif Type == 1,   Characters  =   'NOPONOPQQQNOPQ' ;   % Signs second derivative: L:- / F:0 / U:+
else                Characters  =   'DEAFCGBLFUNOPQ'   ;   % Signs derivatives: B:-- / E:-0 / C:-+ / F:00 / A:+- / G:+0 / D:++ 
end

if nargout>=2
    Signs =     [   -1  -1  -1  0   +1  +1  +1  -1  0   +1  nan(1,4)        ;
                    -1  0   +1  0   -1  0   +1  nan(1,3)    -1 0 +1 nan     ];
                
end

String      =   Characters(Qualifier)       ;   % get corresponding characters
