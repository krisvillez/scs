function [Aeq,beq,Aineq,bineq,nSeg0,nSeg1]  =  SetupQCS(model,EP,iV,TrK)

% -------------------------------------------------------------------------
% SCS toolbox - SetupQCS.m
% -------------------------------------------------------------------------
% Description
%   This function produces the equality and inequality constraint matrices
%   and vectors which must be satisfied by the parameters involved in the
%   considered shape constrained spline function fitting problem. The
%   constraints ae structured as follows:
%       Aeq	  * delta == beq
%       Aineq * delta >= bineq
%   with delta the vector of all parameters in the optimation problem.
%   
% -------------------------------------------------------------------------
% Syntax:   [Aeq,beq,Aineq,bineq,nSeg0,nSeg1]  =  SetupQCS(model,EP,iV,TrK)
%   
% Inputs:
%   model   Shape Constrained Spline model
%   EP      Episode assignments of every spline knot
%   iV    	Output variable (for multivariate model output)
%   TrK     Transition positions expressed relative to the knot positions
%
% Outputs:
%   Aeq     Left-hand side matrix for equality constraints 
%   beq     Right-hand side vctor for equality constraints
%   Aineq   Left-hand side matrix for inequality constraints
%   bineq   Right-hand side vctor for inequality constraints
%   nSeg0   Number of segments with inequality-constrained function value
%   nSeg1   Number of segments with inequality-constrained 1st derivative
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2018.01.26
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% ========================================================================
%   Basic inputs
% ========================================================================

if nargin<=2 || isempty(iV),    iV	=	1       ;   end
if nargin<=3 || isempty(TrK),   TrK	=	[   ]	;	end

% Dimensions:
nAlpha      =	model.dims.alpha        ;
nBeta       =	model.dims.beta         ;
nGamma      =	model.dims.gamma        ;
degree      =	model.dims.degree       ;
nSeg        =	model.dims.segment      ;
nTrans      =	model.dims.transitions	;


% Matrix/vector for reconstruction of regression coefficients from the
% whitened coefficient space:
M           =	model.M                 ;
gamma_unc   =	model.gamma_unc(:,iV)	;

Mc          =	M(1:nBeta,:)            ;
beta_unc	=	gamma_unc(1:nBeta)      ;


% ========================================================================
%   Application of constraints in the exact location of transitions:
% ========================================================================

if isempty(TrK) % no transitions specified
    a_trans_ineq	=	sparse(0,nBeta) ;
    b_trans_ineq	=	sparse(0,1)     ;
    a_trans_eq      =	sparse(0,nBeta) ;
    b_trans_eq      =	sparse(0,1)     ;
    nCon_trans_ineq	=   0               ;
    nCon_trans_eq	=	0               ;
else
    
    % evaluate basis function in the transitions:
    TransT          =	K2T(model,TrK)                      ;
    Xtrans0         =	eval_basis(TransT,model.basisobj,0) ;
    Xtrans1         =	eval_basis(TransT,model.basisobj,2) ;
    Xtrans2         =	eval_basis(TransT,model.basisobj,3)	;
    
    % applied tolerance:
    delta           =	0                                   ;
    
    % define the constraint matrices:
    a_trans_beta	=	[ Xtrans0 ; Xtrans1 ; Xtrans2 ]     ;
    b_trans_beta    =	delta*ones(nTrans*degree,1)         ;
    
    % pre-allocation derivative signs in the transitions
    sign_trans      =	nan(nTrans,degree)                  ;
    
    for iTrans=1:nTrans
        for iDeg=0:(degree-1)
            % for considered transition and derivative ...
            
            % find the signs in the EP before and after the
            % transition
            SIGNS   =	model.QS(iTrans+(0:1),iDeg+1)       ;
            
            if any(~isnan(SIGNS))
                % if some signs are specified ...
                if any(SIGNS)==0
                    % if some signs are zero set the sign in the transition
                    % to zero as well:
                    sign_trans(iTrans,iDeg+1)	=	0	;
                else
                    
                    % find specified signs:
                    SIGNS =	SIGNS(~isnan(SIGNS))    ;
                    
                    if length(SIGNS)==1
                        % only 1 sign specified, use that one
                        sign_trans(iTrans,iDeg+1)	=	SIGNS	;
                    elseif SIGNS(1)==SIGNS(2)
                        % both signs are the same, no additional constraint necessary
                    elseif -SIGNS(1)==SIGNS(2)
                        % both signs are the opposite: add a constraint
                        sign_trans(iTrans,iDeg+1)	=	0	;
                    else
                        warning('SetupQCS: Unrecognized condition')
                    end
                end
            end
        end
        
        % The following lines remove a number of constraints, this was
        % found necessary to work with Mosek v8.1. With CVX no such
        % problems were observed (but they are removed anyway as a
        % precaution).  
        FIRST = ~isnan(sign_trans) ;
        if any( FIRST)
            d = find(FIRST,1,'first')+1 ;
            sign_trans(iTrans,d:end) = nan;
        end
    end
    
    % find constraint selection, equality and inequality:
    sign_trans      =	sign_trans'                         ;
    S_trans         =	sign_trans(:)                       ;
    inc_trans_eq	=	S_trans==0                          ;
    inc_trans_ineq	=	or(S_trans==+1,S_trans==-1)         ;
    
    SD              =   diag(S_trans(inc_trans_ineq))       ;
    a_trans_ineq    =	SD*a_trans_beta(inc_trans_ineq,:)	;
    b_trans_ineq    =	b_trans_beta(inc_trans_ineq,:)      ;
    a_trans_eq      =	a_trans_beta(inc_trans_eq,:)        ;
    b_trans_eq      =	b_trans_beta(inc_trans_eq,:)*0      ;
    
    nCon_trans_eq	=	sum(inc_trans_eq)                   ;
    nCon_trans_ineq =	sum(inc_trans_ineq)                 ;
    
end


nAlphaCon	=	(nAlpha>0)*(nAlpha-1)   ;   % number of constraints for non-spline regression coefficients
nContin_eq	=	model.dims.contin_eq	;   % number of continuity constraints (splines with discontinuities)

% ========================================================================
% Setup sign matrices
% ========================================================================
degree = model.dims.degree ;

SignsKnt            =	nan(model.dims.knots,degree)    ;
nEP                 =	model.dims.episodes             ;
for iEP=1:nEP
    rows                =	EP==iEP ;
    nrow                =	sum(rows) ;
    SignsKnt(rows,:)	=	repmat(model.QS(iEP,1:degree),[nrow 1]) ;
end

if any(~isnan(EP))
    KnotIndex = find(~isnan(EP));
    KnotIndex = KnotIndex(diff(EP(KnotIndex))~=0) ;
    for iK=1:length(KnotIndex)-1
        rows = KnotIndex(iK+(0:1)) ;
        if rows(2)-rows(1)>1
            Ep = EP(rows) ;
            for deg=1:degree
                if (~isnan(model.QS(Ep(1),deg)) && ~any(model.Disc(Ep(1),:))) && all(model.QS(Ep(1):Ep(2),deg)==model.QS(Ep(1),deg))
                    SignsKnt(rows(1):rows(2),deg) = model.QS(Ep(1),deg) ;
                end
            end
        end
    end
end

SignsSeg    =   nan(nSeg,degree)     ;
for iSeg=1:nSeg
    for d=1:model.dims.degree
        if ~isnan(SignsKnt(iSeg,d)) && (SignsKnt(iSeg,d) == SignsKnt(iSeg+1,d))
            SignsSeg(iSeg,d) = SignsKnt(iSeg,d)  ;
        end
    end
end

% ========================================================================
%   SECLECT CONSTRAINTS AND VARIABLES
% ========================================================================

% --------------------------------------------------------------------
%   Load precomputed matrices
% --------------------------------------------------------------------

a_alpha_beta	=   model.con.a_alpha_beta ;
b_alpha         =   model.con.b_alpha    ;

a_contin_beta   =   model.con.a_contin_beta ;
b_contin        =   model.con.b_contin    ;

a_knt_beta      =   model.con.a_knt_beta  ;
b_knt           =   model.con.b_knt  ;

a0_beta         =   model.con.a0_beta  ;
a0_conic        =   model.con.a0_conic ;
segcol0         =   model.con.segcol0  ;
segrow0         =   model.con.segrow0   ;

a1_beta         =   model.con.a1_beta    ;
a1_conic        =   model.con.a1_conic  ;
a1_conic_ineq   =   model.con.a1_conic_ineq   ;
segcol1         =   model.con.segcol1  ;
segrow1         =   model.con.segrow1  ;
segrow1_ineq    =   model.con.segrow1_ineq ;

% --------------------------------------------------------------------
%   Select constraints associated with the spline knots
% --------------------------------------------------------------------
S_knt           =	SignsKnt(:)                     ;
inc_knt_eq      =	and(~isnan(S_knt),S_knt==0)     ;
inc_knt_ineq    =	and(~isnan(S_knt),S_knt~=0)     ;

SD              =   diag(S_knt(inc_knt_ineq))       ;
a_knt_ineq      =	SD*a_knt_beta(inc_knt_ineq,:)   ;
b_knt_ineq      =	b_knt(inc_knt_ineq,:)           ;

a_knt_eq        =	a_knt_beta(inc_knt_eq,:)        ;
b_knt_eq        =	b_knt(inc_knt_eq,:)*0           ;

nKnt_eq         =	length(b_knt_eq)                ;
nKnt_ineq       =	length(b_knt_ineq)              ;

% --------------------------------------------------------------------
%   Select constraints associated with function value in the inter-knot
%   intervals 
% --------------------------------------------------------------------

incSeg0     =   find(or(SignsSeg(:,1)==-1,SignsSeg(:,1)==+1))   ;
nSeg0       =	length(incSeg0)                                 ;

incrow0     =	ismember(segrow0,incSeg0)           ;
inccol0     =	ismember(segcol0,incSeg0)           ;

ncol0       =	sum(inccol0)                        ;
nrow0       =	sum(incrow0)                        ;

S0          =	kron(SignsSeg(incSeg0,1),ones(4,1)) ;

a0_beta     =	diag(S0)*a0_beta(incrow0,:)         ;
a0_conic	=	a0_conic(incrow0,inccol0)           ;

% --------------------------------------------------------------------
%   Select constraints associated with 1st derivative in the inter-knot 
%   intervals 
% --------------------------------------------------------------------

incSeg1         =   find(or(SignsSeg(:,2)==-1,SignsSeg(:,2)==+1))   ;
nSeg1           =	length(incSeg1)                                 ;

incrow1         =	ismember(segrow1,incSeg1)           ;
incrow1_ineq    =	ismember(segrow1_ineq,incSeg1)      ;
inccol1         =	ismember(segcol1,incSeg1)           ;

ncol1           =	sum(inccol1)                        ;
nrow1           =	sum(incrow1)                        ;
nrow1_ineq      =	sum(incrow1_ineq)                   ;

S1in            =	SignsSeg(incSeg1,2)                 ;
S1              =	kron(S1in,ones(3,1))                ;

a1_beta         =	diag(S1)*a1_beta(incrow1,:)         ;
a1_conic        =	a1_conic(incrow1,inccol1)           ;
a1_conic_ineq   =	a1_conic_ineq(incrow1_ineq,inccol1) ;

% ========================================================================
%   COMPOSE CONSTRAINT MATRICES AND VECTORS
% ========================================================================

ConTotEq	=	nrow0+nrow1+nKnt_eq+nContin_eq+nCon_trans_eq	;
ConTotIneq	=	nrow1_ineq+nKnt_ineq+nAlphaCon+nCon_trans_ineq	;
VarTot      =	nGamma+ncol0+ncol1                              ;

Aeq         =	sparse(ConTotEq,VarTot)     ;
Aineq       =	sparse(ConTotIneq,VarTot)	;

% --------------------------------------------------------------------
% Define elements of Aeq corresponding to the auxiliary variables 
% --------------------------------------------------------------------
rr0                 =   1:nrow0                         ;
cc0                 =   nGamma+(1:ncol0)                ;
Aeq( rr0 ,	cc0	)   =	a0_conic                        ;

rr1                 =   (1:nrow1)+nrow0                 ;
cc1                 =   nGamma+(1:ncol1)+ncol0          ;
Aeq( rr1 ,	cc1	)	=	a1_conic                        ;

% --------------------------------------------------------------------
% Define elements of Aeq corresponding to the regression coefficients
% --------------------------------------------------------------------
ccg                 =   1:nGamma                            ;
Aeq( rr0 , ccg	)	=	a0_beta*Mc                          ;
Aeq( rr1 , ccg	)	=   a1_beta*Mc                          ;

rrk	=   nrow0+nrow1+(1:nKnt_eq)                             ;
rrc =   nrow0+nrow1+nKnt_eq+(1:nContin_eq)                  ;
rrt =   nrow0+nrow1+nKnt_eq+nContin_eq+(1:nCon_trans_eq)	;

Aeq( rrk , ccg	)	=   a_knt_eq*Mc                         ;
Aeq( rrc , ccg	)	=   a_contin_beta*Mc                    ;
Aeq( rrt , ccg	)   =   a_trans_eq*Mc                       ;
    
beq	=	[	-a0_beta*beta_unc               ; ...
            -a1_beta*beta_unc               ; ...
            b_knt_eq-a_knt_eq*beta_unc      ; ...
            b_contin-a_contin_beta*beta_unc ; ...
            b_trans_eq-a_trans_eq*beta_unc	]   ;

% --------------------------------------------------------------------
% Define elements of Aeq corresponding to the auxiliary variables 
% --------------------------------------------------------------------

rreq                =	1:nrow1_ineq	;
Aineq( rreq	, cc1 ) =	a1_conic_ineq	;

% --------------------------------------------------------------------
% Define elements of Aeq corresponding to the regression coefficients
% --------------------------------------------------------------------

rrin = nrow1_ineq+(1:(nKnt_ineq+nAlphaCon+nCon_trans_ineq)) ;

Aineq( rrin , ccg )	= [	a_alpha_beta*M          ;
                        a_knt_ineq*Mc           ; 
                        a_trans_ineq*Mc     ]   ;

bineq = [	sparse(nrow1_ineq,1)                    ;
            b_alpha-a_alpha_beta*gamma_unc          ;
            b_knt_ineq-a_knt_ineq*beta_unc          ;
            b_trans_ineq-a_trans_ineq*beta_unc	]   ;
        
% ***********************************************************************
% ***********************************************************************

end
