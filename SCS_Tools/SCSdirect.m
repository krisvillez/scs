function [model,tsim]  =  SCSdirect(y,t,String,varargin)

% -------------------------------------------------------------------------
% SCS toolbox - SCSdirect.m
% -------------------------------------------------------------------------
% Description
%   This function provides short-hand access to the complete shape
%   constrained spline fitting procedure. The essential elements to provide
%   are the MxN measurement vector or matrix (y) and the desired
%   qualitative sequence as a string.
%
% -------------------------------------------------------------------------
% Syntax:   model  =  SCSdirect(y,t,String,varargin)
%
% Inputs:
% 	y           Dependent variable measurements [MxN]
%   t       (x)	Independent variable column vector [Mx1] (+ t=(1:M)' )
%   String	(x)	Qualitative sequence expressed as a string (+ String = 'Q')
%
%   (x) Additional pairs of permitted inputs:
%           [Name]          [Value]
%           'Convolution'   Structure defining convolution operator by
%                           means of two fields: (1) .tau and (2) .U . See
%                           ApplyConv.m for detailed instructrions 
%           'Display'       Integer controlling command line output
%                            verbosity [0-2]
%           'Graph'         Integer controlling graphical output [0-3]
%           'KnotDistance'  Integer value controlling the number of data
%                            samples between subsequent knot positions.
%
%   (x)     Optional input
%   (+)     Default value
%
% Outputs:
% 	model       Shape Constrained Spline model
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-06-09
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% =======================================================================
%   DEFAULT INPUTS
% =======================================================================

if nargin==0
    disp(sprintf('\nHere I am, brain the size of a planet, \n and they ask me to make something out of nothing. \n Call that job satisfaction, ''cause I don''t.\n')) 
end

if nargin<2 || isempty(t)
    t = (1:length(y))';
end

if nargin<3 || isempty(String)
    String = 'Q' ;
end

% =======================================================================
%   SETUP OPTIONS
% =======================================================================

% default, typical-use values:
dk      =	1       ;
convol  =	[   ]   ;
display =   2       ;
graph   =	2       ;

% Organize variable inputs
nAddInput	=	length(varargin)                    ;
assert(mod(nAddInput,2)==0,'Error in SCSdirect: Variable arguments must come in pairs')

nAddInput	=	nAddInput/2                         ;
VarInput	=	reshape(varargin,[2 nAddInput])     ;

% set values to input values where applicable:
for iAddInput = 1:nAddInput
    switch VarInput{1,iAddInput}
        case 'Convolution',     convol  =   VarInput{2,iAddInput}	;
        case 'Display',         display =	VarInput{2,iAddInput}	;
        case 'Graph',           graph	=	VarInput{2,iAddInput}	;
        case 'KnotDistance',    dk      =	VarInput{2,iAddInput}	;
        otherwise
            warning('Unknown input name.')
    end
end


% =======================================================================
%   MODEL SETUP
% =======================================================================

if display>0, disp('Model setup ...'), end

t               =   t(:)	;

% use interpolating spline without smooting:
if dk==1,   knots =	[	t(1)	;	t(3:end-2)	;	t(end)	]   ; % skip second and second last point in the time series
else,       knots =	[	t(1:dk:end-dk)          ;	t(end)	]   ;
end
lambda          =	0       ;   

% setup basic SCS model:
model0         =	SCSsetup(y,t,[],knots,lambda)	;

% add information about qualitative sequence:
model0          =   ApplyQS(model0,String)          ;

% add function to use for bounding procedure:
model0.boundfun	=	@SCSfitbnd                      ;

if display>0, disp('Model setup complete'), end

% =======================================================================
%   CONVOLUTION 
% =======================================================================
if ~isempty(convol)
    if display>0, disp('Convolution ...'), end
    
    % check inputs:
    assert(isfield(convol,'TimeConstants'),'Error in SCSdirect: The input "Convolution" must have a field named "TimeConstants".')
    assert(isfield(convol,'BinaryInput'),'Error in SCSdirect: The input "Convolution" must have a field named "BinaryInput".')
    
    % apply convolution to obtain new basis for function fitting:
    model0	=	ApplyConv(model0,convol.BinaryInput,convol.TimeConstants)	;
    
    if display>0, disp('Convolution complete'), end
end


% =======================================================================
%   OPTIMIZATION 
% =======================================================================
if display>0, disp('Optimization ...'), end

if model0.dims.transitions==0 
    
    % there are no transitions so there is no need for nonlinear
    % optimization.
    SolK = [];
    
else
    
    % Nonlinear optimization with branch-and-bound
    
    % Define search space
    KnotInt	=	[	1	model0.dims.knots	]	;
    KnotInt	=   repmat(KnotInt,[ model0.dims.transitions 1])  ;
    
    % Define desired resolution:
    resol	=	ones(model0.dims.transitions,1)	;
    if isfield(model0,'Disc') || model0.dims.nVarOut>1
        resol(any(model0.Disc,2))	=   min([1/dk 1/(2^3)])               ;
    end
    model0.resol        =	resol           ;	% resolution parameters
    
    % Define optimization algorithm meta-parameters:
    options.display     =	display>=2      ;	% display interval
    options.graph       =	max(graph-1,0)	;	% produce graphical output?
    options.branching   =	[ 1  ]          ;	% branching method
    
    % Execute search:
    SolK	=	BBsearch(model0,KnotInt,options)	;
end

if display>0, disp('Optimization complete'), end


% =======================================================================
%   FINAL MODEL AND REPORTING 
% =======================================================================
if display>0, disp('Best model generation ...'), end

% Fit model with selected solution:
model                  =	SCSfit(model0,SolK)      ;

% Get exact values for the transitions:
[TransKnot,TransTime]	=	ComputeTransitions(model)	;
model.transknot        =	TransKnot                   ;
model.transtime        =	TransTime                   ;

if display>0, disp('Best model generation complete.'), end


% =======================================================================
%   GRAPHICAL OUTPUT
% =======================================================================

if graph
    tsim	=	GenerateGrid(model,7)   ;
    %SCSplot(model,tsim)
    % call the addapted version of SCSplot to get every iteration in one
    % figure
    SCSplot_1fig(model,tsim)                 ;
end

% ***********************************************************************
% ***********************************************************************
