function T  =  K2T(model,K)

% -------------------------------------------------------------------------
% SCS toolbox - K2T.m
% -------------------------------------------------------------------------
% Description
%   This function translates independent values expressed relative to the
%   spline knots to independent values in the original scale of the
%   variable. 
%
% -------------------------------------------------------------------------
% Syntax:   T  =  K2T(model,K)
%
% Inputs:
%   model   Shape Constrained Spline model
%   K       Independent variable expressed relative to the spline knots 
%
% Outputs:
%   T       Independent variable expressed in original scale of the
%           independent variable. 
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2018.01.25
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% =======================================================================
%   INPUT AND CHECKS
% =======================================================================

assert(isfield(model,'knots'),'Error in K2T: Field model.knots not found in model')
assert(isfield(model,'dims'),'Error in K2T: Field model.dims not found in model')
assert(isfield(model.dims,'knots'),'Error in K2T: Field model.dims.knots not found in model.dims')

knots       =	model.knots         ;
nk          =	model.dims.knots	; 

if ~isempty(K)
    assert(min(K(:))>=1,'Not all elements in K are above or equal to the maximum number of knots (model.dims.knots)')
    assert(max(K(:))<=nk,'Not all elements in K are below or equal to the maximum number of knots (model.dims.knots)')
end


% ========================================================================
%   TRANSFORMATION
% ========================================================================

knotindex	=	1:nk        ;

% Pre-allocation
[I,J]       =   size(K)     ;
T           =	nan(I,J)    ;

for i=1:I
    for j=1:J
        % For every element of K, k, ...

        % Find last knot which is equal to or before the considered k
        k       =	K(i,j)                          ;
        index	=	find(knotindex<=k,1,'last')	;
        
        if index==nk
            % IF: the identified knot is the last knot, 
            % THEN: the T-value is equal to this knot's position
            t	=	knots(index)                                    ;
        else
            % ELSE: The T-value is between two knot positions:
            f	=	( k-index )  * ( knots(index+1)-knots(index) )	;
            t	=	knots(index) + f                                ;
        end
        
        % Store value:
        T(i,j)	=	t                   ;
        
        assert(t>=model.domain(1),'Obtained value on the left-hand side of the function domain.')
        assert(t<=model.domain(2),'Obtained value on the right-hand side of the of function domain.')
    end
end
