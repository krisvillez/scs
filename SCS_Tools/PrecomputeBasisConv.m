function [Y1,Y2]  =  PrecomputeBasisConv(bao,knots,tspan,tau,f1,f2,U)

% -------------------------------------------------------------------------
% SCS toolbox - PrecomputeBasisConv.m
% -------------------------------------------------------------------------
% Description
%   This function executes the convolution necessary to relate the input
%   signal, represented as a cubic spline function, to the output signal of
%   a second order system composed of two first-order systems in series.
%   Practically, the response of the first and second system in the series
%   is given as a basis matrix with columns corresponding to regression
%   coefficients.
%
% -------------------------------------------------------------------------
% Syntax:   [Y1,Y2]  =  PrecomputeBasisConv(bao,knots,tspan,tau,f1,f2,U)
%
% Inputs:
% 	bao     Functional basis object (as defined in the FDA toolbox)
%   knots   Spline knot positions
%   tspan   Independent variable positions for which the convoluted basis
%   tau     Time constants (dimensions [2x1])
%   f1      Precomputed symbolic response functions for the first
%           first-order system
%   f2      Precomputed symbolic response functions for the complete
%           second-order system
%   U       On-off binary input signal given as an [Mx2] matrix with M the
%           number times the input signal changes value. The first column
%           represents the times of change and the second column the value
%           (0 or 1).
%
% Outputs:
%   Y1      Basis for the first first-order system
%   Y2      Basis for the complete second-order system
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2018.01.25
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% ========================================================================
%   INPUT CHECKS
% ========================================================================

assert(strcmp(class(bao),'basis'),  'Error in PrecomputeBasisConv: bao is not of basis class.')
assert(strcmp(class(f1),'cell'),	'Error in PrecomputeBasisConv: f1 is not of class cell.')
assert(strcmp(class(f2),'cell'),	'Error in PrecomputeBasisConv: f2 is not of class cell.')

assert(size(knots,2)==1,'Error in PrecomputeBasisConv: knots is not a column vector.')
assert(size(tspan,2)==1,'Error in PrecomputeBasisConv: tspan is not a column vector.')
assert(length(tau)==2,	'Error in PrecomputeBasisConv: tau does not have length 2.')
assert(size(U,2)==2,	'Error in PrecomputeBasisConv: U is not a matrix with two columns.')


% ========================================================================
%   DIMENSIONS
% ========================================================================

nbasis	=	getnbasis(bao)      ;
order   =   getbasisorder(bao)	;

% ========================================================================
%   SYSTEM INPUT-OUTPUT CHARACTERIZATION
% ========================================================================

% parameters
tau1    =	tau(1)	;
tau2    =	tau(2)	;

% selected unit response for second order system
equaltau  = tau1==tau2 ;
if equaltau
    f2 = f2(:,1) ;
else
    f2 = f2(:,2) ;
end

% ========================================================================
%   INPUT SIGNALS CHARACTERIZATION
% ========================================================================

%   The unknown input represented as a spline function changes in
%   meaningful ways at the spline knots whereas the known binary input
%   signal changes at locations given in U(:,1). These are combined
%   to define the periods of time (sections) within which both input
%   signals remain invariant.
SecBounds	=	sort(unique([ knots(:) ;  U(:,1) ]))        ;
ttsim       =	sort(unique([tspan ; SecBounds]))           ;
ntt         =	length(ttsim)                               ;
nSec        =	length(SecBounds)-1                         ;

% Define variable AERO matching the number of sections in size and having a
% boolean value corresponding to the known input signal during each
% section.
AERO            =	nan(nSec,1)	;
for j=1:size(U,1)-1
    bool1       =	SecBounds(1:end-1)>=U(j,1)       ;
    bool2       =	SecBounds(1:end-1)<=U(j+1,1)     ;
    index       =	find(and(bool1,bool2))                  ;
    AERO(index) =	U(j,2)                           ;
end


% ========================================================================
%   EVALUATE THE SPLINE BASIS FUNCTIONS IN THE SECTION BOUNDARIES
% ========================================================================

B0              =	nan(nSec,getnbasis(bao),order)          ;
for j=1:(order)
    B0(:,:,j)	=	eval_basis(SecBounds(1:nSec),bao,j-1)   ;
end

% ========================================================================
%   FIRST-ORDER SYSTEM
% ========================================================================

% --------------------------------------------------------------------
% 1. First section
% --------------------------------------------------------------------
iSec    =   1                       ;    

b0      =	squeeze(B0(iSec,:,:))'	;
AER     =	AERO(iSec)              ; 

% response of first state variable to unknown input signal
% response of first state variable to equilibrium state  via known input signal
% response of first state variable to initial signal via known input signal
bb1_0   =   -ttsim.^[1:order]*diag(1./factorial(1:order))*b0    ;
bb1_1   =   +AER*f1{1}(tau1,ttsim)                              ;
bb1_2   =   (1-AER*f1{1}(tau1,ttsim))                           ;

Y1      =	zeros(ntt,nbasis+2) ;
Y1      =	Y1 + [ bb1_0    bb1_1   bb1_2   ]                   ;

%   feedback on unknown input signal via known input signal
for j=1:order
    bb1_0_AER       =   AER*f1{j+1}(tau1,ttsim)*b0(j,:)         ;
    Y1(:,1:nbasis)	=	Y1(:,1:nbasis) + bb1_0_AER              ;
end

% --------------------------------------------------------------------
% 2. Every other section
% --------------------------------------------------------------------

for iSec=2:nSec
    
    % Locate start of section
    index	=	find(ttsim>=SecBounds(iSec),1,'first')  ;
    ttloc	=	max(ttsim(index:end)-SecBounds(iSec),0) ;
    nttSec	=	length(ttloc )                          ;
    
    % basis function evaluation at the start of the section
    b0      =   squeeze(B0(iSec,:,:))'                  ;
    
    % known input signal during this section
    AER     =	AERO(iSec)                              ;
    
    % --------------------------------------------------------------------
    % 2.1. response to current unknown input signal
    % --------------------------------------------------------------------
    
    % pre-allocation
    B1      =	zeros(length(ttloc),getnbasis(bao))     ;
    
    % direct response to unknown input signal
    bb_0    =   -ttloc.^[1:order]*diag(1./factorial(1:order))*b0    ;
    B1      =	B1 + bb_0 ;
    
    % feedback on first state variable via known input signal
    for j=1:order
        bbj =   AER*f1{j+1}(tau1,ttloc)*b0(j,:)     ;
        B1  =	B1 + bbj                            ;
    end
    
    B1g     =   [ B1 AER*f1{1}(tau1,ttloc) zeros(nttSec,1) ]	;
    
    % --------------------------------------------------------------------
    % 2.2. response to current state
    % --------------------------------------------------------------------
    B1fb    =   (1-AER*f1{1}(tau1,ttloc))*Y1(index,:)           ;
    
    
    % --------------------------------------------------------------------
    % 2.3. Combine the two responses 
    % --------------------------------------------------------------------
    Y1(index:end,:)	=	B1g + B1fb	;
    
    
end

% --------------------------------------------------------------------
% 3. Add column for second initial state
% --------------------------------------------------------------------
Y1	=	[	Y1	zeros(length(ttsim),1)	]   ;

% ========================================================================
%   SECOND-ORDER SYSTEM
% ========================================================================

% --------------------------------------------------------------------
% 1. First section
% --------------------------------------------------------------------

iSec	=   1                       ;
AER     =	AERO(iSec)              ;
b0      =	squeeze(B0(iSec,:,:))'	;

if equaltau
    
    % direct response of first state variable to unknown input signal (none)
    % response of first state variable to state 1
    % response of first state variable to equilibrium state
    % response of first state variable to state 2
    cc1_0   =   zeros(ntt,nbasis)                               ;
    cc1_1   =   AER*f2{1}(tau2,ttsim)                           ;
    cc1_2   =   f1{1}(tau2,ttsim)-AER*f2{1}(tau2,ttsim)         ;
    cc1_3   =   1-f1{1}(tau2,ttsim)                             ;
    
    Y2      =	[	 cc1_0  cc1_1   cc1_2   cc1_3   ]           ;
    
    % direct response of first state variable to unknown input signal via
    % known input signal
    for j=1:order
        cc2_AER         =   (AER*f2{j+1}(tau2,ttsim)-f1{j+1}(tau2,ttsim))*b0(j,:)   ;
        Y2(:,1:nbasis)	=	Y2(:,1:nbasis) + cc2_AER                                ;
    end
    
else
    
    % direct response of first state variable to unknown input signal (none)
    % response of first state variable to state 1
    % response of first state variable to equilibrium state
    % response of first state variable to state 2
    cc2_0   =   zeros(ntt,nbasis)                               ;
    cc2_1   =   AER*f2{1}(tau1,tau2,ttsim)                      ;
    cc2_2   =   f1{1}(tau2,ttsim)-AER*f2{1}(tau1,tau2,ttsim)	;
    cc2_3   =   1-f1{1}(tau2,ttsim)                             ;
    
    Y2      =	[	 cc2_0  cc2_1   cc2_2   cc2_3   ]           ;
    
    % direct response of first state variable to unknown input signal via
    % known input signal
    for j=1:order
        cc2_AER         =   (AER*f2{j+1}(tau1,tau2,ttsim)-f1{j+1}(tau2,ttsim))*b0(j,:)  ;
        Y2(:,1:nbasis)	=	Y2(:,1:nbasis) + cc2_AER                                    ;
    end
end

% --------------------------------------------------------------------
% 2. Every other section
% --------------------------------------------------------------------

for iSec=2:nSec
    
    % Locate start of section
    index	=	find(ttsim>=SecBounds(iSec),1,'first') ;
    ttloc	=	max(ttsim(index:end)-SecBounds(iSec),0);
    
    % basis function evaluation at the start of the section
    b0      =   squeeze(B0(iSec,:,:))';

    % known input signal during this section
    AER = AERO(iSec) ;
    
    % --------------------------------------------------------------------
    % 2.1. response to current unknown input signal
    % --------------------------------------------------------------------
    
    if equaltau
        % direct response to first state
        C1              =	(f1{1}(tau2,ttloc)-AER*f2{1}(tau2,ttloc))*Y1(index,:)	;
        
        % response to equilibrium state
        cc0             =   AER*f2{1}(tau2,ttloc)	;
        C1(:,nbasis+1)	=	C1(:,nbasis+1) +cc0     ;
        
        % response to unknown input signal
        for j=1:order
            cc1             =	-f1{j+1}(tau2,ttloc)*b0(j,:)	;
            cc2             =	AER*f2{j+1}(tau2,ttloc)*b0(j,:)	;
            C1(:,1:nbasis)	=	C1(:,1:nbasis)+cc1+cYc          ;
        end
        
    else
        % direct response to unknown input signal
        C1              =	(f1{1}(tau2,ttloc)-AER*f2{1}(tau1,tau2,ttloc))*Y1(index,:) ;
        
        % feedback on second state variable via known input signal
        cc0             =   AER*f2{1}(tau1,tau2,ttloc)	;
        C1(:,nbasis+1)	=	C1(:,nbasis+1) + cc0        ;
        
        % response to unknown input signal
        for j=1:order
            cc1             =   -f1{j+1}(tau2,ttloc)*b0(j,:)            ;
            cc2             =   AER*f2{j+1}(tau1,tau2,ttloc)*b0(j,:)	;
            C1(:,1:nbasis)	=   C1(:,1:nbasis)+cc1+cc2                  ;
        end
    end
    
    % --------------------------------------------------------------------
    % 2.2. response to current state
    % --------------------------------------------------------------------
    C1fb    =   (1-f1{1}(tau2,ttloc))*Y2(index,:) ;
    
    
    % --------------------------------------------------------------------
    % 2.3. Combine the two responses 
    % --------------------------------------------------------------------
    Y2(index:end,:) = C1+C1fb ;
end

% ========================================================================
%   FINAL STEPS
% ========================================================================

% Select rows in Y1 and Y2 corresponding to values of the time variable
% that are requested
inc	=	ismember(ttsim,tspan)	;
Y1	=	Y1(inc,:)               ;
Y2	=	Y2(inc,:)               ;

% rotate the last 3 columns (facilitates bounding procedure)
P	=	[	+1	0	0       ;	...
            +1	-1	0       ;   ...
            +1	0	-1	]   ;
P	=       blkdiag(eye(nbasis),P)  ;
        
% apply reverse rotation to the computed matrices
Q	=       pinv(P) ;
Y1	=       Y1*Q	;
Y2	=       Y2*Q	;

end

% ***********************************************************************
% ***********************************************************************
