function [fighan,subhan]  =  SCSplot(model,tsim,drv)

% -------------------------------------------------------------------------
% SCS toolbox - SCSplot_1fig.m (addapted from SCSplot.m
% -------------------------------------------------------------------------
% Description
%   This function produces a plot to vizualize the shape constrained spline
%   function and its derivatives. 
%
% -------------------------------------------------------------------------
% Syntax:   [fighan,subhan]  =  SCSplot(model,zsim,drv)
%
% Inputs:
% 	model       Shape Constrained Spline model
%   tsim	(*) Input variable values for which the function needs to be
%               plotted.
%   drv     (*) Derivatives to plot given as a vector of values between 0
%               and model.dims.degree
%
% Outputs:
%	fighan      Handle for the figure that is produced
% 	subhan      Handles for the panels in the produced figure
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-06-09
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% ========================================================================
%   INPUT CHECKS
% ========================================================================

% fields that are required in model:
fields = {'basisobj','beta','knots','t','Xt','Xk','y'} ;
for iF=1:length(fields)
    assert(isfield(model,fields{iF}),['Error in SCSplot: Field model.' fields{iF} ' not found in model'])
end

if nargin<2 || isempty(tsim)
    tsim	=	GenerateGrid(model,model.dims.degree)           ;
end
assert(any(size(tsim)==[1 1]),      'Error in SCSplot: tsim has the wrong dimensions, should be column or row vector')
assert(all(and(tsim>=model.domain(1),tsim<=model.domain(end))),   'Error in SCSplot: tsim has values that are outside of the function domain (model.domain)')

if nargin<3 || isempty(drv)
    drv     =	0           ;
end
assert(any(size(drv)==[1 1]),       'Error in SCSplot: drv has the wrong dimensions, should be column or row vector')
assert(all(drv>=0),                 'Error in SCSplot: drv contains a value that is lower than 0')
assert(all(drv<=model.dims.degree), 'Error in SCSplot: drv contains a value that is higher than model.dims.degree')


if isfield(model,'conv') 
    % if the model is an input-output model, an additional panel is added
    % to display the system output (convoluted spline function) and input
    % (spline function) separately.
    drv = [ nan ; drv(:) ];
end
n_drv       =	length(drv) ;

% ========================================================================
%   GET INFORMATION FROM MODEL
% ========================================================================

gamma       =	model.gamma     ;
knots       =	model.knots     ;
t_model     =	model.t         ;
y           =   model.y         ;

% ========================================================================
%   GET TRANSITIONS
% ========================================================================

if isfield(model,'transtime')
    transtime   =	model.transtime ;
else
    transtime   =	sparse(0,1)     ;
end

ty      =	t_model ;
tk      =	knots	;

% ========================================================================
%   REVERSE WARPING
% ========================================================================

% To display all features in the original scale of the data, the internal
% model variables are warped back into their original scale:
if isfield(model,'warp') 
    
    Warp	=	model.warp      ;
    nWarp	=	size(Warp,1)    ;
    for j=1:4
        switch j
            case 1, t	=	tsim        ;
            case 2, t	=	tk          ;
            case 3, t	=	ty          ;
            case 4, t	=	transtime	;
        end
        
        nt              =	length(t)	;
        z               =	nan(nt,1)   ;
        for it=1:nt
            index       =	find(Warp(:,2)<=t(it),1,'last') ;
            if index == nWarp
                z(it)	=	Warp(index,1)                   ;
            else
                F       =	( Warp(index+1,1)-Warp(index,1) )/ (Warp(index+1,2)-Warp(index,2) ) ;
                z(it)	=	Warp(index,1) + (t(it)-Warp(index,2))*F                             ;
            end
        end
        
        switch j
            case 1, zsim	=	z	;
            case 2, zk      =	z	;
            case 3, zy      =	z	;
            case 4, ztrans	=	z	;
        end
        
    end
    
else
    zsim	=	tsim           ;
    zy      =	ty     ;
    zk      =	knots       ;
    ztrans	=	transtime	;
end

% ========================================================================
%   ACTUAL PLOTTING
% ========================================================================

fighan = figure('Name',model.string ,'Numbertitle','off');

for i_drv=1:n_drv

    % --------------------------------------------------------------------
    % Define panel
    if n_drv>1, subhan{i_drv} = subplot(n_drv,1,i_drv) ;
    end
    hold on
    % --------------------------------------------------------------------
    
    plotdata = false ;
    
    if isnan(drv(i_drv))
        % Compute model predictions
        yhat            =	model.Z*gamma	;
        plotdata        =   true            ;
        zhat            =	model.t         ;
    else
        
        % Compute the function or its derivative
        zhat            =   zsim            ;
        yhat = SCSeval(model,tsim,drv(i_drv));
        
        if ~isfield(model,'conv') && drv(i_drv)==0
            plotdata = true ; 
        end
    end
    
    % Plot
    plot(zhat,yhat,'k-','Linewidth',2)
    if plotdata
        if length(y)<=73,   plot(zy,y,'ro','MarkerFaceColor','r'),    
        else,               plot(zy,y,'r.','MarkerSize',11),    set(gca,'Ylim',Ylim)
        end
    end
    
    % Set scales
    set(gca,'Xlim',[zk(1) zk(end)])
    
    % Annotation
    if isnan(drv(i_drv))
        %ylabel(['y^{(0)}'])
    else
        if isfield(model,'conv')
            %ylabel(['r^{(' num2str(drv(i_drv)) ')}'])
        else
            %ylabel(['y^{(' num2str(drv(i_drv)) ')}'])
        end
    end
    
    %PlotVertical(ztrans,'m-','Linewidth',1.1)     ;   % Transitions
    %PlotHorizontal(0,'b-','Linewidth',1.1)        ;   % Zero line
    
    % Add X-axis label in last panel only
    if i_drv==n_drv,    
        %xlabel('t') ,      
    end
    
    if length(knots)<=inf
        % Add lines for spline knots - adding and deleting is used to ensure
        % they are shown over the whole panel
        %han	=	PlotVertical(zk,'-','Color',[1 1 1]*.42)	;
        drawnow
        %delete(han)
        %PlotVertical(zk,'-','Color',[1 1 1]*.73)           ;
    end
    
    % Plot (repeat)
    if plotdata
        if length(y)<=73,   plot(zy,y,'ro','MarkerFaceColor','r'),    
        else,               plot(zy,y,'k*','MarkerSize',1),    %set(gca,'Ylim',Ylim)
        end
    end
end


end

% ***********************************************************************
% ***********************************************************************

