function model  =  ApplyConv(model,U,tau)

% -------------------------------------------------------------------------
% SCS toolbox - ApplyConv.m
% -------------------------------------------------------------------------
% Description
%   This function transforms the shape constrained spline model into an
%   input-output system model where the spline function is used to
%   represent an unknown input signal.
%
% -------------------------------------------------------------------------
% Syntax:   model  =  ApplyConv(model,U,tau)
%
% Inputs:
%   model   Shape Constrained Spline model
%   U       On-off binary input signal given as an [Mx2] matrix with M the
%           number times the input signal changes value. The first column
%           represents the times of change and the second column the value
%           (0 or 1).
%   tau     Time constants (dimensions [2x1])
%
% Outputs:
% 	model   Shape Constrained Spline model (updated)
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2018.01.26
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% ========================================================================
%   INPUT CHECKS
% ========================================================================

assert(length(tau)==2,	'Error in ApplyConv: tau does not have length 2.')
assert(size(U,2)==2,	'Error in ApplyConv: U is not a matrix with two columns.')

% ========================================================================
%	UNIT RESPONSES
% ========================================================================

order   =	model.dims.order	;

if isfield(model,'conv')
    f1  =	model.conv.f1       ;
    f2  =	model.conv.f2       ;
else
    % This part is computationally expensive but is only needed to be
    % computed once in typical applications:
    syms s T Tau1 Tau2
    
    for j=1:(order+2)
        
        % considered unit input in Laplace domain 
        % (j=0 -> pulse response, j=1 -> step response, etc...)
        lapunit	=   (1/s).^(j)	;
        
        % symbolic unit responses of first-order system with time constant
        % Tau1 
        lapfun	=   lapunit * (1/(Tau1*s+1)) ;
        g1      =   ilaplace(lapfun)        ;
        f1{j}   =	matlabFunction(g1)      ;
        
        % symbolic unit responses of second-order system with time constant
        % Tau1 and Tau2
        % - response to be used when Tau1==Tau2
        lapfun	=   lapunit * (1/(Tau1*s+1))  * (1/(Tau1*s+1)) ;
        g1      =   ilaplace(lapfun)        ;
        f2{j,1}   =	matlabFunction(g1)      ;
        % - response to be used when Tau1~=Tau2
        lapfun	=   lapunit * (1/(Tau1*s+1))  * (1/(Tau2*s+1)) ;
        g1      =   ilaplace(lapfun)        ;
        f2{j,2}   =	matlabFunction(g1)      ;
        
    end
    
end


% ========================================================================
% 	COMPUTE CONVOLUTED BASIS
% ========================================================================

bao     =	model.basisobj      ;
knots	=	model.knots         ;
ttil    =	model.t             ;
y       =	model.y             ;

[~,Z]	=	PrecomputeBasisConv(bao,knots,ttil,tau,f1,f2,U)	;


% ========================================================================
%   DIMENSIONS
% ========================================================================
nGamma	=	size(Z,2)           ;
nBeta	=	model.dims.beta     ;
nAlpha	=	nGamma-nBeta        ;


% ========================================================================
% UNCONSTRAINED SOLUTION
% ========================================================================
lambda	=	model.lambda	;
method	=   1               ;
switch method
    case 1
        R           =	blkdiag(model.R,sparse(nAlpha,nAlpha))	;
    	gamma_unc	=   RidgeRegression(Z,y,R,lambda,method)	;
    case 2
        L           =   model.L                                 ;
        L           =	[	L	sparse(size(L,1),nAlpha)	]   ;
    	gamma_unc	=   RidgeRegression(Z,y,L,lambda,method)	;
end

f           =   -y(:)'*Z                    ;
W           =   Z'*Z+lambda*R               ;
L           =   chol(W)                     ;
M           =	(L'*L)^(-1)*L'              ;

alpha_unc	=	gamma_unc((1:nAlpha)+nBeta)	;
beta_unc	=	gamma_unc(1:nBeta)          ;


% ========================================================================
%   STORE INFORMATION
% ========================================================================

% parameters defining the fit and penalty objectives
model.L         =   L           ;
model.M         =	M           ;
model.R         =   R           ;
model.Z         =	Z           ;
model.W         =	(W+W')/2    ;
model.f         =	f           ;

% current solution
model.alpha     =   alpha_unc	;
model.alpha_unc	=	alpha_unc	;
model.beta      =   beta_unc	;
model.beta_unc	=	beta_unc	;
model.gamma     =   gamma_unc   ;
model.gamma_unc	=	gamma_unc	;

% convolution parameters
model.conv.f1	=	f1	;
model.conv.f2	=	f2  ;
model.conv.U	=	U  ;

% model dimension parameters
model.dims.alpha	=	nAlpha	;
model.dims.beta     =	nBeta   ;
model.dims.gamma    =	nGamma  ;


% ========================================================================
%   FOR CONIC OPTIMIZATION
% ========================================================================
model	=	PrecomputeConstraints(model)    ;

end

% ***********************************************************************
% ***********************************************************************
