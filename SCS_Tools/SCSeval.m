function ysim = SCSeval(model,tsim,drv)

% -------------------------------------------------------------------------
% SCS toolbox - SCSeval.m
% -------------------------------------------------------------------------
% Description
%   This function computes SCS model predictions or the derivative thereof.
%
% -------------------------------------------------------------------------
% Syntax:   ysim = SCSeval(model,tsim,drv)
%
% Inputs:
% 	model       SCS model as structure
%   tsim    (x)	Vector: values for independent variable
%   drv     (x)	Scalar: derivative that needs to be computed (+ 0 )
%
%   (x)     Optional input
%   (+)     Default value
%
% Outputs:
% 	ysim        Model prediction
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-06-09
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% Last modification: Kris Villez, 2019-06-09

if nargin<3 || isempty(drv)
    drv =0
end

t_model     =	model.t         ;
if (length(tsim)==length(t_model) && all(t_model==tsim))
    SelectBasis	=	1	;
elseif length(tsim)==length(model.knots) && all(model.knots==tsim)
    SelectBasis =	2	;
else
    SelectBasis =	0	;
end

% If necessary, evaluate the basis functions in the requested
% locations. Otherwise, select it from the precomputed ones:
switch SelectBasis
    case 0, X	=	eval_basis(model.basisobj,tsim,drv)	;
    case 1, X	=	model.Xt{drv+1}                    ;
    case 2, X	=	model.Xk{drv+1}                    ;
    case 3, X	=	model.Xk{drv+1}                   ;
end

ysim            =	X*model.beta          ;

end