function beta  =  RidgeRegression(X,y,L,lambda,method)  

% -------------------------------------------------------------------------
% SCS toolbox - RidgeRegression.m
% -------------------------------------------------------------------------
% Description
%   This function implements ridge regression.
%
% -------------------------------------------------------------------------
% Syntax:   beta  =  RidgeRegression(X,y,L,lambda,method) 
%
% Inputs:
%   X           Input/design matrix [Mx1]
%   y           Output variable measurements [MxN]
%   L           Penalty matrix ( if method=1) or Cholesky decomposition of
%                penalty matrix (if method=2)
%   lambda      Regularization meta-parameter
%   method      Applied computational method
%               (1: matrix inversion, 2: Cholesky decomposition)
%
% Outputs:
%   beta        Regression coefficients
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2018.01.25
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2017 Kris Villez
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% =======================================================================
%   INPUT CHECKS
% =======================================================================

assert(size(X,1)==length(y),		'Error in RidgeRegression: Numbers of rows in X and y do not match')
assert(size(L,2)==size(X,2),		'Error in RidgeRegression: Numbers of columns in X and L do not match')
assert(all(size(lambda)==[1 1]),	'Error in RidgeRegression: lambda has the wrong dimensions, should be [1x1]')
assert(isreal(lambda) && lambda>=0,	'Error in RidgeRegression: lambda has an illegal value, should be real non-negative')
assert(all(size(method)==[1 1]),	'Error in RidgeRegression: method has the wrong dimensions, should be [1x1]')
assert(ismember(method,[1 2]),		'Error in RidgeRegression: method has an illegal value, should be 1 or 2')

% =======================================================================
%   COMPUTE
% =======================================================================

switch method
    case 1
        R = L ;
        % naive ridge regression approach
        P               =	(X'*X+lambda*R)^(-1)*X' ;
        beta            =	P*y     ;
    
    case 2
        % replace with cholesky-based solution which is numerically stable
        nL              =       size(L,1)                   ;
        y_aug           =   [   y;  zeros(nL,1) ]           ;
        X_aug           =   [   X ; sqrt(lambda)*L ]	;

        [Q,R]           =       qr(X_aug,0)                 ;
        z               =   	Q'*y_aug                    ;
        % R * beta_aug = z
        beta        =       linsolve(full(R),full(z))   ;
end

end

