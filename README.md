SCS toolbox v3.0 - Shape Constrained Spline toolbox
by Kris Villez

Website:	http://homepages.eawag.ch/~villezkr/spike/
Contact: 	kris.villez@eawag.ch

This toolbox is a collection of Matlab/Octave functions and scripts originally written to implement the Generalized Shape Constrained Spline fitting method. They are released to the public under the GPL v3 license in order to encourage the sharing of code and prevent duplication of effort. If you find these functions useful, drop me a line. I also appreciate bug reports and suggestions for improvements. For more detail regarding copyrights see the file LICENSE.TXT located in the same folder where this README file is located.

These files are provided as is, with no guarantees and are intended for non-commercial use. These routines have been developed and tested successfully with Matlab (R2010a) on a Windows system. Tests in Octave are incomplete at the time of writing. To install the Matlab functions in a Windows systems, using Matlab or Octave, follow the instructions below.

---------------------------------------------------
 I. Installing the toolbox and other preliminaries
---------------------------------------------------

1. Create a directory to contain all required toolboxes.  For example, in my case all toolbox directories are in:

		C:\Tools\

2. Move the unzipped SCS folder (in which this README_SCS.txt file is located) to the following location:
	
		C:\Tools\SCS_v3.0\SCS\

3. The installation includes the DM (Data Mining) toolbox (v1.0). This provides a number of basic data modelling functionalities. It is located in the following folder: 

		C:\Tools\SCS_v3.0\SCS\Adopted\DM_1.0\DM

4. The DM toolbox includes the Spike_B toolbox (v1.0) which provides a number of basic functionalities. It is located in the following folder: 
		
		C:\Tools\SCS_v3.0\SCS\Adopted\Spike_B_v1.0\Spike_B
		
The DM toolbox also includes the a modified version of the pre-existing FDA toolbox. Note that compatibility is only guaranteed with this version, which is located in: 
		
		C:\Tools\SCS_v3.0\SCS\Adopted\DM_1.0\DM\Tools\Supervised\FunctionalDataAnalysis

5. The installation includes the Spike_O toolbox (v1.1). Note that earlier versions of the Spike_O toolbox are incompatible. It is located in the following folder: 
		
		C:\Tools\SCS_v3.0\SCS\Adopted\Spike_O_v1.1\Spike_O\

6. Install MOSEK from: http://www.mosek.com/ at following location (choose custom installation):

		C:\MOSEK\
		
		Make sure you have installed a valid license and placed it in the proper location. This depends on the version of MOSEK. 
		
		Testing: 'TryMOSEK.m' is a script providing a small test problem to verify proper installation.
	
-----------------------
 II. Using the toolbox
-----------------------

1. Start Matlab

2. To enable access to the functions in the toolboxes from any other folder, type the following statements in the command line for each toolbox

		addpath(genpath(	'C:\Tools\SCS_v3.0\SCS\')	)
		addpath(		'C:\MOSEK\'			)
	
	This allows access during the current Matlab/Octave session.

3. To enable access to the toolboxes permanently (across sessions), type the following in the command line:
	
		savepath

4. To test and demo the SCS toolbox, execute the following scripts located in the SCS_Applications folder (e.g. .\SCS\SCS_Applications\):
	
	Demo1 	Folder: \SCS_Applications\Publication_2013_Villez&al_CACE_DEMO
			Script: Demo_Titanium
	Demo2 	Folder: \SCS_Applications\Publication_2019_Thuerlimann&al_DemoAndFigures
			Script: Script_figure3_video


5. For help with the SCS toolbox, type 'help FUN' (without quotes) and FUN the actual function you want help with, e.g.:
	
		help SCSfit
	
----------------------------
 III. Relevant publications
----------------------------

Please refer to this toolbox by citing following publication:
[1] Thuerlimann, C. M., Udert, K. M., Morgenroth, E., Villez, K. (2019). Stabilizing Control of a Urine Nitrification Process in the Presence of Sensor Drift. Submitted.

The original FDA toolbox was published on-line:
[2] Ramsay, J. O. & Silverman, B. W. (2001). Functional data analysis software. MATLAB edition. On-line at
http://ego.psych.mcgill.ca/misc/fda/ .

in association with the following books:
[3] Ramsay, J. O. & Silverman, B. W. (2002). Applied functional data analysis: methods and case studies (Vol. 77). New York: Springer.
[4] Ramsay, J. O. (2006). Functional data analysis. John Wiley & Sons, Inc..
 
The 'Titanium' data set was originally published in:
[5] de Boor, C. (1978). A Practical Guide to Splines. Springer.

Other publications discussing SCS models are:
[6] Derlon, N, Thürlimann, C M, Dürrenmatt, D J, Villez, K (2017). Batch settling curve registration via image data modeling. Wat Res, 114, 327-337.
[7] Masic, A, Srinivasan, S, Billeter, J, Bonvin, D, Villez, K (2017). Shape constrained splines as transparent black-box models for bioprocess modeling. Comp Chem Eng, 99, 96-105.
[8] Villez, K., Habermacher, J. (2016). Shape Anomaly Detection for Process Monitoring of a Sequencing Batch Reactor. Computers and Chemical Engineering, 91, 365-379.
[9] Villez, K., Rieger, L., Keser, B. & Venkatasubramanian, V. (2012). Probabilistic qualitative analysis for fault detection and identification of an on-line phosphate analyzer. International Journal of Advances in Engineering Sciences and Applied Mathematics, 4, 67-77.
[10] Villez, K., Rosén, C., Anctil, F., Duchesne, C. & Vanrolleghem, P.A. (2013). Qualitative Representation of Trends (QRT): Extended method for identification of consecutive inflection points. Computers and Chemical Engineering, 48, 187-199.
[11] Villez, K., Venkatasubramanian, V. & Rengaswamy, R. (2013). Generalized Shape Constrained Spline Fitting for Qualitative Analysis of Trends. Computers & Chemical Engineering, 58, 116-134.
[12] Villez, K. (2015). Qualitative Path Estimation: A fast and reliable Algorithm for Qualitative Trend Analysis. AIChE Journal, 61, 1535-1546.

-------------
 IV. Changes
-------------

Changes are as follows:

Version 2.0 - changes: 
1. Functionality
It is now possible to optimize the locations of transitions which imply or are otherwise associated with a discontinuity in one or more derivatives which are otherwise continuous.
2. Functionality
A qualitative sequence can now be specified as a string with included characters corresponding to the primitives. This is limited to monotonic, monocurvature and triangular primitives (i.e., implying shape constraints on the first and/or second derivative only).
3. Functionality
There is a shorthand command (function: AutoSCS) which allows straightforward optimization of the transitions using a number of default optimization settings.
4. Supporting software
The SCS toolbox operates exclusively with the Mosek optimization software. The use of CVX for optimization is curnot supported anymore.

Version 2.1 - changes:
5. README file
This file has been updated to correct for errors in the sections "I. Installing the toolbox and other preliminaries" and "II. Using the toolbox" and for updates in the references.
6. Corrections executed in the help text for "Alphabet", "DefaultIntX", "FitSCS.m", "SCS2QR".

Version 3.0 - changes: 
1. Functionality
Fresh implementation, easier to maintain, should be more reliable.
2. Demos
Removed most old demos - will be added in again in a future release.

------------------
 Acknowledgements
------------------

I want to thank the following people for testing the toolbox and their subsequent suggestions:
- Benjamin Stucki
- Christian M. Thürlimann
- Jonathan M. Habermacher

----------------------------------------------------------
Last modified on 09 June 2019