
% -------------------------------------------------------------------------
% DemoPCA.m
% -------------------------------------------------------------------------
% Description 
%
% This script showcases how a principal component analysis model can be
% built. The data used here were published first in [1].
%
% [1] Villez, K. and Habernacher, J. (in preparation). Shape anomaly
%       detection for a batch process. Manuscript in preparation.
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-07-13
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Kris Villez
%
% This file is part of the DM Toolbox for Matlab/Octave. 
% 
% The DM Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The DM Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the DM Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
clear all
close all

A = sprintf(['This demo simply produces a number of graphs relevant to principal \n ' ...
      'component analysis (PCA) model identification. Check the code to \n ' ...
      'learn how to identify your own PCA model.']);
disp(A)

data    =   load(['Data'])      ;
Y       =   data.Z(:,1:end-1)   ;
Z       =   data.Z(:,end)       ;

% =========================================================================
% ARRANGE/SELECT DATA

normal              =   find(Z==2)      ;
normal_cal          =   normal(1:100)   ;
abnormal            =   find(Z==1)      ;

% =========================================================================
% PCA PHASE I

% -------------------------------------------------------------------------
% Setup
Model.Centering.Method	= 	'column-mean'       ;
Model.Scaling.Method 	= 	'none'              ;

Ycal    =	Y(normal_cal,:)         ;
Yval    =   Y                       ;

% -------------------------------------------------------------------------
% Computation of PCs/loading vectors with calibration data
[Tcal,Model]	=	EstimationPCA(Ycal,Model)           ;

% -------------------------------------------------------------------------
% Vizualization
XlimPC = [0 11]+.5;
XtickPC = floor(XlimPC(1)):ceil(XlimPC(end)) ;
YtickPC = [0:10:100] ;

figure, FigPrep
subplot(2,1,1), AxisPrep
han = bar(Model.Summary.Eigenvalues./sum(Model.Summary.Eigenvalues)*100)    ;
set(han,'EdgeColor','k','FaceColor',[1 1 1]*.73)
ylabel({' ','Relative Variance','[%]'})

set(gca,'Xlim',XlimPC,'Xtick',XtickPC,'Xticklabel',[],'tickdir','out')
set(gca,'Ytick',YtickPC)
grid on
gcapos = get(gca,'Position') ;
gcapos(1)   =	.2	;
gcapos(2)   =	.56 ;
gcapos(4)   =	.42	;
set(gca,'Position',gcapos)

subplot(2,1,2)   , AxisPrep
relcumvar = cumsum(Model.Summary.Eigenvalues)./sum(Model.Summary.Eigenvalues)*100 ;
han = bar(relcumvar)    ;
set(han,'EdgeColor','k','FaceColor',[1 1 1]*.73)
xlabel('#PC')
ylabel({'Cumulative','Relative Variance','[%]'})

set(gca,'Xlim',XlimPC,'Xtick',XtickPC,'tickdir','out')
set(gca,'Ytick',YtickPC)
grid on
gcapos = get(gca,'Position') ;
gcapos(1)= .2 ;
gcapos(4) = .42 ;
set(gca,'Position',gcapos)

figure, FigPrep
TickMinutes = [0:5:85] ;
XlimTime    = [-1 513] ;

for pc=1:2
    subplot(2,1,pc), AxisPrep, hold on,
    PC = Model.Summary.Basis(:,pc) ;
    PC = PC*sign(PC(end)) ;
    
    if UsingOctave
        plot(1:length(PC),PC,'.','Color','k')
    else
        stem(1:length(PC),PC,'.','Color',[1 1 1]*.73)
        plot(1:length(PC),PC,'.','Color','k')
    end
    if pc==1
        set(gca,'Xlim',XlimTime,'Xtick',TickMinutes*6,'Xticklabel',TickMinutes)
        ylabel('PC1 [mV]','Position',[ -73 mean(get(gca,'Ylim'))])
    else
        set(gca,'Xlim',XlimTime,'Xtick',TickMinutes*6,'Xticklabel',TickMinutes)
        ylabel('PC2 [mV]','Position',[ -73 mean(get(gca,'Ylim'))])
    end
    gcapos = get(gca,'Position') ;
    gcapos(1) = .17 ;
    set(gca,'Position',gcapos) ;
end
xlabel('Time [min.]')

% =========================================================================
% PCA PHASE II

% Computation of scores for all data
[Tval]          =	EstimationPCA(Yval,Model,+1) 		;

% -------------------------------------------------------------------------
% Dimension reduction (2 PCs)
Tcal(:,3:end)   =	0                                   ;
Tval(:,3:end)   =	0                                   ;

% -------------------------------------------------------------------------
% Reconstruction and statistic computation
[Yval_rec]      =	EstimationPCA(Tval,Model,-1) 		;
SSR             =	sum( (Yval_rec-Yval).^2 ,2)         ;


figure(3), AxisPrep, hold on

if UsingOctave
    plot(normal,SSR(normal,1),'ko','MarkerFaceColor','w')
    plot(abnormal,SSR(abnormal,1),'ko','MarkerFaceColor','r')
    plot(normal_cal,SSR(normal_cal,1),'ko','MarkerFaceColor','b')
else
    stem(normal,SSR(normal,1),'ko','MarkerFaceColor','w')
    stem(abnormal,SSR(abnormal,1),'ko','MarkerFaceColor','r')
    stem(normal_cal,SSR(normal_cal,1),'ko','MarkerFaceColor','b')
end
set(gca,'Yscale','log')
xlabel('Sample index')
ylabel('SSR')
legend('Normal samples','Abnormal samples','Calibration samples')
title('Q statistic')

figure(4), FigPrep
for j=1:2
    subplot(2,1,j)
    AxisPrep, hold on
    if UsingOctave
        plot(normal,Tval(normal,j),'ko','MarkerFaceColor','w')
        plot(abnormal,Tval(abnormal,j),'ko','MarkerFaceColor','r')
        plot(normal_cal,Tval(normal_cal,j),'ko','MarkerFaceColor','b')
    else
        stem(normal,Tval(normal,j),'ko','MarkerFaceColor','w')
        stem(abnormal,Tval(abnormal,j),'ko','MarkerFaceColor','r')
        stem(normal_cal,Tval(normal_cal,j),'ko','MarkerFaceColor','b')
    end
    xlabel('Sample index')
    ylabel(['PC' num2str(j) ' score'])
    legend({'Normal samples','Abnormal samples','Calibration samples'})
    if j==1
        title('PC Score')
    end
end


figure(5), AxisPrep, hold on
plot(Tval(normal,1),Tval(normal,2),'ko','MarkerFaceColor','w')
plot(Tval(abnormal,1),Tval(abnormal,2),'ko','MarkerFaceColor','r')
plot(Tcal(:,1),Tcal(:,2),'ko','MarkerFaceColor','b')
xlabel('PC1 score')
ylabel('PC2 score')
legend('Normal samples','Abnormal samples','Calibration samples')
title('Score biplot')

return
