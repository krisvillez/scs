function [Xout,Model] = EstimationPCA(Xin,Model,Mode)
				
% -------------------------------------------------------------------------
% DM Toolbox - EstimationPCA.m
% -------------------------------------------------------------------------
% Description 
%
% EstimationPCA is a function which identifies or applies a principal
% component model. The function provides three modes of operation, which
% are:
%   0	- Calibration mode
%   +1	- Projection mode
%   -1	- Reconstruction mode
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[Xout,Model] = EstimationPCA(Xin,Model,Mode)
%
% INPUT
%	Xin         Input data. These can be data to analyze or principal
%               scores.
%               (required)
%	Model       Existing PCA model or structure describing PCA model
%               properties. 
%               (required)
%	Mode        Integer indicating the mode of operation (see above).
%               (optional, default=0)
%
% OUTPUT
%	Xout     	Principal scores (Mode = 0 or +1) or
%               reconstructed data (Mode = -1)
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-07-13
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Kris Villez
%
% This file is part of the DM Toolbox for Matlab/Octave. 
% 
% The DM Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The DM Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the DM Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

assert(nargin>=2,'Insufficient number of inputs to ''Estimation.m''. ')

if nargin<=2 || isempty(Mode)
	Mode = 0 ; % Calibration mode
end

switch Mode
	case 0 % calibration
		
		[Beta_c,center]			=	Centering(Xin,[],0,...
										Model.Centering.Method				)	;
		
		[Beta_cs,scale]			=	Scaling(Beta_c,[],0,...
										Model.Scaling.Method				)	;
		
		[Xout,P,eigs,eigs_rel]	=	Decomposition(Beta_cs) 		;
			
		Basis 		=	diag(scale)*P 	; 
		Center		=	center 		;	 
		
		% Summarize model:
		Model.Summary.Center 	= Center ;
		Model.Summary.Basis 	= Basis ;
        Model.Summary.Eigenvalues 	= eigs ;
        Model.Summary.RelVar 	= eigs_rel ;
				
	case {1,-1} 
		
		assert(isfield(Model,'Summary'),'Field ''Summary'' not available in Model')
		assert(isfield(Model.Summary,'Basis'),'Field ''Basis'' not available in Model.Summary')
		assert(isfield(Model.Summary,'Center'),'Field ''Center'' not available in Model.Summary')
		
		A 		= 	Model.Summary.Center	;
		B 		=	Model.Summary.Basis		;
		assert(size(B,1)==size(A,2),'Model parameter dimensions do not match')
		
		switch Mode 
			case 1	% projection (forward steps)
				
				[m,n] = size(Xin) ;
				[n2,c] = size(B) ;
				assert(n2==n,'Model parameter dimensions and data matrix dimensions do not match')
				
				if any(isnan(Xin(:)))
					Xout 				=	nan(m,c) 		;
					
					Nans 				= 	~isnan(Xin) 		;
					[Nans_unique,I,J]	=	unique(Nans,'rows') ;
					nNans_unique 		= 	size(Nans_unique,1) ;
					
					for iNans_unique=1:nNans_unique % for each pattern of missing data
						
						row	=	(J==J(I(iNans_unique))) ;
						col	=	Nans_unique(iNans_unique,:) ;
				
						Xout(row,:) = (Xin(:,col)-A(:,col))*pinv(B(col,:)') ;
				
                    end
                    
				else
					Xout	=	(Xin-repmat(A,[m 1]))*pinv(B') ;
				end

			case -1
				
				[m,c] = size(Xin) ;
				assert(size(B,2)==c,'Model parameter dimensions and score matrix dimensions do not match')
                
                Xout = Xin*B'+repmat(A,[m 1]) ;
				
			otherwise
				error('Unknown value for ''Mode'' in Estimation.m')
		end
	otherwise
		error('Unknown value for ''Mode'' in Estimation.m')
end

