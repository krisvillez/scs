function [Xout,P,eigs,eigs_rel] = Decomposition(Xin,P,Mode,Method,lvmax)

% -------------------------------------------------------------------------
% DM Toolbox - Decomposition.m
% -------------------------------------------------------------------------
% Description 
%
% This function identifies or applies a basic latent variable model.
% The function provides three modes of operation, which are:
%   0	-   Calibration mode
%   +1	-   Projection mode
%   -1	-   Reconstruction mode
% The function currently supports the use of one model identification
% method:
%   1   -   Principal component analysis (PCA)
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[Xout,P,eigs,eigs_rel] = Decomposition(Xin,P,Mode,Method,lvmax)
%
% INPUT
%	Xin         Input data. These can be data to analyze or principal
%               scores.
%               (required)
%	P           Existing projection matrix
%               (ignored for mode 0, required for modes +1 and -1)
%	Mode        Integer indicating the mode of operation (see above).
%               (optional, default=0)
%	Method      Applied modelling method
%               (optional, default=0)
%
% OUTPUT
%	Xout     	Principal scores (Mode = 0 or +1) or
%               reconstructed data (Mode = -1)
%   P           Projection matrix
%   eigs        Eigenvalues
%   eigs_rel    Eigenvalues in relative scale (=relative variance)
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-07-13
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Kris Villez
%
% This file is part of the DM Toolbox for Matlab/Octave. 
% 
% The DM Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The DM Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the DM Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

[m,n]=size(Xin) ;

if nargin<5 || isempty(lvmax)
	lvmax = min([m n]) ;
end

% Mode:
%	1: forward (projection)
%	0: estimation
% 	-1: reverse (reconstruction)

 [m,n] = size(Xin) ;

if nargin<2 || isempty(P)
	if nargin>=3 && ~isempty(Mode)
		assert(Mode==0,'Improper Mode of operation')
	else
		Mode = 0 ;
	end
end

if nargin==2 || isempty(Mode)
	Mode = 1 ; % forward
end

if Mode==0 % estimation
	if nargin<4 || isempty(Method)
		Method = 1;
	end
	
	switch	Method
		case 1 % PCA
			%[U,S,P]=svd(Xin,lvmax) ;
            [U,S,P]=svd(Xin) ;
			eigs = (diag(S).^2) ;
			eigs_rel = eigs./sum(eigs) ;
			%Xout1 = U*S ;
			
			eigs = eigs(1:lvmax) ;
			eigs_rel = eigs_rel(1:lvmax) ;
			P = P(:,1:lvmax) ;
			
		case 2 % ICA
			error('ICA not defined yet')
		case 3 % NMF
			error('NMF not defined yet')
		otherwise
			error('Unknown identification method')
	end
end

switch Mode
	case {0,1} % estimation/projection
	
		assert(n==size(P,1),'Dimensions of data matrix and projection matrix do not match')
		if any(isnan(Xin(:)))
			
			Xout = nan(m,size(P,2)) ;
			Nans 		= 	~isnan(Xin) 			;
			[Nans_unique,I,J]	=	unique(Nans,'rows') ;
			nNans_unique = size(Nans_unique,1) ;
			for iNans_unique=1:nNans_unique % for each pattern of missing data
				rows = (J==J(I(iNans_unique))) ;
				cols = Nans_unique(iNans_unique,:) ;
				Xout(rows,:) = Xin(rows,cols)*P(cols,:)*diag(1./diag(P(cols,:)'*P(cols,:))) ;
			end
		else
			Xout 	=	Xin*P ;
        end
        
	case -1 % reconstruction
		assert(n==size(P,2),'Dimensions of data matrix and projection matrix do not match')
		Xout 	=	Xin*P' ;
end
