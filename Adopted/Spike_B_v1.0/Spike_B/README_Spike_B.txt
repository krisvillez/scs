Spike_B Toolbox v1.0 - Basic Matlab functions
by Kris Villez

Website:	http://homepages.eawag.ch/~villezkr/spike/
Contact: 	kris.villez@eawag.ch

This toolbox is a collection of Matlab/Octave functions and scripts which help to program efficiently in Matlab. They are released to the public under the GPL v3 license in order to encourage the sharing of code and prevent duplication of effort. If you find these functions useful, drop me a line. I also appreciate bug reports and suggestions for improvements. For more detail regarding copyrights see the file LICENSE.TXT located in the same folder where this README file is located.

These files are provided as is, with no guarantees and are intended for non-commercial use. These routines have been developed and tested successfully with Matlab (R2010a) on a Windows system. Tests in Octave are incomplete at the time of writing. To install the Matlab functions in a Windows systems, using Matlab or Octave, follow the instructions below.

---------------------------------------------------
 I. Installing the toolbox and other preliminaries
---------------------------------------------------

1. Create a directory to contain all required toolboxes.  For example, in my case all toolbox directories are in:

		C:\Tools\

2. Move the unzipped Spike_B folder (in which this README_Spike_B.txt file is located) to the following location:
	
		C:\Tools\Spike_B_v1.0\Spike_B
	
-----------------------
 II. Using the toolbox
-----------------------

1. Start Matlab

2. To enable access to the functions in the toolboxes from any other folder, type the following statement in the command line for each toolbox

		addpath C:\Tools\Spike_B_v1.0\Spike_B\
	
	This allows access during the current Matlab/Octave session.

3. To enable access to the toolboxes permanently (across sessions), type the following in the command line:
	
		savepath

4. For help with the SCS toolbox, type 'help FUN' (without quotes) and FUN the actual function you want help with, e.g.:
	
		help PlotHorizontal

		
----------------------------
 III. Relevant publications
----------------------------

This toolbox was developed first to produce results in the following manuscript:
[1]	Habermacher, J., Villez, K. (2016). Shape Anomaly Detection for Process Monitoring of a Sequencing Batch Reactor. Computers and Chemical Engineering, Accepted, In Press.

----------------------------------------------------------
Last modified on 11 May 2016