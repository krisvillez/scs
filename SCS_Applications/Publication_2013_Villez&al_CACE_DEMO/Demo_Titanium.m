
% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - Demo_Titanium.m
% -------------------------------------------------------------------------
% Description 
%
% This script demonstrates the use of the SCS and SPIKE_O toolboxes for
% shape constrained spline function fitting. 
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc 
clear all
close all

addpath(genpath('../../Adopted'))
addpath('../../SCS_Tools')
SetupEnvironment_Test

% =========================================================================

disp('0. Load and plot data:')
load data_titanium

plottime = x(1):.01:x(end);

figure, plot(x,y,'ko')

disp(' paused')
pause

% =========================================================================

disp('1. Create SCS object and fit unconstrained spline:')
tk      =       x(1:1:end)          ;
tx      =   [   tk(1):1:tk(end) ]   ;
tk      =   [   tk([1:1:end-1]) tk(end)    ]   ;


lambda      =   10^(-7)   ;
degree      =   3 ; 
model         =	SCSsetup(y(:),x(:),degree,tk(:),lambda)	;

model1        =   SCSfit(model)        ;

[fig1,ax1]  =  SCSplot(model,[],[0 1]);

disp(' 	Notice the fluctuations in the 1st derivative of the fitted function')
disp(' paused')
pause

% =========================================================================

disp('2. Add shape constraint as in Turlach et al. (2005) and fit Shape Constrained Spline accordingly:') 

SolT = [835 955];


model2  =       model ;
model2          =   ApplyQS(model,'PQP')          ;
SolK  =  T2K(model2,SolT);
model2          =	SCSfit(model2,SolK)      ;


disp('	Applied QR: ')            
disp(['      Episode start: ' num2str([ model2.domain(1) ; model2.transtime]','%5.0f') ])
disp(['      Episode end:   ' num2str([ model2.transtime ; model2.domain(end) ]','%5.0f') ])            
disp(['      Primitive:     ' ' P    Q    P'] )            

[fig2,ax2]  =  SCSplot(model2,[],[0 1]);

disp(' 	Notice how the fluctuations in the 1st derivative of the fitted function have disappeared')
disp(' paused')
pause

% =========================================================================

disp('3. Compute bounds:') 

model3  =       model ;
model3          =   ApplyQS(model3,'BNA')          ;

TempInt = [ 835 886 ;
            910 955];
disp('	Applied intervals for transitions: ')            
disp([ '      Transition 1: ' num2str(TempInt(1,:),'%5.0f')])    
disp([ '      Transition 2: ' num2str(TempInt(2,:),'%5.0f')])

KnotInt =  T2K(model3,TempInt);
[LB,UB,BestK,modelLB,modelUB]  =  SCSfitbnd(model3,KnotInt);
BestT = K2T(model3,BestK);

disp('	Computed solution for upper bound: ')            
disp([ '      Transition 1: ' num2str(BestT(1)','%5.2f')])    
disp([ '      Transition 2: ' num2str(BestT(2)','%5.2f')])
disp('')
disp('	Applied QR for upper bound: ')            
disp(['      Episode start: ' num2str([model3.domain(1) ; BestT]','%5.0f') ])
disp(['      Episode end:   ' num2str([BestT ; model3.domain(2) ]','%5.0f') ])            
disp(['      Primitive:     ' ' B    N    A'] )        
disp('') 
disp('	Applied QR for lower bound: ')            
disp(['      Episode start: ' num2str([model3.domain(1)   TempInt(2,:)],'%5.0f') ])
disp(['      Episode end:   ' num2str([TempInt(1,:) model3.domain(2) ],'%5.0f') ])                
disp(['      Primitive:     ' ' B    N    A'] )        

disp('	Computed bounds: ')            
disp([ '      Lower bound: ' num2str(LB,'%5.6f')])    
disp([ '      Upper bound: ' num2str(UB,'%5.6f')])

disp(' paused')
pause

% =========================================================================
% Branch-and-Bound
disp('4. Branch-and-bound search:') 

model4  =       model ;
model4          =   ApplyQS(model3,'BNA')          ;

TempInt = repmat(model4.domain,[ length(model4.string)-1 1 ]);

disp('	Applied search space for transitions: ')            
disp([ '      Transition 1: ' num2str(TempInt(1,:),'%5.0f')])    
disp([ '      Transition 2: ' num2str(TempInt(2,:),'%5.0f')])

% Define optimization algorithm meta-parameters:
display=1;
graph=5;
options.display     =	display>=2      ;	% display interval
options.graph       =	max(graph-1,0)	;	% produce graphical output?
options.branching   =	[ 1  ]          ;	% branching method

model4.boundfun = @SCSfitbnd  ;
model4.resol	=	ones(model4.dims.transitions,1)	;

% Execute search:

disp('')
disp('	Colour code: ')    
disp('      Blue    = fathomed node')    
disp('      Pink    = verified solution (upper bound = lower bound)')    
disp('  Please take note of above colour codes to understand subsequent visualization of the branch-and-bound algorithm')    
disp(' paused')
pause

KnotInt =  T2K(model4,TempInt);
SolK	=	BBsearch(model4,KnotInt,options)	;

% Fit model with selected solution:
model4                  =	SCSfit(model4,SolK)      ;

[TransKnot,TransTime,TransInt]  =  ComputeTransitions(model4);

disp('')
disp('	Obtained optimal solution: ')            
disp([ '      Transition 1: ' num2str(TransTime(1)','%5.3f')])    
disp([ '      Transition 2: ' num2str(TransTime(2)','%5.3f')])

tsim	=	model.domain(1):model.domain(2)  ;
ysim	=	SCSeval(model4,tsim,0)                   ;

figure(42)
if exist('FigPrep','file'), FigPrep, end

ax12 = subplot(3,1,1:2);
    if exist('AxisPrep','file'), AxisPrep, end
    plot(model4.t,model4.y,'ko','linewidth',2,'markerfacecolor','k')
    plot(tsim,ysim,'k-','linewidth',2)
    ylabel('Measurement');
    ax12.Position([1 3]) = [ 0.1 0.85];
    set(ax12,'Xlim',model4.domain,'Xtick',unique(round(model4.t/25)*25),'Xticklabel',[],'XAxisLocation','top')
    set(ax12,'Ylim',[0.5 2.4])
    set(ax12,'YTick',[0.5:0.1:2.4])
    ax12.YTickLabel(ax12.YTick*2~=round(ax12.YTick*2))={' '};
    if exist('xticklabel_rotate','file')
        % if interested,get this function at:
        %   http://www.mathworks.com/matlabcentral/fileexchange/3486-xticklabelrotate
        set(ax12,'Xlim',model4.domain,'Xtick',unique(floor(model4.t/10)*10),'Xticklabel',[])
    else
        set(ax12,'Xlim',model4.domain,'Xtick',unique(round(model4.t/25)*25))
        ax12.XTickLabel(2:2:end,:)=' ';
    end
    
ax3 = subplot(3,1,3);
    if exist('AxisPrep','file'), AxisPrep, end
    qrthan = QRTplot(model4.string,sort([model4.domain(:); model4.transtime]),1);
    xlabel('Temperature [^oC]');
    ylabel('QR');
    ax3.Position([1 3]) = [ 0.1 0.85];
    ax3.Position([2]) = ax3.Position([2]) +0.05;
    if exist('xticklabel_rotate','file')
        % if interested,get this function at:
        %   http://www.mathworks.com/matlabcentral/fileexchange/3486-xticklabelrotate
        set(ax3,'Xlim',model4.domain,'Xtick',unique(floor(model4.t/10)*10),'Xticklabel',unique(floor(model4.t/10)*10))
        ax3.XTickLabel(1:2:end,:)=' ';
        hText = xticklabel_rotate([],90,[],'FontSize',get(gca,'FontSize'),'VerticalAlignment','middle');
        
    else
        set(ax3,'Xlim',model4.domain,'Xtick',unique(round(model4.t/25)*25))
        ax3.XTickLabel(2:2:end,:)={' '};
    end
    ax3.YTick =[];
    
disp('	Plotted optimal solution with qualitative representation')            

return
