function [cut]=cutd(dat,startdate,enddate) 
%CUTD cut data by date
% [cut] = CUTD(dat,startdate,enddate) cuts out a data section from startdate 
% until enddate out off a data matrix  
%
% Parameters
% --------------------------------------------------------------
%       dat = data matrix with a date number in the first column
% startdate = start date/time as a date number
%   enddate = end date/time as a date number
% --------------------------------------------------------------

istart=find(dat(:,1)>=(startdate),1,'first');
iend=find(dat(:,1)<=(enddate),1,'last');
cut=dat(istart:iend,:);
end