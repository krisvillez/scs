% SCRIPT plotting a video of the SCS fitting procedure
% It calls SCSdirect which calls the SCSplot_1fig.m function
% (addapted from SCSplot.m) to plot every iteration of the fitting
% procedure in the same figure.

clc
clear all
close all

addpath('./data')
addpath(genpath('../../Adopted'))
addpath('../../SCS_Tools')

SetupEnvironment_Test
load NO2sel.mat %example data NO2-signal UV-VIs 
load pHsel.mat %corresponding pH data
% load data



% define timespan and step (minutes)
startTime = 200;
endTime = 1300;
step = 10;

% open the video function and name it
v = VideoWriter('./video/ModelSelection.avi');
v.Quality = 95;
v.FrameRate = 7;
open(v)

strings = {'UDA','UD','U'}  ;
dk = 50;
S = length(strings) ;

% open figure
ax = figure(1);
ax.Position = [280 258 700 440];
if exist('FigPrep'), FigPrep, end
ax1 = subplot(2,1,1);
ax1.Position(3:4) = ax1.Position(3:4)*1.1;
Pos1 = ax1.Position;
ax2 = subplot(2,1,2);
    if exist('AxisPrep'), AxisPrep, end
ax2.Position(2) = ax2.Position(2)+0.02; 
ax2.Position(3:4) = ax2.Position(3:4)*1.1;
Pos2 = ax2.Position;
    if exist('AxisPrep'), AxisPrep, end

RMSR = nan(length(NO2sel(2,:)),S);
snapshot = true;

% Loop over the defined time span
for i = startTime:step:endTime

    % define entries for SCSdirect
    NO2 = NO2sel(2,1:i)';
    y = NO2;
    t       =	(1:length(y))'-1 ;
    
    tsim = linspace(t(1),t(end),length(t)*5+1);
    Yhat = nan(length(tsim),S);
    
    for s=1:S
        % --------------------------------------------------------------------
        %   SHAPE DEFINITION
        % --------------------------------------------------------------------
        String  =   strings{s}                       ;
        
        % --------------------------------------------------------------------
        %   FITTING
        % --------------------------------------------------------------------
        model	=	SCSdirect(y,t,String,'KnotDistance',dk,'Graph',0,'Display',0)	;
        Yhat(:,s)	=	eval_basis(model.basisobj,tsim,0)*model.beta	;
        % --------------------------------------------------------------------
        %   STORING RESULTS
        % --------------------------------------------------------------------
        %transhist(1:length(model.transtime),1,s)     =   model.transtime     ;
        RMSR(i,s)        =   sqrt(model.performance.objective);
    end

    ax1 = subplot('Position',Pos1);
    plot(t/60,y,'k.')
    hold on
    if exist('AxisPrep'), AxisPrep, end
    hp = plot(tsim/60,Yhat,'-','Linewidth',2);
    ax1.XLim = [0 22];
    ax1.XTick = [0:22];
    ax1.YLim = [-50 0];
    ax1.YTick = [-50:5:0 ];
    ax1.YTickLabel(2:2:end)={' '};
    ax1.XTickLabel =[];
    ylabel('[mgNO_{2}-N/L]');
    legend('Location','NorthEast','data','UDA','UD','U')
    drawnow()
    hold off
    
    %% Add subplot with the RMSR values
    ax2 = subplot('Position',Pos2);
    plot(t/60,RMSR(1:i,1),'o','Color',hp(1).Color,'Linewidth',2);
    hold on
    if exist('AxisPrep'), AxisPrep, end
    plot(t/60,RMSR(1:i,2),'+','Color',hp(2).Color,'Linewidth',2);
    plot(t/60,RMSR(1:i,3),'x','Color',hp(3).Color,'Linewidth',2);
    ax2.XLim = [0 22];
    ax2.XTick = [0:22];
    ax2.YLim = [0 10];
    ax2.YTick = [0:1:10];
    xlabel('Time [h]')
    ylabel('RMSR');
    legend('Location','NorthWest','UDA','UD','U')
    drawnow()
    hold off
    
    if snapshot && (i/60)>=9
        tsnap = tsim;
        Ysnap = Yhat;
        isnap = i;
        snapshot = false;
    end
    
    % write this step to video
    M = getframe(gcf);
    writeVideo(v,M)

end
% complete video
close(v)

figure,
if exist('FigPrep'), FigPrep, end
ax11 =subplot(2,2,1);
    if exist('AxisPrep'), AxisPrep, end
    hold on
    plot(t(1:isnap)/60,y(1:isnap),'k.')
    hp = plot(tsnap/60,Ysnap,'-','Linewidth',2);
    ax11.XLim = [0 22];
    ax11.XTick = [0:22];
    ax11.YLim = [-50 0];
    ax11.YTick = [-50:5:0 ];
    ax11.YTickLabel(2:2:end)={' '};
    ax11.XTickLabel =[];
    ax11.Position(1) = ax11.Position(1)-0.02;
    ax11.Position(2) = ax11.Position(2)-0.02;
    ax11.Position(3:4) = ax11.Position(3:4)*1.2;
    ylabel('[mgNO_{2}-N/L]');
    legend('Location','NorthEast','data','UDA','UD','U')
    drawnow()
    
ax12 =subplot(2,2,2);
    if exist('AxisPrep'), AxisPrep, end
    hold on
    plot(t/60,y,'k.')
    hp = plot(tsim/60,Yhat,'-','Linewidth',2);
    ax12.XLim = [0 22];
    ax12.XTick = [0:22];
    ax12.YLim = [-50 0];
    ax12.YTick = [-50:5:0 ];
    ax12.YTickLabel(2:2:end)={' '};
    ax12.XTickLabel =[];
    ax12.YAxisLocation = 'right';
    ax12.Position(1) = ax12.Position(1)-0.05;
    ax12.Position(2) = ax12.Position(2)-0.02;
    ax12.Position(3:4) = ax12.Position(3:4)*1.2;
    legend('Location','NorthEast','data','UDA','UD','U')
    drawnow()
    
ax21 =subplot(2,2,3);
    if exist('AxisPrep'), AxisPrep, end
    hold on
    plot(t(1:isnap)/60,RMSR(1:isnap,1),'o','Color',hp(1).Color,'Linewidth',2);
    plot(t(1:isnap)/60,RMSR(1:isnap,2),'+','Color',hp(2).Color,'Linewidth',2);
    plot(t(1:isnap)/60,RMSR(1:isnap,3),'x','Color',hp(3).Color,'Linewidth',2);
    ax21.XLim = [0 22];
    ax21.XTick = [0:22];
    ax21.XTickLabels(2:2:end)={' '};
    ax21.YLim = [0 10];
    ax21.YTick = [0:1:10];
    ax21.Position(1) = ax21.Position(1)-0.02;
    ax21.Position(3:4) = ax21.Position(3:4)*1.2;
    xlabel('Time [h]')
    ylabel('RMSR');
    legend('Location','NorthWest','UDA','UD','U')
    drawnow()
    
ax22 =subplot(2,2,4);
    if exist('AxisPrep'), AxisPrep, end
    plot(t/60,RMSR(1:i,1),'o','Color',hp(1).Color,'Linewidth',2);
    hold on
    plot(t/60,RMSR(1:i,2),'+','Color',hp(2).Color,'Linewidth',2);
    plot(t/60,RMSR(1:i,3),'x','Color',hp(3).Color,'Linewidth',2);
    ax22.XLim = [0 22];
    ax22.XTick = [0:22];
    ax22.XTickLabels(2:2:end)={' '};
    ax22.YLim = [0 10];
    ax22.YTick = [0:1:10];
    ax22.YAxisLocation = 'right';
    ax22.Position(1) = ax22.Position(1)-0.05;
    ax22.Position(3:4) = ax22.Position(3:4)*1.2;
    xlabel('Time [h]')
    legend('Location','NorthWest','UDA','UD','U')
    drawnow()

figurepath =  './figures/figure3' ;
saveas (gcf,figurepath,'fig')
saveas (gcf,figurepath,'epsc')
saveas (gcf,figurepath,'tiff')