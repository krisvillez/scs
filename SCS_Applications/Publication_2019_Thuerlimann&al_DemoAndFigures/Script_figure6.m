% Monitor SCS Controller Data
clc
clear all
close all

addpath('./data')
addpath(genpath('../../Adopted'))

% load the data files
load('Conditions_Mar-19_to_Jun-20')
load('Logbook_mat')
load('TemppH1.mat')


% get date limits
start_date = datenum(2018,03,19);
end_date = datenum(2018,05,30);

% transpose Logbook tables
Temperature_all=Temperature_all';
NO2 = NO2';
NO2_orig = NO2;
NO2_strip = NO2_strip';
NO2_strip(2,:) = NO2_strip(2,:);

% select temp values with reference measurements
% with Lange test
member = ismembertol(Temperature_all(1,:),NO2(1,:),1/((7.371482784837963e+05)*24*600));
Temp_Lange = Temperature_all(:,member);
member2 = ismembertol(NO2(1,:),Temperature_all(1,:),1/((7.371482784837963e+05)*24*60));
NO2 = NO2(:,member2);
% with nitrite strip tests
member_strip = ismembertol(Temperature_all(1,:),NO2_strip(1,:),1/((7.371482784837963e+05)*24*600));
Temp_Strip = Temperature_all(:,member_strip);
member2strip = ismembertol(NO2_strip(1,:),Temperature_all(1,:),1/((7.371482784837963e+05)*24*62));
NO2_strip = NO2_strip(:,member2strip);


% choose only ref measurements with RMSR=3
RMSR3 = find(conditions.RMSRtrue==3);
% for Lange
member_low = ismembertol(Temp_Lange(1,:),conditions.actualTime(1,RMSR3),20/((7.371482784837963e+05)*24*60));
Temp_Lange_low = Temp_Lange(:,member_low);
member2_low = ismembertol(NO2(1,:),Temp_Lange_low(1,:),20/((7.371482784837963e+05)*24*60));
NO2_low = NO2(:,member2_low);
% for Strip tests
member_low_strip = ismembertol(Temp_Strip(1,:),conditions.actualTime(1,RMSR3),20/((7.371482784837963e+05)*24*60));
Temp_low_strip = Temp_Strip(:,member_low_strip);
member2_low_strip = ismembertol(NO2_strip(1,:),Temp_low_strip(1,:),20/((7.371482784837963e+05)*24*600));
NO2_low_strip = NO2_strip(:,member2_low_strip);

%%
dOP=diff(conditions.opState);


%%f
ax = figure(1);
if exist('FigPrep'), FigPrep, end
if exist('AxisPrep'), AxisPrep, end
Time=conditions.actualTime(1,dOP(1,:)>0);
ylim([0 25])
hold on
h1=plot(NO2_low_strip(1,:),NO2_low_strip(2,:),'o','MarkerSize',11);
h1.MarkerFaceColor = h1.Color ;
h2=plot(NO2_low(1,:),NO2_low(2,:),'o','MarkerSize',11);
h2.MarkerFaceColor = h2.Color ;
if exist('PlotVertical'), PlotVertical(Time,'k-','Linewidth',1); end

plot(NO2_low_strip(1,:),NO2_low_strip(2,:),'o','Color',h1.Color ,'MarkerFaceColor',h1.Color,'MarkerSize',11 );
plot(NO2_low(1,:),NO2_low(2,:),'o','Color',h2.Color ,'MarkerFaceColor',h2.Color ,'MarkerSize',11);

datetick('x','dd/mm','keepticks','keeplimits')
xlim([start_date end_date])
hold on
ylabel('NO_{2}^{-},Ref. Measurement [mgNO_{2}-N/L]')
legend({'NO_{2}^{-}-N strip test','NO_{2}^{-}-N LCK341','end of cycles'})

ax=gca;
    ax.YTick = [0:25];
    ax.YTickLabel((ax.YTick/5~=round(ax.YTick/5)))={' '};
    
    set(ax,'xtick',start_date:7:end_date)
    datetick('x','dd/mm','keepticks','keeplimits')
    XTickLBL = ax.XTickLabel;
    XTickLBL(setdiff(1:length(XTickLBL),1:1:length(XTickLBL)),:)=' ';
    ax.XTickLabel = XTickLBL;
    xlabel('Time [dd/mm]')

figurepath =  './figures/figure6' ;
saveas (gcf,figurepath,'fig')
saveas (gcf,figurepath,'epsc')
saveas (gcf,figurepath,'tiff')