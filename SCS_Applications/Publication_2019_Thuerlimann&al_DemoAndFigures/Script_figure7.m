% Monitor SCS Controller Data
clc
clear all
close all

addpath('./data')
addpath(genpath('../../Adopted'))

% load the data files
load('Conditions_Mar-19_to_Jun-20')
load('Logbook_mat')
load('COD_measurement.mat')
load('HRT_measurement.mat')

% cut out erronous values
ind = find(conditions.NO2(1,:)>10^5);
conditions.NO2 = conditions.NO2(:,ind);

% get date limits
date_start = conditions.NO2(1,1);
date_end = datenum(2018,06,01);

% transpose Logbook tables
NO2 = NO2';
NO2_strip = NO2_strip';
NO3 = NO3';
NH4 = NH4';
COD = COD(:,1:2)';
HRT = HRT';

% cut out strip tests above 23 mg/L
ind_strip = find(NO2_strip(2,:)<24);
NO2_strip = NO2_strip(:,ind_strip);

% calc average value for SCAN measurements
number2average = 20;
conditions.NO2(2,:) = conditions.NO2(2,:);
for i = number2average/2+1:length(conditions.NO2)-number2average/2
    filtered_NO2(2,i-number2average/2) = mean(conditions.NO2(2,i-number2average/2:i+number2average/2));
    filtered_NO2(1,i-number2average/2) = conditions.NO2(1,i);
end


% select values with reference measurements
% with Lange test
member = ismembertol(filtered_NO2(1,:),NO2(1,:),1/((7.371482784837963e+05)*24*80));
NO2_SCAN1 = filtered_NO2(:,member);
member2 = ismembertol(NO2(1,:),filtered_NO2(1,:),1/((7.371482784837963e+05)*24*62));
NO2 = NO2(:,member2);
% with nitrite strip tests
member = ismembertol(filtered_NO2(1,:),NO2_strip(1,:),1/((7.371482784837963e+05)*24*80));
NO2_SCAN2 = filtered_NO2(:,member);
member2 = ismembertol(NO2_strip(1,:),filtered_NO2(1,:),1/((7.371482784837963e+05)*24*62));
NO2_strip = NO2_strip(:,member2);

%%
% Lange test Time Series 
ax = figure(1);
if exist('FigPrep','file'), FigPrep, end

ax1 = subplot(3,1,1:2) ;
if exist('AxisPrep','file'), AxisPrep, end
hold on

Diff_Lange = NO2_SCAN1(2,:) - NO2(2,:);
Diff_piecewise(1,:) = NO2_SCAN1(1,:);
Diff_piecewise(2,:) = NO2_SCAN1(2,:) - NO2(2,:);
Diff_piecewise_strip(1,:) = NO2_SCAN2(1,:);
Diff_piecewise_strip(2,:) = NO2_SCAN2(2,:) - NO2_strip(2,:);

c = NO2(2,:);

%%
%Create area for temp experiment
area( [datenum(2018,04,21,18,00,00), datenum(2018,04,25,18,00,00), datenum(2018,04,25,18,00,00), datenum(2018,04,21,18,00,00)],[10,-60,10,-60], 'FaceColor',[0.901960784313726 0.901960784313726 0.901960784313726],'FaceAlpha',0.5);

plot(NO2(1,:),Diff_Lange,'ko','MarkerFaceColor','k','MarkerSize',8);

ylabel({'NO_{2,UV-Vis}^{-} - NO_{2,Ref. Meas.}^{-}','[mg NO_{2}-N/L]'})
set(gca,'xticklabel',[])
xlim([date_start date_end]);
ylim([-55 -28]);

x = cutd(NO2',datenum(2018,03,25),datenum(2018,04,13,11,00,00));
y1 = cutd(Diff_piecewise',datenum(2018,03,25),datenum(2018,04,13,11,00,00));
P = polyfit(x(:,1)-x(1,1),y1(:,2),1);
    yfit = P(1)*(x-x(1,1))+P(2);
    plot(x,yfit,'k-','LineWidth',3,'HandleVisibility','off');
    s = ['r = ',num2str(round(P(1),1)),' mgNO_{2}^{-}-N/L/d'];
    text(x(1)+1,-49,s,'Color','k','FontSize',14);
% second piece
x = cutd(NO2',datenum(2018,04,13,11,00,00),datenum(2018,04,30,11,00,00));
y1 = cutd(Diff_piecewise',datenum(2018,04,13,11,00,00),datenum(2018,04,30,11,00,00));
P = polyfit(x(:,1)-x(1,1),y1(:,2),1);
    yfit = P(1)*(x(:,1)-x(1,1))+P(2);
    plot(x,yfit,'k-','LineWidth',3,'HandleVisibility','off');
    s = ['r = ',num2str(round(P(1),1)),' mgNO_{2}^{-}-N/L/d'];
    text(x(1)+1,-48,s,'Color','k','FontSize',14);
    
% third piece
x = cutd(NO2',datenum(2018,04,30),datenum(2018,05,14,8,00,00));
y1 = cutd(Diff_piecewise',datenum(2018,04,30),datenum(2018,05,14,8,00,00));
P = polyfit(x(:,1)-x(1,1),y1(:,2),1);
    yfit = P(1)*(x(:,1)-x(1,1))+P(2);
    plot(x,yfit,'k-','LineWidth',3,'HandleVisibility','off');
    s = ['r = ',num2str(round(P(1),1)),' mgNO_{2}^{-}-N/L/d'];
    text(x(1)+.5,-39.2,s,'Color','k','FontSize',14);
% fourth piece
x = cutd(NO2',datenum(2018,05,14),datenum(2018,06,01));
y1 = cutd(Diff_piecewise',datenum(2018,05,14),datenum(2018,06,01));
P = polyfit(x(:,1)-x(1,1),y1(:,2),1);
    yfit = P(1)*(x(:,1)-x(1,1))+P(2);
    plot(x,yfit,'k-','LineWidth',3,'HandleVisibility','off');
    s = ['r = ',num2str(round(P(1),1)),' mgNO_{2}^{-}-N/L/d'];
    text(x(1)-5,-30,s,'Color','k','FontSize',14);
    plot(0,0,'k-','LineWidth',3);
    
    % create and add a line for the change from men's to women's urine
    urine_change = datenum(2018,04,30,09,13,00) ;
    if exist('PlotVertical','file'), PlotVertical(urine_change,'b--','linewidth',3); end
    % create and add lines for S::CAN cleaning
    cleaned = [datenum(2018,02,19)  datenum(2018,03,19)  datenum(2018,04,13,11,00,00) datenum(2018,05,22,12,30,00)];
    if exist('PlotVertical','file'), PlotVertical(cleaned,'r:','linewidth',3); end
    
legend({'Temperature high experiment', 'Difference of nitrite estimation and nitrite reference','Piece-wise linear approximation of differences','Influent switched from male to female urine', 'UV-Vis sensor cleaning'},'Location','North')
 xlim([date_start date_end]);
    set(ax1,'xtick',date_start:7:date_end)
    ax1.XAxisLocation='top' ;
    datetick('x','dd/mm','keepticks','keeplimits')
    XTickLBL = ax1.XTickLabel;
    XTickLBL(setdiff(1:length(XTickLBL),1:1:length(XTickLBL)),:)=' ';
    ax1.XTickLabel = XTickLBL;

%%
% Ammonium, and Nitrate concentrations in a second subplot
ax3 = subplot(3,1,[3]) ;
if exist('AxisPrep','file'), AxisPrep, end
hold on
Colors = get(gca,'ColorOrder');
yyaxis left
set(gca,'Ycolor','k')
plot(NO3(1,:),NO3(2,:),'+','MarkerSize',8,'Color',Colors(1,:),'linewidth',2);
plot(NH4(1,:),NH4(2,:),'x','MarkerSize',8,'Color',Colors(2,:),'linewidth',2);
xlim([date_start date_end]);
ylim([1150 2200])
ylabel({'[mg NH_{4}-N/L]','[mg NO_{3}-N/L]'});
yyaxis right
set(gca,'Ycolor','k')
plot(HRT(1,:),HRT(2,:),'o','DisplayName','HRT','Color','k','linewidth',2)
if exist('PlotVertical','file'), PlotVertical(urine_change,'b--','linewidth',3); end
xlim([date_start date_end]);
ylabel('HRT [d]');
ylim([5 17]);
legend({'NO_{3}^{-} Reference Measurements','NH_{4}^{+} Reference Measurements','Hydraulic Retention Time (HRT)'},'Location','SouthWest')

xlim([date_start date_end]);
    set(ax3,'xtick',date_start:7:date_end)
    datetick('x','dd/mm','keepticks','keeplimits')
    XTickLBL = ax3.XTickLabel;
    XTickLBL(setdiff(1:length(XTickLBL),1:1:length(XTickLBL)),:)=' ';
    ax3.XTickLabel = XTickLBL;
    xlabel('Time [dd/mm]')
    
figurepath =  './figures/figure7' ;
saveas (gcf,figurepath,'fig')
saveas (gcf,figurepath,'epsc')
saveas (gcf,figurepath,'tiff')
return