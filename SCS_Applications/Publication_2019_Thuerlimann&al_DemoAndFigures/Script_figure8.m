% Monitor SCS Controller Data
clc
clear all
close all

addpath('./data')
addpath(genpath('../../Adopted'))

% load the data files
load('Conditions_Mar-19_to_May-25.mat')
load('Temperature-April.mat')
load('pH1-March-April.mat')
load('locationRMSRtrue2to3.mat')
load('Logbook_mat_adjusted_20180525.mat')

% define timespan for plot
start_date = datenum(2018,04,21,18 ,00,00);
end_date = datenum(2018,04,25,18 ,00,00);


% Plot NO2 measurement, RMSRtrue and intState
figure(1)
if exist('FigPrep'), FigPrep, end
ax1 = subplot(4,1,1);
    if exist('AxisPrep'), AxisPrep, end
    hold on
    area( [datenum(2018,04,24,10,00,00), datenum(2018,04,25,14,00,00)],[-100,-100], 'FaceColor',[0.5 0.5 0.5],'FaceAlpha',0.3,'LineStyle','none');
    plot(conditions.NO2(1,:), conditions.NO2(2,:),'.k','MarkerSize',8,'DisplayName','NO2 measurement')
    ylim([-50 0])
    ylabel({'NO_{2}-N','measurement'})
    xlim([start_date end_date])
    set(gca,'xticklabel',{[]})
    set(gca,'xtick',start_date:end_date)
    ax1.YTick = [-50:5:0 ];
    ax1.YTickLabel(2:2:end)={' '};
    set(gca,'xtick',start_date:1/6:end_date)

ax2 = subplot(4,1,2);
    if exist('AxisPrep'), AxisPrep, end
    hold on
    area( [datenum(2018,04,24,10,00,00), datenum(2018,04,25,14,00,00)],[100,100], 'FaceColor',[0.5 0.5 0.5],'FaceAlpha',0.3,'LineStyle','none');
    plot(pH1(1,:),pH1(2,:),'color','k','LineWidth', 3,'DisplayName','pH measurement')
    ylim([6 7])
    ylabel('pH')
    xlim([start_date end_date])
    set(gca,'xtick',start_date:end_date)
    set(gca,'xticklabel',{[]})
    hline1 = refline([0 6.8]);
    hline1.Color = [1 1 1]*.5;
    hline1.LineWidth = 1;
    hline2 = refline([0 6.1]);
    hline2.Color = [1 1 1]*.5;
    hline2.LineWidth = 1;
    ax2.YTick = [6:.1:7 ];
    ax2.YTickLabel(find(ax2.YTick*2~=round(ax2.YTick*2)))={' '};
    set(gca,'xtick',start_date:1/6:end_date)

ax3 = subplot(4,1,3);
    if exist('AxisPrep'), AxisPrep, end
    hold on
    area( [datenum(2018,04,24,10,00,00), datenum(2018,04,25,14,00,00)],[100,100], 'FaceColor',[0.5 0.5 0.5],'FaceAlpha',0.3,'LineStyle','none');
    plot(temp(1,:),temp(2,:),'color','k','LineWidth', 3,'DisplayName','T measurement')
    ylim([24 30])
    ylabel(sprintf('Temperature \n [�C]'))
    xlim([start_date end_date])
    set(gca,'xticklabel',{[]})
    set(gca,'xtick',start_date:end_date)
    ax3.YTick = [24:30 ];
    ax3.YTickLabel(find(ax3.YTick/5~=round(ax3.YTick/5)))={' '};
    set(gca,'xtick',start_date:1/6:end_date)
    
% Plot RMSR
ax4 = subplot(4,1,4);
    if exist('AxisPrep'), AxisPrep, end
    hold on
    area( [datenum(2018,04,24,10,00,00), datenum(2018,04,25,14,00,00)],[100,100], 'FaceColor',[0.5 0.5 0.5],'FaceAlpha',0.3,'LineStyle','none');
    plot(conditions.actualTime(1,:),flipud(conditions.RMSRout(1:3,:)),'LineWIdth',2')
    %stem(Ind(~isnan(Ind(:,2)),1),Ind(~isnan(Ind(:,2)),2)*10,'LineWIdth',2')
    if exist('PlotVertical'), PlotVertical(Ind(~isnan(Ind(:,2)),1),'k:','linewidth',3); end
    ylabel('RMSR')
    xlim([start_date end_date])
    datetick('x','HH:MM','keepticks','keeplimits')
    linkaxes([ax1,ax2,ax3],'x')
    ylim([0 11]);
    ax4.YTick = [0:11];
    ax4.YTickLabel(find(ax4.YTick/5~=round(ax4.YTick/5)))={' '};
    legend('Location','NorthEast',{'Controller inactive','UDA','UD','U'})
    xlim([start_date end_date])
    set(gca,'xtick',start_date:1/6:end_date)
    datetick('x','dd/mm','keepticks','keeplimits')
    XTickLBL = ax4.XTickLabel;
    XTickLBL(setdiff(1:length(XTickLBL),1:6:length(XTickLBL)),:)=' ';
    ax4.XTickLabel = XTickLBL;
    xlabel('Time [dd/mm]')

figurepath =  './figures/figure8' ;
saveas (gcf,figurepath,'fig')
saveas (gcf,figurepath,'epsc')
saveas (gcf,figurepath,'tiff')