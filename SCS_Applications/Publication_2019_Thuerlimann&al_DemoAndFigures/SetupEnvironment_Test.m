

global delta_global
global softwareoptim

softwareoptim = 'mosek71' ;
delta_global = 1e-7 ; 

addpath(genpath('Tools'))
%addpath(genpath('../Tools')) % for Linux
switch softwareoptim
    case {'CVX','cvx'}  % optimization with CVX
        addpath(genpath('C:\cvx-w64\cvx'))
        warning('off','MATLAB:dispatcher:nameConflict')
    case {'Mosek71','mosek71'}  % optimization with Mosek
        addpath('C:\Program Files\Mosek71\7\toolbox\r2013a\')
        %addpath('/home/pingu/mosek/8/toolbox/r2014a')      % for Linux
end
